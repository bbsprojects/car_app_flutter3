import 'package:shared_preferences/shared_preferences.dart';

Future<bool> isLogedIn() async {
  SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
  bool islogedIn = sharedPreferences.getBool('isLogedIn');
  if (isLogedIn == null) {
    await sharedPreferences.setBool('isLogedIn', false);
    return false;
  } else {
    // await sharedPreferences.setBool('isLogedIn', false);
    return islogedIn;
  }
  //sharedPreferences.setBool('isLogedIn', true);
}
