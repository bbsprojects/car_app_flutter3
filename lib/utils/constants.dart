import 'dart:io';

import 'package:flutter/material.dart';
import 'package:my_flutter_app/database/database_provider.dart';
import 'package:my_flutter_app/model/area.dart';
import 'package:my_flutter_app/model/car_system.dart';
import 'package:my_flutter_app/model/currency.dart';
import 'package:my_flutter_app/model/currentmember.dart';
import 'package:my_flutter_app/model/status.dart';

const kPrimaryColor = Color(0xFFfe5b3d);
const kPrimaryLightColor = Color(0xFFFFECDF);
const kaccentPrimaryLightColor = Color(0xFF0000FF);

const kPrimaryGradientColor = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [Color(0xFFfe7747), Color(0xFFfe5b3d)],
);
const kSecondaryColor = Color(0xFFeeeff1);
const kTextColor = Color(0xFF010103);

const kAnimationDuration = Duration(milliseconds: 200);

final headingStyle = TextStyle(
  fontSize: 28,
  fontWeight: FontWeight.bold,
  color: Colors.black,
  height: 1.5,
);
final priceProductStyle = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.normal,
  color: Colors.black,
);
final nameProductStyle = TextStyle(
  fontSize: 16,
  fontWeight: FontWeight.bold,
  color: Colors.black,
);

final styleSectionTitle = TextStyle(
    color: kaccentPrimaryLightColor, fontWeight: FontWeight.w600, fontSize: 17);

// Form Error
final RegExp emailValidatorRegExp =
    RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+");
const String kEmailNullError = "الرجاء ادخال بريدك الإلكتروني";
const String kInvalidEmailError = "الرجاء ادخال بريد الكتروني صحيح";
const String kPassNullError = "الرجاء ادخال كلمة السر";
const String kShortPassError = "كلمة السر قصيرة ";
const String kMatchPassError = "كلمة السر لاتتطابق";
const String kNamelNullError = "الرجاء ادخال اسمك";
const String kInvalidNameError = "الرجاء ادخال اسمك الثلاثي";
const String kPhoneNumberNullError = "الرجاء ادخال رقم موبايلك";
const String kInvalidPhoneNumberError = "الرجاء ادخال رقم موبايل صحيحح";
const String kAddressNullError = "الرجاء ادخال عنوانك";
const String kAddressResponseError = "الرجاء إعادة تسجيل الدخول";
const String kTryRigesterAgain = "خطأ في التسجيل الرجاء المحاولة مرة اخرى";
const String kTrySendCarAgain = "خطأ في الإرسال الرجاء المحاولة مرة اخرى";

// errors
const String p_YearNullError = "الرجاء ادخال سنة الصنع";
const String Invalid_p_Year_Error = "الرجاء ادخال نة الصنع بشكل صحيح";
const String p_PriceNullError = "الرجاء ادخال السعر";
const String p_StatusNullError = "الرجاء إدخال حالة سيارتك.. جديدة-مستخدمة";

// product details
const String CarDetails = 'تفاصيل السيارة';
const String CarMoreDetails = 'تفاصيل أخرى';
const String first_dt = 'لماركة';
const String second_dt = 'النوع';
const String third_dt = 'الموديل';
const String forth_dt = 'الحالة';
const String fifth_dt = 'اللون';
const String sixth_dt = 'المواصفات';
const String sev_dt = 'الممشى';

// product more details
const String eigh_dt = 'نظام الصوت';
const String nin_dt = 'نظام الراحة ';
const String tn_dt = 'مميزات إضافية';
const String elv_dt = 'السماح بالأيجار';
const String twelve_dt = 'نظام الصوت';
const String thrteen_dt = 'نظام الراحة ';
const String fourteen_dt = 'مميزات إضافية';
const String fifteen_dt = 'السماح بالأيجار';
// title pages
const String add_car_page_title = 'لإضافة سيارتك للبيع';
// items of area and cities
List<City> cities = <City>[
  City(c_id: 1, c_name: "الرياض"),
  City(c_id: 2, c_name: "جدة"),
  City(c_id: 3, c_name: "أبها"),
  City(c_id: 4, c_name: "مكة"),
];

List<Area> areas_Alriad = <Area>[
  Area(area_id: 1, area_name: 'كريتر'),
  Area(area_id: 2, area_name: 'معلا'),
  Area(area_id: 3, area_name: 'قلوعة'),
  Area(area_id: 4, area_name: 'المنصورة'),
];
List<Area> areas_Jedah = <Area>[
  Area(area_id: 5, area_name: '1كريتر'),
  Area(area_id: 6, area_name: '1معلا'),
  Area(area_id: 7, area_name: 'قلوعة1'),
  Area(area_id: 8, area_name: 'المنصورة1'),
];
List<Area> areas_Makah = <Area>[
  Area(area_id: 9, area_name: '2كريتر'),
  Area(area_id: 10, area_name: 'معلا2'),
  Area(area_id: 11, area_name: 'قلوعة2'),
  Area(area_id: 12, area_name: 'المنصورة2'),
];

List<Area> areas_Abha = <Area>[
  Area(area_id: 13, area_name: 'كريتر3'),
  Area(area_id: 14, area_name: 'معلا3'),
  Area(area_id: 15, area_name: 'قلوعة3'),
  Area(area_id: 16, area_name: 'المنصورة3'),
];
List<Currency> currency = <Currency>[
  Currency(curr_id: 1, curr_title: "ريال سعودي"),
  Currency(curr_id: 2, curr_title: "دولار"),
];

List<Status> status = <Status>[
  Status(car_stat_id: 1, car_stat_title: "جديد"),
  Status(car_stat_id: 2, car_stat_title: "مستخدم"),
];

List<CarTrans> carTrans = <CarTrans>[
  CarTrans(tr_id: 1, tr_trype: "توماتيك"),
  CarTrans(tr_id: 2, tr_trype: "جير"),
  // CarTrans(tra_id: 3, tra_title: "وماتيك + جير"),
];

List<CarFuell> carFuell = <CarFuell>[
  CarFuell(f_id: 1, f_type: "بنزين"),
  CarFuell(f_id: 2, f_type: "ديزل"),
];

final otpInputDecoration = InputDecoration(
  contentPadding: EdgeInsets.symmetric(vertical: 15),
  border: outlineInputBorder(),
  focusedBorder: outlineInputBorder(),
  enabledBorder: outlineInputBorder(),
);

OutlineInputBorder outlineInputBorder() {
  return OutlineInputBorder(
    borderRadius: BorderRadius.circular(15),
    borderSide: BorderSide(color: kTextColor),
  );
}

InputDecoration textInputDecoration = InputDecoration(
  enabledBorder:
      UnderlineInputBorder(borderSide: new BorderSide(color: Colors.grey)),
  focusedBorder:
      UnderlineInputBorder(borderSide: new BorderSide(color: kPrimaryColor)),
  errorBorder:
      UnderlineInputBorder(borderSide: new BorderSide(color: Colors.red)),
  focusedErrorBorder:
      UnderlineInputBorder(borderSide: new BorderSide(color: Colors.red)),
);

bool theris_internet = false;
Future<bool> isthereInternet() async {
  try {
    final result = await InternetAddress.lookup('google.com');
    if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
      theris_internet = true;
      return true;
    }
  } catch (Excaption) {}
  theris_internet = false;
  return false;
}
/*
Future<void> isLogined1() async {
  DatabaseProvider.db.getMember().then((value) => {
        CurrentMember.m_id = value.m_id,
        CurrentMember.m_account = value.m_account,
        CurrentMember.m_address = value.m_address,
        CurrentMember.m_area = value.area_name,
        CurrentMember.m_cdate = value.m_cdate,
        CurrentMember.m_comment = value.m_comment,
        CurrentMember.m_country = value.c_name,
        CurrentMember.m_email = value.m_email,
        CurrentMember.m_fname = value.m_fname,
        CurrentMember.m_gender = value.m_gender,
        CurrentMember.m_img = value.m_img,
        CurrentMember.m_lname = value.m_lname,
        CurrentMember.m_mobile = value.m_mobile,
      });
}*/
