import 'package:flutter/material.dart';
import 'package:multi_image_picker/multi_image_picker.dart';

class MultiImages extends StatefulWidget {
  @override
  _MultiImagesState createState() => _MultiImagesState();
}

class _MultiImagesState extends State<MultiImages> {
  List<Asset> images = List<Asset>();
  String _error = 'لا يوجد خطأ';

  @override
  void initState() {
    super.initState();
  }
  Widget buildGridView() {
    return GridView.count(
      crossAxisCount: 3,
      children: List.generate(images.length, (index) {
        Asset asset = images[index];
        return AssetThumb(
          asset: asset,
          width: 300,
          height: 300,
        );
      }),
    );
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();
    String error = 'لايوجد خطأ';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 300,
        enableCamera: true,
        selectedAssets: images,
       // cupertinoOptions: CupertinoOptions(takePhotoIcon: "مثال"),
        materialOptions: MaterialOptions(
          //actionBarColor: "#abcdef",
          actionBarTitle: "ddddd",
          allViewTitle: "جميع الصور ",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      images = resultList;
      _error = error;
    });
  }


  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Center(child: Text('Error: $_error')),
        RaisedButton(
          child: Text("أضف صورا لسيارتك"),
          onPressed: loadAssets,
        ),
        Expanded(
          child: buildGridView(),
        )
      ],
    );
  }
}
