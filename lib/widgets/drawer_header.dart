import 'package:flutter/material.dart';
import 'package:my_flutter_app/model/currentmember.dart';
import 'package:my_flutter_app/model/members.dart';
import 'package:my_flutter_app/pages/menu_pages/profile_page.dart';
import 'package:my_flutter_app/utils/page_routes.dart';
import 'package:my_flutter_app/widgets/stepper_body.dart';

class HeadrDrawer extends StatefulWidget {
  @override
  _HeadrDrawerState createState() => _HeadrDrawerState();
}

class _HeadrDrawerState extends State<HeadrDrawer> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return createDrawerHeader(context);
  }

  Widget createDrawerHeader(BuildContext context) {
    return DrawerHeader(
        margin: EdgeInsets.zero,
        padding: EdgeInsets.zero,
        decoration: BoxDecoration(
            image: DecorationImage(
                fit: BoxFit.fill,
                image: ExactAssetImage('assets/images/bg_second_board.png'))),
        child: Stack(
          children: <Widget>[
            Positioned(
              bottom: 70.0,
              left: 16.0,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 20),
                    child: Text(
                      "اهلا بك في عالم السيارات ",
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 25.0,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  //profile
                ],
              ),
            ),
            Positioned(
              bottom: 0,
              child: InkWell(
                child: Container(
                  width: MediaQuery.of(context).size.width * 0.74,
                  color: Colors.white,
                  child: Column(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(
                            right: 20, bottom: 10, top: 10),
                        child: CurrentMember.m_id != 0
                            ? Text('${CurrentMember.m_account}')
                            : Text(
                                'تسجيل دخول',
                                style: TextStyle(
                                  color: Colors.black,
                                ),
                                textAlign: TextAlign.right,
                              ),
                      ),
                    ],
                  ),
                ),
                onTap: () {
                  Navigator.pop(context);
                  if (CurrentMember.m_id != 0) {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return ProfilePage();
                    })).then((value) {
                      print("LLLLLLLLLLLLL $value");
                      setState(() {
                        CurrentMember.m_id = CurrentMember.m_id;
                        CurrentMember.m_account = CurrentMember.m_account;
                        CurrentMember.m_address = CurrentMember.m_address;
                        CurrentMember.m_area = CurrentMember.m_area;
                        CurrentMember.m_cdate = CurrentMember.m_cdate;
                        CurrentMember.m_comment = CurrentMember.m_comment;
                        CurrentMember.m_country = CurrentMember.m_country;
                        CurrentMember.m_email = CurrentMember.m_email;
                        CurrentMember.m_fname = CurrentMember.m_fname;
                        CurrentMember.m_gender = CurrentMember.m_gender;
                        CurrentMember.m_img = CurrentMember.m_img;
                        CurrentMember.m_lname = CurrentMember.m_lname;
                        CurrentMember.m_mobile = CurrentMember.m_mobile;
                      });
                    });
                  } else {
                    Navigator.pushNamed(context, PageRoutes.login)
                        .then((value) {
                      print("LLLLLLLLLLLLL $value");
                      setState(() {
                        CurrentMember.m_id = CurrentMember.m_id;
                        CurrentMember.m_account = CurrentMember.m_account;
                        CurrentMember.m_address = CurrentMember.m_address;
                        CurrentMember.m_area = CurrentMember.m_area;
                        CurrentMember.m_cdate = CurrentMember.m_cdate;
                        CurrentMember.m_comment = CurrentMember.m_comment;
                        CurrentMember.m_country = CurrentMember.m_country;
                        CurrentMember.m_email = CurrentMember.m_email;
                        CurrentMember.m_fname = CurrentMember.m_fname;
                        CurrentMember.m_gender = CurrentMember.m_gender;
                        CurrentMember.m_img = CurrentMember.m_img;
                        CurrentMember.m_lname = CurrentMember.m_lname;
                        CurrentMember.m_mobile = CurrentMember.m_mobile;
                      });
                    });
                  }
                },
              ),
            ),
          ],
        ));
  }
}
