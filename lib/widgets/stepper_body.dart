
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_flutter_app/api/portofolio_api.dart';
import 'package:my_flutter_app/model/currentmember.dart';
import 'package:my_flutter_app/model/portofolio.dart';
import 'package:my_flutter_app/utils/api_path.dart';
import 'package:my_flutter_app/utils/constants.dart';
import 'package:my_flutter_app/utils/page_routes.dart';
import 'package:my_flutter_app/widgets/default_button.dart';

import 'cstmtoast/toast_utils.dart';
import 'dialog.dart';

class MyData {
  String u_skills = '';
  String u_cert = '';
  String u_defination = '';
  String u_accomplishment = '';
  String u_available_day = '';
  String u_category = '';


}

List<GlobalKey<FormState>> formKeys = [GlobalKey<FormState>(),GlobalKey<FormState>(), GlobalKey<FormState>(), GlobalKey<FormState>(), GlobalKey<FormState>()];

class StepperBody extends StatefulWidget {
  static const String route_name = '/techinfo';

  @override
  _StepperBodyState createState() =>  _StepperBodyState();
}

class _StepperBodyState extends State<StepperBody> {
  int currStep = 0;
  static var _focusNode = FocusNode();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  static MyData data = MyData();
  bool isdone=false;

  @override
  void initState() {
    super.initState();
    _focusNode.addListener(() {
      setState(() {});
      print('Has focus: $_focusNode.hasFocus');
    });
  }

  /*
  @override
  void dispose() {
    _focusNode.dispose();
    super.dispose();
  }
*/


  List<Step> steps = [
    Step(
        title: const Text('عرف بنفسك '),
        isActive: true,
        //state: StepState.error,
        state: StepState.indexed,
        content: Form(
          key: formKeys[0],
          child: Column(
            children: <Widget>[
              TextFormField(
                focusNode: _focusNode,
                keyboardType: TextInputType.text,
                autocorrect: false,
                onSaved: (String value) {
                  data.u_defination = value;
                },
                //maxLines: 1,
                //initialValue: 'Aseem Wangoo',
                validator: (value) {
                  if (value.isEmpty || value.length < 1) {
                    return 'يرجى التعريف بنفسك كفني';
                  }
                },
                decoration: InputDecoration(
                    labelText: 'معلوماتك الشخصية بإختصار',
                    hintText: 'المعلومات الشخصية',
                    //filled: true,
                    //icon: const Icon(Icons.person),
                    labelStyle:
                    TextStyle(decorationStyle: TextDecorationStyle.solid)),

              ),
            ],
          ),
        )),
    Step(
        title: const Text('مهاراتـــك'),
        //subtitle: const Text('Subtitle'),
        isActive: true,
        //state: StepState.editing,
        state: StepState.indexed,
        content: Form(
          key: formKeys[1],
          child: Column(
            children: <Widget>[
              TextFormField(
                keyboardType: TextInputType.text,
                autocorrect: false,
                validator: (value) {
                  if (value.isEmpty ) {
                    return 'رجى إدخال مهاراتك';
                  }
                },
                onSaved: (String value) {
                  data.u_skills = value;
                },
                maxLines: 1,
                decoration: InputDecoration(
                    labelText: 'مهاراتـــك',
                    hintText: 'ادخل مهارة واحدة على الأقل',
                    labelStyle:
                    TextStyle(decorationStyle: TextDecorationStyle.solid)),
              ),
            ],
          ),
        )),
    Step(
        title: const Text('شهاداتـــك التي حصلت عليها '),
        // subtitle: const Text('Subtitle'),
        isActive: true,
        state: StepState.indexed,
        // state: StepState.disabled,
        content: Form(
          key: formKeys[2],
          child: Column(
            children: <Widget>[
              TextFormField(
                keyboardType: TextInputType.text,
                autocorrect: false,

                validator: (value) {
                  if (value.isEmpty ) {
                    return 'يرجى إدخال شهاداتك';
                  }
                },
                onSaved: (String value) {
                  data.u_cert = value;
                },
                maxLines: 1,
                decoration: InputDecoration(
                    labelText: 'ادخل شهاداتك كفني سيارات',
                    hintText: 'اختياري',
                    labelStyle:
                    TextStyle(decorationStyle: TextDecorationStyle.solid)),
              ),
              SizedBox(height: 10,),
              TextFormField(
                keyboardType: TextInputType.text,
                autocorrect: false,
                validator: (value) {
                  if (value.isEmpty ) {
                    return 'يرجى ادخال خبراتك العملية';
                  }
                },
                maxLines: 2,
                onSaved: (String value) {
                  data.u_accomplishment = value;
                },
                decoration: InputDecoration(
                    labelText: 'الخبرة العملية',
                    hintText: 'اذكر واحدة على الأقل ',
                    labelStyle:
                    TextStyle(decorationStyle: TextDecorationStyle.solid)),
              ),

            ],
          ),
        )),
    Step(
        title: const Text('أيام العمل المناسبـــة لـك '),
        // subtitle: const Text('Subtitle'),
        isActive: true,
        state: StepState.indexed,
        content: Form(
          key: formKeys[3],
          child: Column(
            children: <Widget>[
              TextFormField(
                keyboardType: TextInputType.text,
                autocorrect: false,
                validator: (value) {
                  if (value.isEmpty ) {
                    return 'يرجى إدخال الايام المتاح فيها للعمل';
                  }
                },
                onSaved: (String value) {
                  data.u_available_day = value;
                },
                decoration: InputDecoration(
                    labelText: ' الأيام المتوفرة',
                    hintText: 'الأيام',
                    labelStyle:
                    TextStyle(decorationStyle: TextDecorationStyle.solid)),
              ),
            ],
          ),
        ),
    ),
    Step(
      title: const Text('تخصصك المهني'),
      // subtitle: const Text('Subtitle'),
      isActive: true,
      state: StepState.indexed,
      content: Form(
        key: formKeys[4],
        child: Column(
          children: <Widget>[
            TextFormField(
              keyboardType: TextInputType.number,
              autocorrect: false,
              validator: (value) {
                if (value.isEmpty ) {
                  return 'يرجى إدخال تخصصك .. كهرباء ميكنيك على سبيل المثال';
                }
              },
              onSaved: (String value) {
                data.u_category = value;
              },
              decoration: InputDecoration(
                  labelText: 'كهربائي.. ميكانيكي .. الخ',
                  hintText: 'تخصصك',
                  labelStyle:
                  TextStyle(decorationStyle: TextDecorationStyle.solid)),
            ),
          ],
        ),
      ),
    ),
  ];

  @override
  Widget build(BuildContext context) {
    void showSnackBarMessage(String message,
        [MaterialColor color = Colors.red]) {
      Scaffold
          .of(context)
          .showSnackBar(SnackBar(content: Text(message)));
    }

    void _submitDetails() {
      final FormState formState = _formKey.currentState;

      if (!formState.validate()) {
        showSnackBarMessage('يرجى إدخال معلوماتك بشكل صحيح');
      } else {
        formState.save();
        print("u_defination: ${data.u_defination}");
        print("u_skills: ${data.u_skills}");
        print("u_cert: ${data.u_cert}");
        print("u_accomplishment: ${data.u_accomplishment}");
        print("u_category: ${data.u_category}");

        showDialog(
            context: context,
            child: AlertDialog(
              title: Text("Details"),
              //content:  Text("Hello World"),
              content: SingleChildScrollView(
                child: ListBody(
                  children: <Widget>[
                    Text("your_defination : " + data.u_defination),
                    Text("your skills : " + data.u_skills),
                    Text("your certifications : " + data.u_cert),
                    Text("your expierment : " + data.u_accomplishment),
                    Text("your category : " + data.u_category),

                  ],
                ),
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('تم'),
                  onPressed: () async {
                    PortfolioApi portfolioApi = PortfolioApi();
                    Dialogs.showLoadingDialog(context, _keyLoader, "جاري الإرسال");
                    Portfolio portfolio = Portfolio(
                        u_defination: data.u_defination,
                        u_skills: data.u_skills,
                        u_cert: data.u_cert,
                        u_accomplishment:data.u_accomplishment,
                        u_available_day:data.u_available_day,
                         u_category: data.u_category,);
                    var status = await portfolioApi.sendPortfolioInfo(portfolio);
                    if (status == 1) {

                      ToastUtils.showCustomToast(context, "تم الإرسال بنجاح");
                      Navigator.pop(context);
                    } else if (status == 0 || status == -9) {
                      ToastUtils.showCustomToast(context, "لم يتم الإرسال يرجى إعادة المحاولة");
                    }
                    Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                        .pop();

                   // Navigator.of(context).pop();
                  },
                ),
              ],
            ));
      }
    }
    return Scaffold(
      appBar: AppBar(
        centerTitle: false,
        title: Text('بياناتك الشخصية'),
      ),
      body: Container(
          child: Form(
            key: _formKey,
            child: ListView(children: <Widget>[

              Stepper(
                steps: steps,
                type: StepperType.vertical,
                currentStep: this.currStep,
                onStepContinue: () {
                  setState(() {
                    if (formKeys[currStep].currentState.validate()) {
                      if (currStep < steps.length - 1) {
                        currStep = currStep + 1;
                      } else {
                        currStep = 0;
                      }
                    }
                    // else {
                    // Scaffold
                    //     .of(context)
                    //     .showSnackBar( SnackBar(content:  Text('$currStep')));

                    // if (currStep == 1) {
                    //   print('First Step');
                    //   print('object' + FocusScope.of(context).toStringDeep());
                    // }
                    // }
                  });
                },
                onStepCancel: () {
                  setState(() {
                    if (currStep > 0) {
                      currStep = currStep - 1;
                    } else {
                      currStep = 0;
                    }
                  });
                },
                onStepTapped: (step) {
                  setState(() {
                    currStep = step;
                  });
                },

                controlsBuilder: (BuildContext context,
                    {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                  return Row(
                    children: <Widget>[
                  currStep== steps.length-1 ?  FlatButton(
                    onPressed:(){
                      setState(() {
                        isdone=true;
                      });
                      },
                    child: const Text('تم',style: TextStyle(color: kPrimaryColor),),):

                  FlatButton(
                        onPressed: onStepContinue,
                        child: const Text('التالي',style: TextStyle(color: kPrimaryColor),),
                      ),
                      currStep==0  ? Container(): FlatButton(
                        onPressed: onStepCancel,
                        child: const Text('السابق',style: TextStyle(color: kPrimaryColor),),
                      ),
                    ],
                  );
                },
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 30, right: 30, bottom: 100),

                child: isdone ? Column(
                  children: [
                    Text('اضغط على ارسال لارسال البيانات أو السابق لتعديلها '),
                    DefaultButton(
                        text: "إرسال",
                        press: () {
                          _submitDetails();
                        }
                    ),
                  ],
                ):Container(),
              ),
            ]),
          )),
    );
  }


}