import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import '../utils/constants.dart';

class NoInternetWiew extends StatelessWidget {
  const NoInternetWiew({
    Key key,
    this.press,
  }) : super(key: key);
  final Function press;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Container(
          width: 150,
          height: 150,
          decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('assets/images/noNetwork.png')),
          ),
        ),
        SizedBox(
          height: 10,
        ),
        Text(
          'يبدو انه لايوجد اتصال بالانترنت',
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
        ),
        SizedBox(
          height: 10,
        ),
        Text('تأكد من اتصالك ثم اعد تحميل الصفحة'),
        SizedBox(
          height: 10,
        ),
        SizedBox(
          width: 270,
          height: 50,
          child: FlatButton(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(20)),
            color: kPrimaryColor,
            onPressed: press,
            child: Text(
              'إعادة تحميل الصفحة',
              style: TextStyle(
                fontSize: 18,
                color: Colors.white,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
