import 'package:flutter/material.dart';
import 'package:my_flutter_app/pages/menu_pages/accessoriespage.dart';
import 'package:my_flutter_app/pages/other_pages/bbsoft_info.dart';
import 'package:my_flutter_app/pages/other_pages/favorite_page.dart';
import 'package:my_flutter_app/pages/menu_pages/register_page.dart';
import 'package:my_flutter_app/pages/menu_pages/tangeed_page.dart';
import 'package:my_flutter_app/pages/home_page2.dart';
import 'package:my_flutter_app/pages/on_boarding_pages/onboarding.dart';
import 'package:my_flutter_app/pages/other_pages/splash_page.dart';
import 'package:my_flutter_app/pages/pop_down_pages/about.dart';
import 'package:my_flutter_app/pages/tab_pages/cars_technicians_view.dart';
import 'package:my_flutter_app/utils/my_theme.dart';
import 'package:my_flutter_app/widgets/stepper_body.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:splashscreen/splashscreen.dart';
import 'pages/menu_pages/products_view.dart';
import 'pages/other_pages/add_car.dart';
import 'pages/other_pages/add_part.dart';
import 'pages/other_pages/settings_page.dart';
import 'pages/menu_pages/login_page.dart';
import 'pages/home_page.dart';
import 'pages/other_pages/notifications_page.dart';
import 'pages/menu_pages/profile_page.dart';
import 'pages/other_pages/favorite_page.dart';
import 'pages/other_pages/shop_cart_page.dart';
import 'pages/tab_pages/all_brands_view.dart';
import 'pages/menu_pages/hrag_page.dart';
import 'utils/page_routes.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

bool seen;
void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  seen = prefs.getBool('seen');
  if (seen == null) seen = false;
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      localizationsDelegates: [
        GlobalCupertinoLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        Locale("fa", "IR"),
      ],
      locale: Locale("fa", "IR"),

      debugShowCheckedModeBanner: false,
      title: 'Car App',
      theme: myThemeData(),
      initialRoute: (!seen) ? "first" : "splash",
      routes: {
        'splash': (context) => Splash(),
        '/': (context) => HomePage(),
        "first": (context) => OnBoarding(),
        PageRoutes.home: (context) => CarPage(),
        PageRoutes.homepage: (context) => HomePage(),
        PageRoutes.splash: (context) => Splash(),
        PageRoutes.setting: (context) => Settings(),
        PageRoutes.login: (context) => LoginPage(),
        PageRoutes.profile: (context) => ProfilePage(),
        PageRoutes.notification: (context) => NotificationPage(),
        PageRoutes.register: (context) => Register(),
        PageRoutes.hrag: (context) => HragPage(0),
        PageRoutes.technicians: (context) => CarsTechniciansView(),
        PageRoutes.accessories: (context) => AccessoriesPage(),
        PageRoutes.tangeed: (context) => TangeedPage(),
        PageRoutes.favorite: (context) => Favorite_Page(),
        PageRoutes.shopcart: (context) => ShopCart(),
        PageRoutes.allbrands: (context) => AllBrands(),
        PageRoutes.carpage: (context) => ProductsView(),
        PageRoutes.addcar: (context) => AddNewCar(),
        PageRoutes.addpart: (context) => AddNewPart(),
        PageRoutes.about: (context) => AboutPage(),
        PageRoutes.bbsoft: (context) => BBSoft(),
        PageRoutes.tech_info: (context) => StepperBody(),

        //PageRoutes.details:(context)=>ProductDetails(),
      },
      //initialRoute: PageRoutes.on_boarding,
    );
  }
}

/*
void main() async

{
  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool seen = prefs.getBool('seen');
  Widget _screen;
  if( seen != null || seen ==false){
    _screen=OnBoarding();
  }
  else{
    //Goto home
    _screen = CarPage();
  }
  runApp(MyApp(_screen));
}

class MyApp extends StatelessWidget {
  final Widget _screen;
  MyApp(this._screen);
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: myThemeData,
        home: this._screen,
*/
