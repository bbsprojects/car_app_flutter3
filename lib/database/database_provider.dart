import 'dart:convert';

import 'package:my_flutter_app/model/area.dart';
import 'package:my_flutter_app/model/brand.dart';
import 'package:my_flutter_app/model/carmodel.dart';
import 'package:my_flutter_app/model/favorite.dart';
import 'package:my_flutter_app/model/members.dart';
import 'package:my_flutter_app/model/parts.dart';
import 'package:my_flutter_app/model/products.dart';
import 'package:my_flutter_app/model/shopping_cart.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class DatabaseProvider {
  static const String tableMember = "members";
  static const String m_id = "m_id";
  static const String m_fname = "	m_fname";
  static const String m_lname = "m_lname";
  static const String m_gender = "m_gender";
  static const String m_status = "m_status";
  static const String m_account = "m_account";
  static const String m_pwd = "m_pwd";
  static const String m_mobile = "m_mobile";
  static const String m_email = "m_email";
  static const String m_country = "m_country";
  static const String m_area = "m_area";
  static const String m_address = "m_address";
  static const String m_utype = "m_utype";
  static const String m_cdate = "m_cdate";
  static const String m_comment = "m_comment";
  static const String m_img = "m_img";
  static const String m_token = "token";

  static const String tableShppingCart = "shppingcart";
  static const String s_id = "s_id";
  static const String s_prd_id = "prd_id";
  static const String s_part_id = "part_id";
  static const String s_qty = "s_qty";
  static const String s_date = "s_date";

  static const String tableFavorite = "Favorite";
  static const String f_id = "f_id";
  static const String f_prt = "f_prt";
  static const String f_prd = "f_prd";
  static const String f_date = "f_date";

  static const String tableCountry = "countries";
  static const String c_id = "c_id";
  static const String c_name = "c_name";
  static const String c_latLang = "c_latLang";

  static const String tableArea = "area";
  static const String area_id = "area_id";
  static const String area_name = "area_name";
  static const String area_latLang = "area_latLang";
  static const String country = "country";

  static const String tableProducts = "products";
  static const String p_id = "p_id";
  static const String p_name = "p_name";
  static const String is_sold = "is_sold";
  static const String p_model = "p_model";
  static const String vin_no = "vin_no";
  static const String is_new = "is_new";
  static const String p_mfr = "p_mfr";
  static const String p_detailes = "p_detailes";
  static const String p_comment = "p_comment";
  static const String p_cdate = "p_cdate";
  static const String p_brand = "p_brand";
  static const String p_currency = "p_currency";
  static const String p_price = "p_price";
  static const String p_img = "p_img";
  static const String is_available = "is_available";
  static const String is_haraj = "is_haraj";
  static const String p_createdBy = "p_createdBy";
  static const String p_tag = "p_tag";
  static const String p_mileage = "p_mileage";
  static const String p_trans = "p_trans";
  static const String p_fuel = "p_fuel";
  static const String p_year = "p_year";
  static const String is_orginal = "is_orginal";

  static const String tableParts = "parts";
  static const String pr_id = "pr_id";
  static const String pr_name = "pr_name";
  static const String pr_is_sold = "is_sold";
  static const String pr_model = "pr_model";
  static const String pr_vin_no = "vin_no";
  static const String pr_is_new = "is_new";
  static const String pr_mfr = "pr_mfr";
  static const String pr_detailes = "pr_detailes";
  static const String pr_comment = "pr_comment";
  static const String pr_cdate = "pr_cdate";
  static const String pr_brand = "pr_brand";
  static const String pr_currency = "pr_currency";
  static const String pr_price = "pr_price";
  static const String pr_img = "pr_img";
  static const String pr_is_available = "is_available";
  static const String pr_is_haraj = "is_haraj";
  static const String pr_createdBy = "pr_createdBy";
  static const String pr_tag = "pr_tag";
  static const String pr_year = "pr_year";
  static const String pr_type = "pr_type";
  static const String pr_is_orginal = "is_orginal";

  static const String tableNotifyCategory = "notifycategory";
  static const String nc_id = "nc_id";
  static const String nc_type = "nc_type";
  static const String nc_desc = "nc_desc";

  static const String tableNotification = "notification";
  static const String n_id = "n_id";
  static const String n_cat = "n_cat";
  static const String n_cdate = "n_cdate";
  static const String n_desc = "n_desc";
  static const String n_content = "n_content";
  static const String n_createdBy = "n_createdBy";
  static const String is_seen = "is_seen";

  static const String tableBrands = "brand";
  static const String b_id = "b_id";
  static const String b_title = "b_title";
  static const String b_def = "b_def";
  static const String b_img = "b_img";
  static const String b_sort = "b_sort";
  static const String b_type = "b_type";

  static const String tableModels = "models";
  static const String mo_id = "mo_id";
  static const String mo_title = "mo_title";
  static const String mo_brand = "mo_brand";

  DatabaseProvider._();
  static final DatabaseProvider db = DatabaseProvider._();

  Database _database;

  Future<Database> get database async {
    print("database getter called");

    if (_database != null) {
      return _database;
    }

    _database = await createDatabase();

    return _database;
  }

  Future<Database> createDatabase() async {
    String dbPath = await getDatabasesPath();

    return await openDatabase(
      join(dbPath, 'cardb17.db'),
      version: 1,
      onCreate: onCreate,
    );
  }

  void onCreate(Database db, int version) async {
    print("Creating car DB table");
    await db.execute(
        'CREATE TABLE $tableShppingCart ($s_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, $s_part_id INTEGER, $s_prd_id INTEGER, $s_qty STRING, $s_date STRING)');
    await db.execute(
        'CREATE TABLE $tableMember ($m_id INTEGER PRIMARY KEY NOT NULL, $m_fname STRING, $m_lname STRING,$m_gender STRING, $m_status INTEGER,$m_account STRING,$m_pwd STRING, $m_mobile STRING,$m_email STRING,$m_country INTEGER,$m_area INTEGER, $m_address STRING, $m_utype INTEGER,$m_cdate STRING,$m_comment STRING,$m_img STRING,$m_token STRING,$c_name STRING,$area_name STRING)');

    await db.execute(
        'CREATE TABLE $tableCountry ($c_id INTEGER PRIMARY KEY NOT NULL, $c_name STRING, $c_latLang STRING)');
    await db.execute(
        'CREATE TABLE $tableArea ($area_id INTEGER PRIMARY KEY NOT NULL, $area_name STRING, $area_latLang STRING, $country INTEGER)');
    await db.execute(
        'CREATE TABLE $tableProducts ($p_id INTEGER PRIMARY KEY NOT NULL, $p_name STRING, $is_sold INTEGER,$p_model INTEGER,$vin_no STRING, $is_new INTEGER,$p_mfr INTEGER,$p_detailes STRING, $p_comment STRING,$p_cdate STRING,$p_brand INTEGER,$p_currency INTEGER, $p_price DOUBLE, $p_img STRING,$is_available INTEGER,$is_haraj INTEGER,$p_createdBy INTEGER,$p_tag STRING,$p_mileage STRING,$p_trans INTEGER,$p_fuel INTEGER,$p_year STRING,$is_orginal INTEGER,name STRING,m_img STRING,trans STRING,fuel STRING,currency STRING, brand STRING, model STRING)');

    await db.execute(
        'CREATE TABLE $tableParts ($pr_id INTEGER PRIMARY KEY NOT NULL, $pr_name STRING, $pr_is_sold INTEGER,$pr_model INTEGER,$pr_vin_no STRING, $pr_is_new INTEGER,$pr_mfr INTEGER,$pr_detailes STRING,$pr_comment STRING,$pr_cdate STRING,$pr_brand INTEGER,$pr_currency INTEGER, $pr_price DOUBLE, $pr_img STRING,$pr_is_available INTEGER,$pr_is_haraj INTEGER,$pr_createdBy INTEGER,$pr_tag STRING,$pr_year STRING,$pr_type INTEGER,$pr_is_orginal INTEGER,m_img STRING,name STRING,brand STRING,model STRING,currency STRING)');

    await db.execute(
        'CREATE TABLE $tableFavorite ($f_id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, $f_prd INTEGER, $f_prt INTEGER,$f_date STRING)');
    await db.execute(
        'CREATE TABLE $tableBrands ($b_id INTEGER PRIMARY KEY NOT NULL, $b_title STRING, $b_def STRING, $b_img STRING, $b_sort INTEGER, $b_type STRING)');
    await db.execute(
        'CREATE TABLE $tableModels ($mo_id INTEGER PRIMARY KEY NOT NULL,$mo_title STRING, $mo_brand INTEGER)');
  }

  ///Favorite
  Future<List<Favorite>> getFavorite() async {
    Database _db = await this.database;
    var items = await _db.query(tableFavorite);

    List<Favorite> listitem = List<Favorite>();

    items.forEach((currentItem) {
      Favorite itm = Favorite.fromMap(currentItem);
      listitem.add(itm);
    });

    return listitem;
  }

  Future<int> insertFavorite(Favorite itm) async {
    Database _db = await this.database;
    return await _db.insert(tableFavorite, itm.toMap());
  }

  Future<int> deleteFavoritePart(String no) async {
    Database _db = await this.database;
    return await _db
        .delete(tableFavorite, where: '$f_prt = ?', whereArgs: [no]);
  }

  Future<int> deleteFavoritePoduct(String no) async {
    Database _db = await this.database;
    return await _db
        .delete(tableFavorite, where: '$f_prd = ?', whereArgs: [no]);
  }

  Future<int> deleteFavorite(Favorite itm) async {
    Database _db = await this.database;
    return await _db
        .delete(tableFavorite, where: '$f_id = ?', whereArgs: [itm.f_id]);
  }

  Future<bool> isExistinFavtProduct(String no) async {
    Database _db = await this.database;
    var result =
        await _db.rawQuery("Select * from $tableFavorite Where $f_prd =$no");
    if (result.isEmpty)
      return false;
    else
      return true;
  }

  Future<bool> isExistinFavtPart(String no) async {
    Database _db = await this.database;
    var result =
        await _db.rawQuery("Select * from $tableFavorite Where $f_prt =$no");
    if (result.isEmpty)
      return false;
    else
      return true;
  }

/////

  ///ShoppingCart
  Future<List<ShoppingCart>> getcarts() async {
    Database _db = await this.database;
    var items = await _db.query(tableShppingCart);

    List<ShoppingCart> listitem = List<ShoppingCart>();

    items.forEach((currentItem) {
      ShoppingCart itm = ShoppingCart.fromMap(currentItem);
      listitem.add(itm);
    });

    return listitem;
  }

  Future<List<Map<String, dynamic>>> getPartbynoincart(String no) async {
    Database _db = await this.database;
    return _db
        .rawQuery("Select * from $tableShppingCart Where $s_part_id =$no");
  }

  Future<bool> isExistincartPart(String no) async {
    Database _db = await this.database;
    var result = await _db
        .rawQuery("Select * from $tableShppingCart Where $s_part_id =$no");
    if (result.isEmpty)
      return false;
    else
      return true;
  }

  Future<int> insertShoppingcart(ShoppingCart itm) async {
    Database _db = await this.database;
    return await _db.insert(tableShppingCart, itm.toMap());
  }

  Future<int> updateitem(ShoppingCart itm) async {
    Database _db = await this.database;
    return await _db.update(tableShppingCart, itm.toMap(),
        where: '$s_id = ?', whereArgs: [itm.s_id]);
  }

  Future<int> deleteitem(ShoppingCart itm) async {
    Database _db = await this.database;
    return await _db
        .delete(tableShppingCart, where: '$s_id = ?', whereArgs: [itm.s_id]);
  }

  Future<int> getCountShoppincart() async {
    Database _db = await this.database;
    List<Map<String, dynamic>> x =
        await _db.rawQuery("Select Count($s_id) From $tableShppingCart");
    int count = Sqflite.firstIntValue(x);
    return count;
  }

  Future<int> deleteAllShoppingcart() async {
    Database _db = await this.database;
    return await _db
        .delete(tableShppingCart, where: '$s_id > ?', whereArgs: [0]);
  }

////Member

  Future<Members> getMember() async {
    Database _db = await this.database;
    var result = await _db.rawQuery("Select * from $tableMember ");
    Members member = Members();
    result.forEach((currentMem) {
      Members m = Members.fromMap(currentMem);
      member = m;
    });

    return member;
  }

  Future<int> insertMember(Members member) async {
    Database _db = await this.database;
    return await _db.insert(tableMember, member.toMap());
  }

  Future<int> updateMember(Members member) async {
    Database _db = await this.database;
    return await _db.update(tableMember, member.toMap(),
        where: '$m_id = ?', whereArgs: [member.m_id]);
  }

  Future<int> deleteMember(Members member) async {
    Database _db = await this.database;
    return await _db
        .delete(tableMember, where: '$m_id = ?', whereArgs: [member.m_id]);
  }

  //// Brands
  Future<List<Brand>> getBrands() async {
    Database _db = await this.database;
    var result = await _db.rawQuery("Select * from $tableBrands ");
    List<Brand> brand = List<Brand>();
    result.forEach((currentMem) {
      Brand bb = Brand.fromMap(currentMem);
      brand.add(bb);
    });

    return brand;
  }

  Future<int> insertBrand(Brand brand) async {
    Database _db = await this.database;
    return await _db.insert(tableBrands, brand.toMap());
  }

  Future<int> updateBrand(Brand brand) async {
    Database _db = await this.database;
    return await _db.update(tableBrands, brand.toMap(),
        where: '$b_id = ?', whereArgs: [brand.b_id]);
  }

  Future<int> deleteBrand(Brand brand) async {
    Database _db = await this.database;
    return await _db
        .delete(tableBrands, where: '$b_id = ?', whereArgs: [brand.b_id]);
  }

  Future<bool> isExistinBrand(String no) async {
    Database _db = await this.database;
    var result =
        await _db.rawQuery("Select * from $tableBrands Where $b_id =$no");
    if (result.isEmpty)
      return false;
    else
      return true;
  }
  ////
  ///

  //// Models
  Future<List<Module>> getModels() async {
    Database _db = await this.database;
    var result = await _db.rawQuery("Select * from $tableModels ");
    List<Module> modl = List<Module>();
    result.forEach((currentMem) {
      Module mm = Module.fromMap(currentMem);
      modl.add(mm);
    });

    return modl;
  }

  Future<List<Module>> getModelsbyBrand(String brand) async {
    Database _db = await this.database;
    var result =
        await _db.rawQuery("Select * from $tableModels Where $mo_brand=$brand");
    List<Module> modl = List<Module>();
    result.forEach((currentMem) {
      Module mm = Module.fromMap(currentMem);
      modl.add(mm);
    });

    return modl;
  }

  Future<int> insertModel(Module modl) async {
    Database _db = await this.database;
    return await _db.insert(tableModels, modl.toMap());
  }

  Future<int> updateModel(Module modl) async {
    Database _db = await this.database;
    return await _db.update(tableModels, modl.toMap(),
        where: '$mo_id = ?', whereArgs: [modl.mo_id]);
  }

  Future<int> deleteModel(Module modl) async {
    Database _db = await this.database;
    return await _db
        .delete(tableModels, where: '$mo_id = ?', whereArgs: [modl.mo_id]);
  }

  Future<bool> isExistinModel(String no) async {
    Database _db = await this.database;
    var result =
        await _db.rawQuery("Select * from $tableModels Where $mo_id =$no");
    if (result.isEmpty)
      return false;
    else
      return true;
  }
  ////

  //// Country
  Future<City> getCountries() async {
    Database _db = await this.database;
    var result = await _db.rawQuery("Select * from $tableCountry ");
    City city = City();
    result.forEach((currentMem) {
      City cc = City.fromMap(currentMem);
      city = cc;
    });

    return city;
  }

  Future<int> insertCountry(City city) async {
    Database _db = await this.database;
    return await _db.insert(tableCountry, city.toMap());
  }

  Future<int> updateCountry(City city) async {
    Database _db = await this.database;
    return await _db.update(tableCountry, city.toMap(),
        where: '$c_id = ?', whereArgs: [city.c_id]);
  }

  Future<int> deleteCountry(City city) async {
    Database _db = await this.database;
    return await _db
        .delete(tableCountry, where: '$c_id = ?', whereArgs: [city.c_id]);
  }
  ////

  //// Area
  Future<Area> getArea() async {
    Database _db = await this.database;
    var result = await _db.rawQuery("Select * from $tableArea ");
    Area area = Area();
    result.forEach((currentMem) {
      Area aa = Area.fromMap(currentMem);
      area = aa;
    });

    return area;
  }

  Future<int> insertArea(Area area) async {
    Database _db = await this.database;
    return await _db.insert(tableArea, area.toMap());
  }

  Future<int> updateArea(Area area) async {
    Database _db = await this.database;
    return await _db.update(tableCountry, area.toMap(),
        where: '$area_id = ?', whereArgs: [area.area_id]);
  }

  Future<int> deleteArea(Area area) async {
    Database _db = await this.database;
    return await _db
        .delete(tableArea, where: '$area_id = ?', whereArgs: [area.area_id]);
  }
  ////

  ////Products
  Future<int> insertProduct(Products product) async {
    Database _db = await this.database;
    return await _db.insert(tableProducts, product.toMap());
  }

  Future<int> deleteProduct(Products product) async {
    Database _db = await this.database;
    return await _db
        .delete(tableProducts, where: '$p_id = ?', whereArgs: [product.p_id]);
  }

  Future<List<Products>> getFavoriteProducts() async {
    Database _db = await this.database;
    var items = await _db.rawQuery("Select * From $tableProducts");
    List<Products> listitem = List<Products>();
    items.forEach((currentItem) {
      Products itm = Products.fromMap(currentItem);
      listitem.add(itm);
    });
    return listitem;
  }

  ///Parts
  Future<int> insertParts(Parts part) async {
    Database _db = await this.database;
    return await _db.insert(tableParts, part.toMap());
  }

  Future<int> deleteParts(Parts part) async {
    Database _db = await this.database;
    return await _db
        .delete(tableParts, where: '$pr_id = ?', whereArgs: [part.pr_id]);
  }
}
