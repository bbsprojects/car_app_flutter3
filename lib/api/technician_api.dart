import 'dart:convert';

import 'package:my_flutter_app/database/database_provider.dart';
import 'package:my_flutter_app/model/brand.dart';
import 'package:my_flutter_app/model/members.dart';
import 'package:my_flutter_app/utils/api_path.dart';
import 'package:http/http.dart' as http;

class TechniciansApi {
  Future<List<Members>> fetchAllTechnicians(count) async {
    List<Members> members = List<Members>();

    String uri = base_api + get_technician;

    try {
      var response = await http.get(uri);
      print('response status: ,${response.statusCode}');
      if (response.statusCode == 200) {

        var technicians = jsonDecode(response.body);
        print('body ${response.body}');
        for (var item in technicians) {
          Members m = Members.fromMap(item);
          members.add(m);

          print(('technician Name:${Members.fromMap(item).m_account}'));
          print(('technician type:${Members.fromMap(item).m_utype}'));

        }
      }
      return members;
    } catch (Excaption) {
      return members;
    }
  }
}
