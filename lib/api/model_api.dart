import 'dart:convert';

import 'package:my_flutter_app/database/database_provider.dart';
import 'package:my_flutter_app/model/carmodel.dart';
import 'package:my_flutter_app/utils/api_path.dart';
import 'package:http/http.dart' as http;

class ModuleApi {
  Future<List<Module>> fetchAllModelbyBrand(int brand_n0) async {
    List<Module> modules_items = List<Module>();

    String uri =
        base_api + get__models_dependon_brand_api + brand_n0.toString();

    try {
      var response = await http.get(uri);
      print('response status: ,${response.statusCode}');
      if (response.statusCode == 200) {
        var jsonData = jsonDecode(response.body);
        print('body ${response.body}');

        var modules = jsonData;
        for (var item in modules) {
          modules_items.add(Module.fromMap(item));

          print(('module Name:${Module.fromMap(item).mo_title}'));
        }
      }
      return modules_items;
    } catch (Excaption) {
      return modules_items;
    }
  }

  Future<List<Module>> fetchAllModel() async {
    List<Module> modules_items = List<Module>();

    String uri = base_api + get__models;

    try {
      var response = await http.get(uri);
      print('response status: ,${response.statusCode}');
      if (response.statusCode == 200) {
        var jsonData = jsonDecode(response.body);
        print('body ${response.body}');

        var modules = jsonData;
        for (var item in modules) {
          Module mm = Module.fromMap(item);
          modules_items.add(mm);
          DatabaseProvider.db
              .isExistinModel(mm.mo_id.toString())
              .then((value) => {
                    if (value)
                      {DatabaseProvider.db.updateModel(mm)}
                    else
                      {DatabaseProvider.db.insertModel(mm)}
                  });
          print(('module Name:${Module.fromMap(item).mo_title}'));
        }
      }
      return modules_items;
    } catch (Excaption) {
      return modules_items;
    }
  }
}
