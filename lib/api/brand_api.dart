import 'dart:convert';

import 'package:my_flutter_app/database/database_provider.dart';
import 'package:my_flutter_app/model/brand.dart';
import 'package:my_flutter_app/utils/api_path.dart';
import 'package:http/http.dart' as http;

class BrandApi {
  Future<List<Brand>> fetchAllBrand() async {
    List<Brand> brand_items = List<Brand>();

    String uri = base_api + get_brands;

    try {
      var response = await http.get(uri);
      print('response status: ,${response.statusCode}');
      if (response.statusCode == 200) {
        var jsonData = jsonDecode(response.body);
        print('body ${response.body}');

        var brands = jsonData;
        for (var item in brands) {
          Brand bb = Brand.fromMap(item);
          brand_items.add(bb);

          DatabaseProvider.db
              .isExistinBrand(bb.b_id.toString())
              .then((value) => {
                    if (value)
                      DatabaseProvider.db.updateBrand(bb)
                    else
                      DatabaseProvider.db.insertBrand(bb)
                  });

          print(('Brand Name:${Brand.fromMap(item).b_title}'));
        }
      }
      return brand_items;
    } catch (Excaption) {
      return brand_items;
    }
  }
}
