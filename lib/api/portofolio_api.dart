import 'package:http/http.dart' as http;
import 'package:my_flutter_app/model/currentmember.dart';
import 'dart:convert';

import 'package:my_flutter_app/utils/api_path.dart';
class PortfolioApi {
  String token;

  Future<int> sendPortfolioInfo(prtf) async {
    String uri = base_api + portfolio;
    token = CurrentMember.token;

    Map<String, String> headers = {
      'Content-Type': 'application/json ',
      'accept': 'application/json ',
      'token': 'bearer ' + token,
    };
    try {
      var response = await http.post(uri,
          headers: headers, body: json.encode(prtf.toMap()));

      String portfolio = json.encode(prtf.toMap());
      print('Response status: ${response.statusCode}');
      //print('body map:$portfolio');

      // check status code
      if (response.statusCode == 200) {
        print('Response body: ${response.body}');
        var jsonData = jsonDecode(response.body);
        print('json Data: ${jsonData}');

        return 1;
      }

      return 0;
    } catch (Exception) {
      return -9;
    }
  }
}