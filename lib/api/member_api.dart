import 'package:http/http.dart' as http;
import 'package:my_flutter_app/model/members.dart';
import 'dart:convert';
import 'package:my_flutter_app/database/database_provider.dart';
import 'package:my_flutter_app/utils/api_path.dart';
import 'package:shared_preferences/shared_preferences.dart';

class MemberApi {
  SharedPreferences sharedPreferences;
// method for login///////////////////
  Future<int> sendLoginInfo(String phone, String pass) async {
    String login_api = base_api + credentials_login_api;
    print(login_api);

    Map<String, String> headers = {
      'Content-Type': 'application/json ',
      'accept': 'application/json '
    };

    Map<String, dynamic> bdy = {'m_mobile': phone, 'm_pwd': pass};
    try {
      // set user with info from register screen
      var response =
          await http.post(login_api, headers: headers, body: json.encode(bdy));
      print('Response status: ${response.statusCode}');
      Members item = Members();
      if (response.statusCode == 200) {
        var jsonData = json.decode(response.body);
        print('json Data: ${jsonData}');
        item = Members.fromMap(jsonData);
        print('item status: ${item.m_status}');
        print('item token: ${item.token}');
        /* get only the array data:
      var data = jsonData['data'];
      print(data); // get all objects and info*/
        sharedPreferences = await SharedPreferences.getInstance();
        sharedPreferences.setString("token", item.token);
        sharedPreferences.setInt("m_id", item.m_id);
        await DatabaseProvider.db.insertMember(item);
        print('token form sp:${sharedPreferences.get('token')}');
        return item.m_status;
      }
      return 0;
    } catch (Exception) {
      return -9;
    }
  }

// method for regester ///////////////////
  Future<int> sendRegestrationInfo(Members member) async {
    String regester_api = base_api + new_member_abi;

    // set user with info from register screen
    Members member1 = member;
    try {
      var response = await http.post(regester_api,
          headers: <String, String>{
            'Content-Type': 'application/json ',
            'accept': 'application/json ',
          },
          body: json.encode(member1.toMap()));

      String gg = json.encode(member1.toMap());
      print('Response status: ${response.statusCode}');
      print('body map:$gg');
      // chech status code
      if (response.statusCode == 200) {
        print('Response body: ${response.body}');
        var jsonData = jsonDecode(response.body);
        bool status = jsonDecode(response.body)['status'];
        print('json Data: ${jsonData}');
        print('status: ${status}');
        if (status)
          return 1;
        else
          return 0;

        /*
      // if there is name for list of json data
      // get only the array data:
      var data = jsonData['data'];
      //regester_opearation=data.toString();
      for (var item in data) {
        print(item.toString()); // get list of full name
        regester_opearation = item.toString();
        Members.fromMap(item);
      }*/

      }

      return 0;
    } catch (Exception) {
      return -9;
    }
  }
}
