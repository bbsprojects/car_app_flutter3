import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:my_flutter_app/model/parts.dart';
import 'package:my_flutter_app/utils/api_path.dart';
class PartsApi {
  // munya ////////////////////////////////
  String token;
  // first method
  Future<List<Parts>> fetchAllParts(int count) async {
    List<Parts> parts_items = List<Parts>();
    String uri = base_api + get_parts_in_stores + count.toString();
    Map<String, String> headers = {
      'Content-Type': 'application/json ',
      'accept': 'application/json ',
      //  'token' :'bearer '+token,
    };
    try {
      var response = await http.get(uri, headers: headers);
      print('response status: ${response.statusCode}');

      if (response.statusCode == 200) {
        var jsonData = jsonDecode(response.body);
        print('body ${response.body}');

        // other way
        var parts = jsonData;
        for (var item in parts) {
          parts_items.add(Parts.fromMap(item));
          print(('productName:${Parts.fromMap(item).pr_name}'));
        }
      } else {
        print('error 0 : response is not 200 ');
      }
    } catch (Excaption) {
      print('error -9  ');
    }
    print (parts_items.length.toString());
    return parts_items;
  }
  // second method
  Future<String> sendPartsInfo(part) async {
    String uri = base_api + create_part_api;
    token =
    "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJCQlNPRlQiLCJuYmYiOjE2MDk0NTkyMDEsImV4cCI6MTYwOTQ1OTIwMX0.fUM6RS15v4om99JYU8lX6KqqZJf0hxj9lPy2zuxWE_s";

    Map<String, String> headers = {
      'Content-Type': 'application/json ',
      'accept': 'application/json ',
      'token': 'bearer ' + token,
    };
    try {
      var response = await http.post(uri,
          headers: headers, body: json.encode(part.toMap()));

      String gg = json.encode(part.toMap());
      print('Response status: ${response.statusCode}');
      print('body map:$gg');

      // chech status code
      if (response.statusCode == 200) {
        print('Response body: ${response.body}');
        var jsonData = jsonDecode(response.body);
        print('json Data: ${jsonData}');

        return '';
      }

      return '0';
    } catch (Exception) {
      return '-9';
    }
  }


  // Ahlam /////////////////////////////////////////////////////////////////
  //first method
  Future<List<Parts>> fetchHaragParts(int lstno) async {
    List<Parts> parts_items = List<Parts>();
    String uri = base_api + get_partHarag_api + lstno.toString();
    try{
      var response = await http.get(uri);
      print('response status: ,${response.statusCode}');

      if (response.statusCode == 200) {
        var jsonData = jsonDecode(response.body);

        var parts = jsonData;
        for (var item in parts) {
          parts_items.add(Parts.fromMap(item));
        }
      }
      else {
        print('error 0 : response is not 200 ');
      }
    }
    catch (Excaption) {
      print('error -9  ');
    }

    return parts_items;
  }
  ///////////////////////////////////////////////////////////////////////////////////////
}
