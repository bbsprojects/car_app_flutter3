import 'dart:convert';
import 'package:my_flutter_app/model/car_system.dart';
import 'package:my_flutter_app/utils/api_path.dart';
import 'package:http/http.dart' as http;

class OtherApi {
  Future<List<CarFuell>> fetchAllFuell() async {
    List<CarFuell> modules_items = List<CarFuell>();

    String uri = base_api + get__models_dependon_brand_api;

    try {
      var response = await http.get(uri);
      print('response status: ,${response.statusCode}');
      if (response.statusCode == 200) {
        var jsonData = jsonDecode(response.body);
        print('body ${response.body}');

        var modules = jsonData;
        for (var item in modules) {
          modules_items.add(CarFuell.fromMap(item));

          print(('module Name:${CarFuell.fromMap(item).f_type}'));
        }
      }
      return modules_items;
    } catch (Excaption) {
      return modules_items;
    }
  }
}
