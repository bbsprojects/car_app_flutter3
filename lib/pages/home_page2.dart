import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_flutter_app/database/database_provider.dart';
import 'package:my_flutter_app/menu/my_drawer.dart';
import 'package:my_flutter_app/model/currentmember.dart';
import 'package:my_flutter_app/model/shopping_cart.dart';
import 'package:my_flutter_app/pages/menu_pages/parts_view.dart';
import 'package:my_flutter_app/pages/newhrag.dart';
import 'package:my_flutter_app/pages/other_pages/favorite_page.dart';
import 'package:my_flutter_app/pages/other_pages/notifications_page.dart';
import 'package:my_flutter_app/pages/pop_down_pages/about.dart';
import 'package:my_flutter_app/pages/pop_down_pages/help.dart';
import 'package:my_flutter_app/pages/recommendedcars.dart';
import 'package:my_flutter_app/pages/menu_pages/hrag_page.dart';
import 'package:my_flutter_app/utils/constants.dart';
import 'package:my_flutter_app/utils/page_routes.dart';
import 'package:my_flutter_app/widgets/default_button.dart';

class HomePage extends StatefulWidget {
  static const String route_name = '/homePage2';

  @override
  _HomePageState createState() => _HomePageState();
}

enum PopOutMenu { HELP, ABOUT, CONTACT, SETTINGS }

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  int harag = 0;
  int notification = 1;
  int shoppingCart = 2;
  int _selectedIndex;
  List<IconData> listicn = [
    FontAwesomeIcons.car,
    FontAwesomeIcons.productHunt,
    FontAwesomeIcons.warehouse,
    FontAwesomeIcons.fontAwesomeFlag,
    FontAwesomeIcons.carCrash,
    Icons.local_offer,
  ];
  List<String> listCatgoryname = [
    "سيارات",
    "قطع غيار",
    "حراج",
    "مستلزمات ",
    "فنيين",
    "عروض",
  ];

  void isLogined() {
    DatabaseProvider.db.getMember().then((value) => {
          setState(() {
            if (value.m_id != null) {
              CurrentMember.m_id = value.m_id;
              CurrentMember.m_account = value.m_account;
              CurrentMember.m_address = value.m_address;
              CurrentMember.m_area = value.area_name;
              CurrentMember.m_cdate = value.m_cdate;
              CurrentMember.m_comment = value.m_comment;
              CurrentMember.m_country = value.c_name;
              CurrentMember.m_email = value.m_email;
              CurrentMember.m_fname = value.m_fname;
              CurrentMember.m_gender = value.m_gender;
              CurrentMember.m_img = value.m_img;
              CurrentMember.m_lname = value.m_lname;
              CurrentMember.m_mobile = value.m_mobile;
            }
          })
        });
  }

  Widget _buildIcon(int index) {
    return GestureDetector(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Stack(
          alignment: Alignment.topCenter,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(bottom: 10.0),
              padding: EdgeInsets.only(bottom: 10.0),
              decoration: _selectedIndex == index
                  ? BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: kPrimaryColor,
                    )
                  : BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                      color: Colors.white,
                    ),
              child: Container(
                padding: const EdgeInsets.all(10),
                width: 100,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10.0),
                      topRight: Radius.circular(10.0)),
                ),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 60,
                      width: 60,
                      decoration: BoxDecoration(
                        color: _selectedIndex == index
                            ? kSecondaryColor
                            : kSecondaryColor,
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      child: Icon(
                        listicn[index],
                        size: 25,
                        color: _selectedIndex == index
                            ? kPrimaryColor
                            : Colors.grey,
                      ),
                    ),
                    Text(
                      '${listCatgoryname[index]}',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: _selectedIndex == index
                              ? kPrimaryColor
                              : kTextColor),
                    )
                  ],
                ),
              ),
            ),
            _selectedIndex == index
                ? Positioned(
                    bottom: -1,
                    child: Icon(
                      FontAwesomeIcons.chevronDown,
                      color: kPrimaryColor,
                      size: 30,
                    ),
                  )
                : Container(),
          ],
        ),
      ),
      onTap: () {
        setState(() {
          _selectedIndex = index;
        });
        print('_selectedIndex $_selectedIndex');
      },
    );
  }

  Widget _buildHragcard() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Card(
        elevation: 0,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  InkWell(
                    child: Column(
                      children: [
                        Text('حراج سيارات'),
                        Container(
                          width: 100,
                          child: Divider(
                            color: harag == 0 ? kPrimaryColor : Colors.white,
                            thickness: 2,
                          ),
                        ),
                      ],
                    ),
                    onTap: () {
                      setState(() {
                        harag = 0;
                      });
                      /*  Navigator.push(context,
                          MaterialPageRoute(builder: (context) {
                        return HragPage(0);
                      }));*/
                    },
                  ),
                  InkWell(
                      child: Column(
                        children: [
                          Text('حراج قطع غيار'),
                          Container(
                            width: 100,
                            child: Divider(
                              color: harag == 1 ? kPrimaryColor : Colors.white,
                              thickness: 2,
                            ),
                          ),
                        ],
                      ),
                      onTap: () {
                        setState(() {
                          harag = 1;
                        });
                      })
                ],
              ),
              SizedBox(
                height: 10,
              ),
              Column(
                children: [
                  InkWell(
                    onTap: () {
                      if (harag == 0) {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return HragPage(0);
                        }));
                      } else {
                        Navigator.push(context,
                            MaterialPageRoute(builder: (context) {
                          return HragPage(1);
                        }));
                      }
                    },
                    child: harag == 0
                        ? Row(
                            children: [
                              Icon(FontAwesomeIcons.carSide,
                                  color: Colors.blue[400]),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                'استعراض جميع سيارات حراج',
                                style: TextStyle(
                                    //  decoration: TextDecoration.underline,
                                    color: Colors.blue[400]),
                              ),
                            ],
                          )
                        : Row(
                            children: [
                              Icon(FontAwesomeIcons.productHunt,
                                  color: Colors.blue[400]),
                              SizedBox(
                                width: 10,
                              ),
                              Text(
                                'استعراض جميع قطع غيار حراج',
                                style: TextStyle(
                                    // decoration: TextDecoration.underline,
                                    color: Colors.blue[400]),
                              ),
                            ],
                          ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(color: kPrimaryColor)),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 10,
                        ),
                        harag == 0
                            ? Text('يمكنك بيع  سياراتك الان في حراج')
                            : Text('يمكنك بيع قطعه غيار سيارتك الان في حراج'),
                        SizedBox(
                          height: 10,
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(horizontal: 20),
                          child: DefaultButton(
                            text: "بيع الان",
                            press: () {
                              if (harag == 0)
                                Navigator.pushNamed(context, PageRoutes.addcar);
                              else
                                Navigator.pushNamed(
                                    context, PageRoutes.addpart);
                            },
                          ),
                        ),
                        SizedBox(
                          height: 10,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCarcard() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Card(
        elevation: 0,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  InkWell(
                      child: Row(
                        children: [
                          Icon(FontAwesomeIcons.car, color: Colors.blue[400]),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            'جميع السيارات',
                            style: TextStyle(
                                //  decoration: TextDecoration.underline,
                                color: Colors.blue[400]),
                          ),
                        ],
                      ),
                      onTap: () {
                        print("*********");
                        Navigator.pushNamed(context, PageRoutes.carpage);
                      }),
                  InkWell(
                    child: Row(
                      children: [
                        Icon(Icons.scatter_plot, color: Colors.blue[400]),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          'بحث بحسب البراند',
                          style: TextStyle(
                              //  decoration: TextDecoration.underline,
                              color: Colors.blue[400]),
                        ),
                      ],
                    ),
                    onTap: () {
                      print("*********");
                      Navigator.pushNamed(context, PageRoutes.allbrands);
                    },
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildPartcard() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Card(
        elevation: 0,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  GestureDetector(
                      child: Row(
                        children: [
                          Icon(
                            FontAwesomeIcons.box,
                            color: Colors.blue[400],
                            size: 20,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            'مكائن',
                            style: TextStyle(
                                //  decoration: TextDecoration.underline,
                                color: Colors.blue[400]),
                          ),
                        ],
                      ),
                      onTap: () {
                        // Navigator.pushNamed(context, PageRoutes.parts_machines);
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) {
                            return PartsView(
                              part_type: 1,
                            );
                          }),
                        );
                      }),
                  GestureDetector(
                    child: Row(
                      children: [
                        Icon(
                          FontAwesomeIcons.carSide,
                          color: Colors.blue[400],
                          size: 20,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'البودي',
                          style: TextStyle(
                              //  decoration: TextDecoration.underline,
                              color: Colors.blue[400]),
                        ),
                      ],
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) {
                          return PartsView(
                            part_type: 2,
                          );
                        }),
                      );
                    },
                  ),
                ],
              ),
              SizedBox(
                height: 25,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  InkWell(
                      child: Row(
                        children: [
                          Icon(
                            FontAwesomeIcons.carCrash,
                            color: Colors.blue[400],
                            size: 20,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            'إطارات',
                            style: TextStyle(
                                //  decoration: TextDecoration.underline,
                                color: Colors.blue[400]),
                          ),
                        ],
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) {
                            return PartsView(
                              part_type: 3,
                            );
                          }),
                        );
                      }),
                  GestureDetector(
                    child: Row(
                      children: [
                        Icon(
                          FontAwesomeIcons.oilCan,
                          color: Colors.blue[400],
                          size: 20,
                        ),
                        SizedBox(
                          width: 10,
                        ),
                        Text(
                          'زيوت',
                          style: TextStyle(
                              //  decoration: TextDecoration.underline,
                              color: Colors.blue[400]),
                        ),
                      ],
                    ),
                    onTap: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) {
                          return PartsView(
                            part_type: 4,
                          );
                        }),
                      );
                    },
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  GestureDetector(
                      child: Row(
                        children: [
                          Icon(
                            FontAwesomeIcons.fantasyFlightGames,
                            color: Colors.blue[400],
                            size: 20,
                          ),
                          SizedBox(
                            width: 10,
                          ),
                          Text(
                            'إكسسوارات',
                            style: TextStyle(
                                //  decoration: TextDecoration.underline,
                                color: Colors.blue[400]),
                          ),
                        ],
                      ),
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(builder: (context) {
                            return PartsView(
                              part_type: 5,
                            );
                          }),
                        );
                      }),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCarSequelscard() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Card(
        elevation: 0,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  GestureDetector(child: Text('تلميع وسرويس'), onTap: () {}),
                  GestureDetector(
                    child: Text('أخرى'),
                    onTap: () {},
                  ),
                ],
              ),
              SizedBox(
                height: 25,
              ),
              GestureDetector(
                child: Text('تنجيد'),
                onTap: () {},
              ),
              SizedBox(
                height: 25,
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildTechnicianscard() {
    // return CarsTechniciansView();
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10),
      child: Card(
        elevation: 0,
        child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  GestureDetector(
                      child: Text('أشخاص'),
                      onTap: () {
                        Navigator.pushNamed(context, PageRoutes.technicians);
                      }),
                  GestureDetector(
                    child: Text('ورش'),
                    onTap: () {},
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildCard() {
    if (_selectedIndex != null) {
      if (_selectedIndex == 0) {
        return _buildCarcard();
      } else if (_selectedIndex == 1) {
        return _buildPartcard();
      } else if (_selectedIndex == 2) {
        return _buildHragcard();
      } else if (_selectedIndex == 3) {
        return _buildCarSequelscard();
      } else if (_selectedIndex == 4) {
        return _buildTechnicianscard();
      } else
        return Container();
    } else
      return Container();
  }

  @override
  void initState() {
    super.initState();
    isLogined();
    //   WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // setState(() { _notification = state; });
  }

  @override
  void dispose() {
    // TODO: implement dispose
    //  WidgetsBinding.instance.removeObserver(this);
  }

  Widget myNotificationIcon() {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => NotificationPage(),
            ));
      },
      child: Container(
        alignment: Alignment.center,
        margin: const EdgeInsets.only(left: 10),
        width: 30,
        height: 30,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Icon(
              Icons.notifications,
              color: Colors.white,
              size: 30,
            ),
            Container(
              width: 30,
              height: 30,
              alignment: Alignment.topRight,
              margin: EdgeInsets.only(top: 5),
              child: Container(
                width: 15,
                height: 15,
                decoration: notification > 0
                    ? BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color(0xffc32c37),
                      )
                    : BoxDecoration(),
                child: Padding(
                  padding: const EdgeInsets.all(0),
                  child: Center(
                    child: Text(
                      notification > 0 ? notification.toString() : '',
                      style: TextStyle(fontSize: 10),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget mySoppingCartIcon() {
    return InkWell(
      onTap: () {
        Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => Favorite_Page(),
            ));
      },
      child: Container(
        alignment: Alignment.center,
        margin: const EdgeInsets.only(left: 10),
        width: 30,
        height: 30,
        child: Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Icon(
              Icons.shopping_cart,
              color: Colors.white,
              size: 30,
            ),
            Container(
              width: 30,
              height: 30,
              alignment: Alignment.topRight,
              margin: EdgeInsets.only(top: 5),
              child: Container(
                width: 15,
                height: 15,
                decoration: shoppingCart > 0
                    ? BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color(0xffc32c37),
                      )
                    : BoxDecoration(),
                child: Padding(
                  padding: const EdgeInsets.all(0),
                  child: Center(
                    child: Text(
                      shoppingCart > 0 ? shoppingCart.toString() : '',
                      style: TextStyle(fontSize: 10),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      backgroundColor: kSecondaryColor,
      appBar: AppBar(
          title: Text(
            'سيارات',
            style: TextStyle(fontSize: 20, color: Colors.white),
          ),
          backgroundColor: kPrimaryColor,
          centerTitle: false,
          actions: <Widget>[
            myNotificationIcon(),
            mySoppingCartIcon(),

            //    _popOutMenu(context),
          ],
          leading: IconButton(
            icon: SvgPicture.asset("assets/icons/menu.svg"),
            onPressed: () => _scaffoldKey.currentState.openDrawer(),
          )
          // set the tabs
          ),
      drawer: MyDrawer(),
      body: SafeArea(
        child: Stack(
          children: [
            ClipPath(
              clipper: UpperClipper(),
              child: Container(
                height: 150,
                decoration: BoxDecoration(
                  gradient: LinearGradient(
                    colors: [kPrimaryColor, kPrimaryColor],
                  ),
                ),
              ),
            ),
            ListView(
              children: <Widget>[
                Container(
                  width: double.infinity,
                  child: Container(
                    //   padding: const EdgeInsets.only(left: 120, right: 20),
                    child: Text(
                      'xx جميع مايلزم سيارتك',
                      textAlign: TextAlign.center,
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                  ),
                ),
                SizedBox(
                  height: 20.0,
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: listicn
                        .asMap()
                        .entries
                        .map((MapEntry map) => _buildIcon(map.key))
                        .toList(),
                  ),
                ),
                _buildCard(),
                SizedBox(
                  height: 20,
                ),
                RecommendedCars(),
                SizedBox(
                  height: 10,
                ),
                NewHarag(),
              ],
            ),
          ],
        ),
      ),
    );
  }

  _popOutMenu(BuildContext context) {
    return PopupMenuButton<PopOutMenu>(
      color: Colors.white,
      itemBuilder: (context) {
        return [
          PopupMenuItem<PopOutMenu>(
            value: PopOutMenu.ABOUT,
            child: Text('ABOUT'),
          ),
          PopupMenuItem<PopOutMenu>(
            value: PopOutMenu.HELP,
            child: Text('HELP'),
          ),
          PopupMenuItem<PopOutMenu>(
            value: PopOutMenu.CONTACT,
            child: Text('CONTACT'),
          ),
          PopupMenuItem<PopOutMenu>(
            value: PopOutMenu.SETTINGS,
            child: Text('SETTINGS'),
          ),
        ];
      },
      onSelected: (PopOutMenu menu) {
        switch (menu) {
          case PopOutMenu.ABOUT:
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) {
                return AboutPage();
              }),
            );
            break;
          case PopOutMenu.HELP:
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) {
                return HelpPage();
              }),
            );
            break;
          case PopOutMenu.CONTACT:
            break;
          case PopOutMenu.SETTINGS:
            Navigator.pushReplacementNamed(context, PageRoutes.setting);
            break;
        }
        //TODO ;
      },
      icon: Icon(Icons.more_vert),
    );
  }
}

class UpperClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final Path path = Path();
    path.lineTo(0.0, size.height / 1.5);

    var firstEndPoint = Offset(size.width, size.height / 1.5);
    var firstControlPoint = Offset(size.width / 2, size.height);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    path.lineTo(size.width, 0.0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) => true;
}
