import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_flutter_app/menu/my_bottom_navigation_bar.dart';
import 'package:my_flutter_app/menu/my_drawer.dart';
import 'package:my_flutter_app/pages/pop_down_pages/about.dart';
import 'package:my_flutter_app/pages/pop_down_pages/help.dart';
import 'package:my_flutter_app/pages/tab_pages/cars_technicians_view.dart';
import 'package:my_flutter_app/pages/menu_pages/hrag_page.dart';
import 'package:my_flutter_app/pages/tab_pages/offers_view.dart';
import 'package:my_flutter_app/pages/menu_pages/products_view.dart';
import 'package:my_flutter_app/utils/constants.dart';
import 'package:my_flutter_app/utils/page_routes.dart';
import 'tab_pages/CarsSequels_view.dart';
import 'menu_pages/parts_view.dart';

class CarPage extends StatefulWidget {
  static const String route_name = '/homePage';

  @override
  _CarPageState createState() => _CarPageState();
}

enum PopOutMenu { HELP, ABOUT, CONTACT, SETTINGS }

class _CarPageState extends State<CarPage> with SingleTickerProviderStateMixin {
  TabController _tabController;
  @override
  void initState() {
    super.initState();
    _tabController = TabController(initialIndex: 0, length: 6, vsync: this);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('سيارات'),
        backgroundColor: kPrimaryColor,
        centerTitle: false,
        actions: <Widget>[
          IconButton(icon: Icon(Icons.search), onPressed: () {}),
          _popOutMenu(context),
        ],
        // set the tabs
        bottom: TabBar(
          indicatorColor: Colors.transparent,
          isScrollable: true,
          tabs: [
            Tab(
              //icon:Icon(Icons.directions_car),
              icon: Icon(FontAwesomeIcons.car),
              text: 'سيارات',
            ),
            Tab(
              icon: Icon(FontAwesomeIcons.productHunt),
              text: 'قطع غيار',
            ),
            Tab(
              icon: Icon(FontAwesomeIcons.warehouse),
              text: 'حراج',
            ),
            Tab(
              //icon: Icon(Icons.library_add),
              icon: Icon(FontAwesomeIcons.fontAwesomeFlag),
              text: 'مستلزمات سيارات',
            ),
            Tab(
              icon: Icon(FontAwesomeIcons.carCrash),
              text: 'فنيين',
            ),
            Tab(
              icon: Icon(Icons.local_offer),
              text: 'عروض',
            ),
          ],
          controller: _tabController,
        ),
      ),
      drawer: MyDrawer(),
      body: Center(
        //child: Text('This is car Page'),
        child: TabBarView(
          children: [
            ProductsView(),
            PartsView(),
            HragPage(0),
            CarsSequelsView(),
            CarsTechniciansView(),
            OffersView(),
          ],
          controller: _tabController,
        ),
      ),
      bottomNavigationBar: myBottomNavigationBar(),
    );
  }

  _popOutMenu(BuildContext context) {
    return PopupMenuButton<PopOutMenu>(
      itemBuilder: (context) {
        return [
          PopupMenuItem<PopOutMenu>(
            value: PopOutMenu.ABOUT,
            child: Text('ABOUT'),
          ),
          PopupMenuItem<PopOutMenu>(
            value: PopOutMenu.HELP,
            child: Text('HELP'),
          ),
          PopupMenuItem<PopOutMenu>(
            value: PopOutMenu.CONTACT,
            child: Text('CONTACT'),
          ),
          PopupMenuItem<PopOutMenu>(
            value: PopOutMenu.SETTINGS,
            child: Text('SETTINGS'),
          ),
        ];
      },
      onSelected: (PopOutMenu menu) {
        switch (menu) {
          case PopOutMenu.ABOUT:
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) {
                return AboutPage();
              }),
            );
            break;
          case PopOutMenu.HELP:
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) {
                return HelpPage();
              }),
            );
            break;
          case PopOutMenu.CONTACT:
            break;
          case PopOutMenu.SETTINGS:
            Navigator.pushReplacementNamed(context, PageRoutes.setting);
            break;
        }
        //TODO ;
      },
      icon: Icon(Icons.more_vert),
    );
  }
}
