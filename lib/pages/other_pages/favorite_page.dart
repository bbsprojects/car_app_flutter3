import 'package:flutter/material.dart';
import 'package:my_flutter_app/database/database_provider.dart';
import 'package:my_flutter_app/model/products.dart';
import 'package:my_flutter_app/model/products_detail.dart';
import 'package:cache_image/cache_image.dart';
import 'package:my_flutter_app/utils/constants.dart';

class Favorite_Page extends StatefulWidget {
  static const String route_name = '/favoritePage';
  @override
  _Favorite_PageState createState() => _Favorite_PageState();
}

class _Favorite_PageState extends State<Favorite_Page> {
  List<Products> lstProduct = List<Products>(); // fill it with salite
  @override
  void initState() {
    super.initState();
    get_Data(); // from salite
  }

  get_Data() {
    DatabaseProvider.db.getFavoriteProducts().then((value) {
      setState(() {
        setState(() {
          lstProduct = value;
        });
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('المفضلة'),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemBuilder: (context, index) {
                return Column(
                  children: [
                    _drawSingleRowFavoriteItem(lstProduct[index]),
                    _drawDivider(),
                  ],
                );
              },
              itemCount: lstProduct.length,
            ),
          ),
          // the next section in product page for
        ],
      ),
    );
  }

  Widget _drawSingleRowFavoriteItem(Products product) {
    Products p1 = product;
    return GestureDetector(
      onTap: () {
        // Navigator.pushReplacementNamed(context, ProductDetails.route_name);
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          // ProductDetails(p: p1,);
        }));
      },
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Row(
          children: <Widget>[
            SizedBox(
              child: FadeInImage(
                placeholder:
                    ExactAssetImage('assets/images/bg_second_board.png'),
                image: CacheImage('${product.p_name}'),
                fit: BoxFit.cover,
              ),
              width: 100,
              height: 100,
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '${product.p_name}',
                    maxLines: 3,
                    style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('${product.name}'),
                      Row(
                        children: <Widget>[
                          IconButton(
                              icon: Icon(
                                Icons.shopping_cart,
                                color: Theme.of(context).primaryColor,
                              ),
                              onPressed: () {}),
                          Text(
                            'شراء',
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                            ),
                          )
                        ],
                      ),
                      IconButton(
                          icon: Icon(
                            Icons.favorite,
                            color: Theme.of(context).primaryColor,
                          ),
                          onPressed: () {})
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  _drawDivider() {
    return Container(
      height: 0.7,
      width: double.infinity,
      color: kSecondaryColor,
    );
  }
}
