import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:my_flutter_app/api/products_api.dart';
import 'package:my_flutter_app/database/database_provider.dart';
import 'package:my_flutter_app/model/brand.dart';
import 'package:my_flutter_app/model/car_system.dart';
import 'package:my_flutter_app/model/carmodel.dart';
import 'package:my_flutter_app/model/currency.dart';
import 'package:my_flutter_app/model/products.dart';
import 'package:my_flutter_app/model/status.dart';
import 'package:my_flutter_app/utils/constants.dart';
import 'package:my_flutter_app/widgets/cstmtoast/toast_utils.dart';
import 'package:my_flutter_app/widgets/default_button.dart';
import 'package:my_flutter_app/widgets/dialog.dart';
import 'dart:io';
import 'dart:convert';

class AddNewCar extends StatefulWidget {
  static const String route_name = '/addcar';
  @override
  _AddNewCarState createState() => _AddNewCarState();
}

class _AddNewCarState extends State<AddNewCar> {
  int p_brand,
      p_model,
      is_new,
      p_currency,
      is_haraj,
      p_createdBy,
      p_trans,
      p_fuel;
  double p_price;
  String vin_no, p_cdate, p_name, p_img, p_details, p_mileage, p_year;
  List<Step> steps;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final _formKey = GlobalKey<FormState>();

  // this for first step ///////////////
  Brand newValue;
  String hintValueBrand = 'إختر الماركة لسيارتك ';
  String hintValueModel = 'اختر الموديل ';

  List<Brand> lstbrands;
  List<Module> lstmodule_brand1;
  Brand default_value_Brand = Brand();
  Module default_value_model = Module();
///////////////////////////////////////

  // this for second step ///////////////
  Currency default_value_Currency = currency[0];
  List<Currency> lstcurr = currency;
  Status default_value_status = status[0];
  List<Status> lststatus = status;
  CarTrans default_value_trans = carTrans[0];
  List<CarTrans> lsttrans = carTrans;
  CarFuell default_value_fuell = carFuell[0];
  List<CarFuell> lstfuell = carFuell;
/////////////////////////////////////
// four step /////////////////
  List<Asset> images = List<Asset>();
  String selectes_image = 'لم يتم اختيار صورة ';
  File file;
  int selected = 0;
  //////////////////////////////////////////

  // for errors ////////////////////
  final List<String> errors = [];
  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }
////////////////////////////////////

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get_brands();
  }

  void get_brands() async {
    try {
      if (await isthereInternet()) {
        lstbrands = await DatabaseProvider.db.getBrands();
        print('brand lenght ${lstbrands.length}');
        if (lstbrands.length > 0) default_value_Brand = lstbrands[0];

        lstmodule_brand1 = await DatabaseProvider.db.getModelsbyBrand(lstbrands[
                0]
            .b_id
            .toString()); //await moduleApi.fetchAllModel(lstbrands[0].b_id);
        print('module lenght ${lstmodule_brand1.length}');

        if (lstmodule_brand1.length > 0)
          default_value_model = lstmodule_brand1[0];

        //  print('default_value_model  ${default_value_model.mo_title}');

        setState(() {
          lstbrands = lstbrands;
          lstmodule_brand1 = lstmodule_brand1;
          default_value_model = default_value_model;
        });
      }
    } catch (Excaption) {}
  }

  getlistmodulesDependonBrand(int brand_id) async {
    try {
      if (await isthereInternet()) {
        lstmodule_brand1 =
            await DatabaseProvider.db.getModelsbyBrand(brand_id.toString());
        print('lstmodule_depend on brand1 ${lstmodule_brand1.length}');
        setState(() {
          if (lstmodule_brand1.length > 0)
            default_value_model = lstmodule_brand1[0];
          else
            default_value_model = Module();
          print(
              'default_value_model depend brand ${default_value_model.mo_title}');
        });
      }
    } catch (Excaption) {}
  }
///////////////////////////////////////////

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(add_car_page_title),
      ),
      body: ListView(
        children: [
          AddNewCarForm(),
          SizedBox(height: 20),
          Text(
            'سيتم إضافة سيارتك في حراج وستتمكن من رؤيتها في تبويب حراج وعرضها للبيع',
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.caption,
          )
        ],
      ),
    );
  }

  Widget AddNewCarForm() {
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            ////////////////////////////////
            Text(
              'البيانات الأساسية لسيارتك',
              style: nameProductStyle,
            ),
            SizedBox(height: 10),
            _drawDropDownButtonBrands(),
            SizedBox(height: 5),
            _drawDropDownButtonModules(),
            _drawYearModelField(),
            SizedBox(height: 20),
            ///////////////////////////////////
            Text('أخبرنا المزيد عن سيارتك ', style: nameProductStyle),
            //SizedBox(height: 10),
            _drawP_PriceField(),
            _drawP_StatusField(),
            _drawP_MilagelField(),
            _drawDropDownButton_P_Trans(),
            _drawDropDownButton_P_Fuell(),
            SizedBox(height: 20),
            ////////////////////////////
            Text('ما هي المواصفات التي تتميز بها سيارتك ',
                style: nameProductStyle),
            SizedBox(height: 10),
            Text('تفاصيل أكثر '),
            _drawCarDetails(),
            SizedBox(height: 20),
            ////////////////////////////
            Text('قم بإرفاق صور عن سيارتك', style: nameProductStyle),
            SizedBox(height: 10),
            _drawImages(),
            SizedBox(height: 30),
            ////////////////////////////
            DefaultButton(
              text: "إرسال",
              press: () async {
                //  if (_formKey.currentState.validate()) {
                ProductApi productApi = ProductApi();
                _formKey.currentState.save();
                Dialogs.showLoadingDialog(context, _keyLoader, "جاري الإرسال");
                Products p = Products(
                  //p_id:1 ,
                  p_name: default_value_model.mo_title + " " + p_year,
                  p_brand: default_value_Brand.b_id ?? 0,
                  p_model: default_value_model.mo_id ?? 0,
                  p_year: p_year,
                  p_mfr: '',
                  p_price: p_price,
                  p_currency: 1,
                  p_detailes: p_details,
                  //p_color: 1,
                  p_trans: default_value_trans.tr_id,
                  p_fuel: default_value_fuell.f_id,
                  p_mileage: p_mileage,
                  p_cdate: DateTime.now().toString(),
                  p_comment: '',
                  p_stock: 1,
                  p_tag: '',
                  p_img: p_img,
                  p_createdBy: 12,
                  is_orginal: 0,
                  is_new: default_value_status.car_stat_id,
                  is_sold: 0,
                  is_available: 1,
                  is_haraj: 1,
                  vin_no: '',
                );
                var status = await productApi.sendProductInfo(p);
                if (status == 1) {
                  print('success');
                  ToastUtils.showCustomToast(context, "تم بنجاح .. شكرا لك  ");
                  Navigator.pop(context);
                } else if (status == 0 || status == -9) {
                  addError(error: kTrySendCarAgain);
                }
                Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                    .pop();
                //  }
              },
            ),
            /////////////////////////////
          ],
        ),
      ),
    );
  }

// for main info /////////////////////////////////
  Widget _drawDropDownButtonBrands() {
    return DropdownButton<Brand>(
      isExpanded: true,
      hint: Text(hintValueBrand),
      icon: Icon(
        FontAwesomeIcons.carSide,
        color: Colors.grey.shade400,
      ),
      iconSize: 22,
      value: default_value_Brand,
      elevation: 16,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (Brand newValue) {
        setState(() {
          default_value_Brand = newValue;

          for (var item in lstbrands) {
            if (newValue.b_id == item.b_id) {
              getlistmodulesDependonBrand(newValue.b_id);
            }
          }
        });
      },
      items: lstbrands.map<DropdownMenuItem<Brand>>((Brand value) {
        return DropdownMenuItem<Brand>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value.b_title ?? '',
              ),
            ),
          ),
        );
      }).toList(),
    );
  }

  Widget _drawDropDownButtonModules() {
    return DropdownButton<Module>(
      isExpanded: true,
      value: default_value_model,
      icon: Icon(
        FontAwesomeIcons.carSide,
        color: Colors.grey.shade400,
      ),
      iconSize: 22,
      elevation: 16,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (Module newValue) {
        setState(() {
          default_value_model = newValue;
        });
      },
      items: lstmodule_brand1.map<DropdownMenuItem<Module>>((Module value) {
        return DropdownMenuItem<Module>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value.mo_title ?? '',
              ),
            ),
          ),
        );
      }).toList(),
    );
  }

  TextFormField _drawYearModelField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      onSaved: (newValue) => p_year = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: p_YearNullError);
        } else if (value.length > 4) {
          removeError(error: Invalid_p_Year_Error);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: p_YearNullError);
          return "";
        } else if (value.length < 5) {
          addError(error: Invalid_p_Year_Error);
          return "";
        }
        return null;
      },
      decoration: textInputDecoration.copyWith(
        hintText: "ادخل سنة الصنع لسيارتك",
        labelText: 'سنة الصنع',
      ),
    );
  }
///////////////////////////////////////////////////////

// for more info ////////////////////////////////
  Widget _drawP_PriceField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      onSaved: (newValue) => p_price = double.tryParse(newValue),
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNumberNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: p_PriceNullError);
          return "";
        }
        return null;
      },
      decoration: textInputDecoration.copyWith(
        hintText: "السعر",
        labelText: "السعر بالسعودي",
        //suffixIcon: Icon(FontAwesomeIcons.car,color: Colors.grey.shade400,),
      ),
    );
  }

  Widget _drawDropDownButton_P_Currency() {
    return DropdownButton<Currency>(
      isExpanded: true,
      icon: Icon(
        FontAwesomeIcons.moneyBill,
        color: Colors.grey.shade400,
      ),
      iconSize: 22,
      value: default_value_Currency,
      elevation: 16,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (Currency newValue) {
        setState(() {
          default_value_Currency = newValue;
        });
      },
      items: lstcurr.map<DropdownMenuItem<Currency>>((Currency value) {
        return DropdownMenuItem<Currency>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value.curr_title,
              ),
            ),
          ),
        );
      }).toList(),
    );
  }

  Widget _drawP_StatusField() {
    return DropdownButton<Status>(
      isExpanded: true,
      icon: Icon(
        FontAwesomeIcons.car,
        color: Colors.grey.shade400,
      ),
      iconSize: 22,
      value: default_value_status,
      elevation: 16,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (Status newValue) {
        setState(() {
          default_value_status = newValue;
        });
      },
      items: lststatus.map<DropdownMenuItem<Status>>((Status value) {
        return DropdownMenuItem<Status>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value.car_stat_title,
              ),
            ),
          ),
        );
      }).toList(),
    );
  }

  Widget _drawP_MilagelField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => p_mileage = newValue,
      decoration: textInputDecoration.copyWith(
        hintText: "عداد السيارة",
        labelText: 'ادخل عداد ممشى سياؤتك',
        suffixIcon: Icon(
          FontAwesomeIcons.car,
          color: Colors.grey.shade400,
        ),
      ),
    );
  }

  Widget _drawDropDownButton_P_Trans() {
    return DropdownButton<CarTrans>(
      isExpanded: true,
      icon: Icon(
        FontAwesomeIcons.car,
        color: Colors.grey.shade400,
      ),
      iconSize: 22,
      value: default_value_trans,
      elevation: 16,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (CarTrans newValue) {
        setState(() {
          default_value_trans = newValue;
        });
      },
      items: lsttrans.map<DropdownMenuItem<CarTrans>>((CarTrans value) {
        return DropdownMenuItem<CarTrans>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value.tr_trype,
              ),
            ),
          ),
        );
      }).toList(),
    );
  }

  Widget _drawDropDownButton_P_Fuell() {
    return DropdownButton(
      isExpanded: true,
      icon: Icon(
        FontAwesomeIcons.car,
        color: Colors.grey.shade400,
      ),
      iconSize: 22,
      value: default_value_fuell,
      elevation: 16,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (CarFuell newValue) {
        setState(() {
          default_value_fuell = newValue;
        });
      },
      items: lstfuell.map<DropdownMenuItem<CarFuell>>((CarFuell value) {
        return DropdownMenuItem<CarFuell>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value.f_type,
              ),
            ),
          ),
        );
      }).toList(),
    );
  }
//////////////////////////////////////////////////////////

  //for details of car ////////////////////////////
  Widget _drawCarDetails() {
    return TextFormField(
      keyboardType: TextInputType.multiline,
      onSaved: (newValue) => p_details = newValue,
      minLines: 1, //Normal textInputField will be displayed
      maxLines: 7,
      decoration: textInputDecoration.copyWith(
        hintText: "ادخل تفاصيل اللون ومميزات سيارتك بوصف لايقل عن خمسة اسطر ",
        labelText: 'وصف السيارة ',
        suffixIcon: Icon(
          FontAwesomeIcons.car,
          color: Colors.grey.shade400,
        ),
      ),
    );
  }
/////////////////////////////////////////////

////////////// for images
  Widget _drawImages() {
    /*
   setState(() {
     file == null
         ? selectes_image=''
         : selectes_image;
   });*/
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            RaisedButton(
              // onPressed: _choose,
              onPressed: () {
                setState(() {
                  if (selected == 0)
                    selected = 1;
                  else
                    selected = 0;
                });
              },
              child: Text('اضف صورة'),
            ),
            SizedBox(width: 10.0),
            RaisedButton(
              onPressed: () {
                //  _upload();
              },
              child: Text('إضافة مزيد من الصور'),
            )
          ],
        ),
        selected == 0 ? Container() : _chooseTool(),
        file == null
            ? Text(selectes_image)
            : Container(
                child: Image.file(file),
                height: 100,
                width: 100,
              ),
      ],
    );
  }

  Widget _chooseTool() {
    return Card(
      child: Column(
        children: [
          InkWell(
            child: Row(
              children: [
                Icon(Icons.camera),
                Text('الكاميرا'),
              ],
            ),
            onTap: () {
              _choose(1);
            }, // _choose(1),
          ),
          InkWell(
            child: Row(
              children: [
                Icon(Icons.filter),
                Text('الصور'),
              ],
            ),
            onTap: () {
              _choose(2);
            }, //_choose(2),
          ),
        ],
      ),
    );
  }

  _choose(int i) async {
    i == 1
        ? file = await ImagePicker.pickImage(source: ImageSource.camera)
        : file = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (file == null) return;
    String base64Image = base64Encode(file.readAsBytesSync());
    String fileName = file.path.split("/").last;
    p_img = base64Image;
    print('base64${base64Image}');
    print('file${fileName}');
    setState(() {
      file = file;
    });
  }

  /////////////////////////////
}
