import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_flutter_app/menu/my_drawer.dart';
import 'package:my_flutter_app/utils/constants.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;

class BBSoft extends StatelessWidget {

  static const String route_name='/bbsoftPage';

  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('دعم وتطوير'),
      ),
      body: SizedBox(
        width: double.infinity,
        height: double.maxFinite,
        child: Center(
          child: Padding(
            padding: const EdgeInsets.only(left:10.0,right: 10,top: 40),
            child: Stack(
              alignment: FractionalOffset.topCenter,
              children: [
                Card(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('بي بي سوفت لل..................'
                      'بي بي سوفت لل..................'
                        'بي بي سوفت لل..................'
                        'بي بي سوفت لل..................'
                        'بي بي سوفت لل..................'
                        'بي بي سوفت لل..................'
                      'بي بي سوفت لل..................'
                        'بي بي سوفت لل..................'
                      'بي بي سوفت لل..................'
                        'بي بي سوفت لل..................',
                      style: TextStyle(
                          fontSize: 17
                      ),),
                  ),
                ),
                SizedBox(height: 20),
                new Positioned(
                  child: new Align(
                    alignment: FractionalOffset.bottomCenter,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Expanded(
                          child: Container(
                            width: 90,
                            height: 110,
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage('assets/images/bbsoft.png')),
                            ),
                          ),
                        ),
                        SizedBox(width: 15,),
                        Container(
                          height: 75,
                          width: 125,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              InkWell(
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    Icon(FontAwesomeIcons.mobile,color: kPrimaryColor,size: 27,),
                                    Text('777885550',style: TextStyle(fontSize: 16),),
                                  ],
                                ),
                                onTap: (){UrlLauncher.launch("tel://777885550");},
                              ),
                              SizedBox(height: 10,),
                              GestureDetector(
                                child: Row(children: [
                                  Icon(Icons.local_phone,color: kPrimaryColor,size: 27,),
                                  Text('02-256323',style: TextStyle(fontSize: 16),),
                                ],),
                                onTap: ()=>UrlLauncher.launch("tel://02256323"),
                              ),
                            ],),
                        ),

                        IconButton(
                          icon: Icon(FontAwesomeIcons.facebook,color: kPrimaryColor,size: 27,),
                          onPressed: () async{
                            await UrlLauncher.canLaunch("https://www.facebook.com/") ?
                            UrlLauncher.launch('https://www.facebook.com/')
                            :print('not lunch');
                          },
                        ),

                         IconButton(
                          icon: Icon(FontAwesomeIcons.whatsapp,color: kPrimaryColor,size: 27,),
                           onPressed: () async{
                            String phone='777885550';
                             var whatsappUrl ="whatsapp://send?phone=$phone";
                             await UrlLauncher.canLaunch(whatsappUrl) ?
                             UrlLauncher.launch(whatsappUrl)
                                 :print('not lunch');
                           },
                         ),
                        IconButton(
                          icon: Icon(Icons.alternate_email,color: kPrimaryColor,size: 27,),
                          onPressed: (){},
                        ),
                      ],
                    ),
                  ),
                ),

              ],
            ),
          ),
        ),
      ),
    );

  }
}
