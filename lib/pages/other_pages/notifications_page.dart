import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_flutter_app/model/notifications.dart';
import 'package:my_flutter_app/utils/constants.dart';

class NotificationPage extends StatefulWidget {
  static const String route_name = '/notificationsPage';
  @override
  _NotificationPageState createState() => _NotificationPageState();
}

class _NotificationPageState extends State<NotificationPage> {
  List<Notifications> listAllnotyf = List<Notifications>();
  List<Notifications> listnotyf1 = List<Notifications>();
  List<Notifications> listnotyf2 = List<Notifications>();
  List<Notifications> listnotyf3 = List<Notifications>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    listAllnotyf = [
      Notifications(
          n_content: "lllll",
          n_cat: 1,
          n_cdate: "2020-02-03",
          n_desc: "lll",
          n_id: 1),
      Notifications(
        n_content: "lllll",
        n_cat: 2,
        n_cdate: "2020-02-03",
        n_desc: "lll",
        n_id: 1,
      ),
      Notifications(
          n_content: "lllll",
          n_cat: 2,
          n_cdate: "2020-02-03",
          n_desc: "lll",
          n_id: 1),
      Notifications(
          n_content: "lllll",
          n_cat: 3,
          n_cdate: "2020-02-03",
          n_desc: "lll",
          n_id: 1),
    ];

    listnotyf1 = listAllnotyf.where((i) => i.n_cat == 1).toList();
    listnotyf2 = listAllnotyf.where((i) => i.n_cat == 2).toList();
    listnotyf3 = listAllnotyf.where((i) => i.n_cat == 3).toList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('الإشعارات'),
      ),
      body: ListView(
        children: [
          ListTile(
            title: Text('طلبات'),
            subtitle: Text('كل مايخص طلباتك'),
            trailing: listnotyf1.length > 0
                ? Stack(
                    children: [
                      CircleAvatar(
                        maxRadius: 9,
                        backgroundColor: Colors.red,
                      ),
                      Positioned(
                        bottom: -6,
                        right: 4,
                        child: Text(
                          '${listnotyf1.length}',
                          style: TextStyle(color: Colors.white),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  )
                : Container(),
            leading: Icon(
              FontAwesomeIcons.listAlt,
              size: 25,
              color: Colors.black,
            ),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return TlabatNotifications(listnotyf1);
              }));
            },
          ),
          ListTile(
            title: Text('اخبار وعروض'),
            subtitle: Text('سوف تجند هنا عروض جديدة واخبار'),
            trailing: listnotyf2.length > 0
                ? Stack(
                    children: [
                      CircleAvatar(
                        maxRadius: 9,
                        backgroundColor: Colors.red,
                      ),
                      Positioned(
                        bottom: -6,
                        right: 4,
                        child: Text(
                          '${listnotyf2.length}',
                          style: TextStyle(color: Colors.white),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  )
                : Container(),
            leading: Icon(
              Icons.volume_up,
              size: 25,
              color: Colors.black,
            ),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return TlabatNotifications(listnotyf2);
              }));
            },
          ),
          ListTile(
            title: Text('تعليقات'),
            subtitle: Text('اشعارات بتعليق في منشورك'),
            trailing: listnotyf3.length > 0
                ? Stack(
                    children: [
                      CircleAvatar(
                        maxRadius: 9,
                        backgroundColor: Colors.red,
                      ),
                      Positioned(
                        bottom: -6,
                        right: 4,
                        child: Text(
                          '${listnotyf3.length}',
                          style: TextStyle(color: Colors.white),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  )
                : Container(),
            leading: Icon(
              FontAwesomeIcons.tenge,
              size: 25,
              color: Colors.black,
            ),
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) {
                return TlabatNotifications(listnotyf3);
              }));
            },
          ),
        ],
      ),
    );
  }
}

class TlabatNotifications extends StatefulWidget {
  List<Notifications> listnotyf;
  TlabatNotifications(this.listnotyf);
  @override
  _TlabatNotificationsState createState() =>
      _TlabatNotificationsState(listnotyf);
}

class _TlabatNotificationsState extends State<TlabatNotifications> {
  List<Notifications> listnotyf;
  _TlabatNotificationsState(this.listnotyf);
  bool isLoading = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('طلبات'),
      ),
      body: Center(
        child: isLoading
            ? CircularProgressIndicator(
                backgroundColor: Colors.white,
                valueColor: AlwaysStoppedAnimation<Color>(Colors.black),
              )
            : Container(),
      ),
    );
  }
}

class NewsNotification extends StatefulWidget {
  List<Notifications> listnotyf;
  NewsNotification(this.listnotyf);
  @override
  _NewsNotificationState createState() => _NewsNotificationState(listnotyf);
}

class _NewsNotificationState extends State<NewsNotification> {
  List<Notifications> listnotyf;
  _NewsNotificationState(this.listnotyf);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('اخبار'),
      ),
    );
  }
}

class CommentsNotification extends StatefulWidget {
  List<Notifications> listnotyf;
  CommentsNotification(this.listnotyf);
  @override
  _CommentsNotificationState createState() =>
      _CommentsNotificationState(listnotyf);
}

class _CommentsNotificationState extends State<CommentsNotification> {
  List<Notifications> listnotyf;
  _CommentsNotificationState(this.listnotyf);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('تعليقات'),
      ),
    );
  }
}
