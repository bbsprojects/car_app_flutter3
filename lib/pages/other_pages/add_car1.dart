import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:multi_image_picker/multi_image_picker.dart';
import 'package:my_flutter_app/api/brand_api.dart';
import 'package:my_flutter_app/api/model_api.dart';
import 'package:my_flutter_app/model/brand.dart';
import 'package:my_flutter_app/model/car_system.dart';
import 'package:my_flutter_app/model/carmodel.dart';
import 'package:my_flutter_app/model/currency.dart';
import 'package:my_flutter_app/model/status.dart';
import '../../utils/constants.dart';

class AddNewCar extends StatefulWidget {
  //static const String route_name = '/addcar';

  @override
  _AddNewCarState createState() => _AddNewCarState();
}

class _AddNewCarState extends State<AddNewCar> {
  int p_brand,
      p_model,
      is_new,
      p_currency,
      is_haraj,
      p_createdBy,
      p_trans,
      p_fuel;
  String p_price, vin_no, p_cdate, p_name, p_img, p_details, p_mileage, p_year;
  int _currentStep = 0;
  bool complete = false;
  List<Step> steps;
  List<Key> _formKeys;

  // this for first step ///////////////
  Brand newValue;
  String hintValueBrand = 'إختر الماركة لسيارتك ';
  String hintValueModel = 'اختر الموديل ';
  BrandApi brandApi = BrandApi();
  ModuleApi moduleApi = ModuleApi();
  List<Brand> lstbrands;
  List<Module> lstmodule_brand1;
  Brand default_value_Brand;
  Module default_value_model;
///////////////////////////////////////

  // this for second step ///////////////
  Currency default_value_Currency = currency[0];
  List<Currency> lstcurr = currency;
  Status default_value_status = status[0];
  List<Status> lststatus = status;
  CarTrans default_value_trans = carTrans[0];
  List<CarTrans> lsttrans = carTrans;
  CarFuell default_value_fuell = carFuell[0];
  List<CarFuell> lstfuell = carFuell;
/////////////////////////////////////
// four step /////////////////
  List<Asset> images = List<Asset>();
  String _error = 'لا يوجد خطأ';
  //////////////////////////////////////////

// for errors ////////////////////
  final List<String> errors = [];
  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }
////////////////////////////////////

  // for navigate of stepper's steps /////////
  next() {
    steps = _drawSteps();
    print(steps.length);
    _currentStep + 1 != steps.length
        ? goTo(_currentStep + 1)
        : setState(
            () => complete = true,
            // _formKeys[_currentStep].currentState.validate();
          );
  }

  cancel() {
    print(_currentStep.toString());
    if (_currentStep > 0) {
      goTo(_currentStep - 1);
    }
  }

  goTo(int step) {
    if (step > _currentStep) {
      setState(() => _currentStep = step);
    }
  }
/////////////////////////////////////////

  // init state to get all brands, then moduels dependon brand id ////////////
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get_brands();
  }

  void get_brands() async {
    Brand brand;
    try {
      if (await isthereInternet()) {
        lstbrands = await brandApi.fetchAllBrand();
        print('brand lenght ${lstbrands.length}');
        if (lstbrands.length > 0) default_value_Brand = lstbrands[0];

        lstmodule_brand1 = await moduleApi.fetchAllModel();
        print('module lenght ${lstmodule_brand1.length}');

        if (lstmodule_brand1.length > 0)
          default_value_model = lstmodule_brand1[0];
        print('default_value_model  ${default_value_model.mo_title}');
      }
    } catch (Excaption) {}
  }

  getlistmodulesDependonBrand(int brand_id) async {
    try {
      if (await isthereInternet()) {
        lstmodule_brand1 = await moduleApi.fetchAllModel();
        print('lstmodule_depend on brand1 ${lstmodule_brand1.length}');
        setState(() {
          default_value_model = lstmodule_brand1[0];
          print(
              'default_value_model depend brand ${default_value_model.mo_title}');
        });
      }
    } catch (Excaption) {}
  }
///////////////////////////////////////////

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(add_car_page_title),
      ),
      body: Column(
        children: <Widget>[
          complete
              ? Expanded(
                  child: Center(
                    child: AlertDialog(
                      title: new Text('إرسال البيانات'),
                      content: new Text(
                        "تم!",
                      ),
                      actions: <Widget>[
                        new FlatButton(
                          child: new Text("إغلاق"),
                          onPressed: () {
                            setState(() => complete = false);
                          },
                        ),
                      ],
                    ),
                  ),
                )
              : Expanded(
                  child: Stepper(
                    type: StepperType.vertical,
                    steps: _drawSteps(),
                    currentStep: _currentStep,
                    /*  onStepContinue:() {
                      setState(() {
                        if (_formKeys[_currentStep].currentState.validate()) {
                          _currentStep++;
                        }
                      });
                    },*/

                    onStepContinue: next,
                    onStepCancel: cancel,
                    onStepTapped: (step) => goTo(step),
                    /*
              onStepContinue:(){
                if(_currentStep != 1)
                  goTo(1);
                  },
              onStepCancel: () {
                if(_currentStep !=0)
                  goTo(-1);
              },
              // to custom button of stepper
              controlsBuilder: (BuildContext context, {VoidCallback onStepContinue, VoidCallback onStepCancel}) {
                return Row(
                  children: <Widget>[
                    DefaultButton(text: 'التالي',
                      press : onStepContinue,
                    ),
                    DefaultButton(text: 'السابق',
                      press : onStepCancel,
                    ),
                  ],
                );
              },*/
                  ),
                ),
        ],
      ),
    );
  }

  List<Step> _drawSteps() {
    List<Step> steps = [
      //first step /////////////////////////////////////
      Step(
        title: const Text('البيانات الأساسية لسيارتك'),
        isActive: true,
        state: StepState.complete,
        //content: Form(key: _formKeys[0], child: …),
        content: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _drawDropDownButtonBrands(),
            _drawDropDownButtonModules(),
            _drawYearModelField(),
          ],
        ),
      ),
      // second step/////////////////////////////
      Step(
        title: const Text('أخبرنا المزيد عن سيارتك '),
        isActive: true,
        state: StepState.disabled,
        content: Container(
          width: double.infinity, //To make it use as much space as it wants
          child: Column(
            children: <Widget>[
              //TextFormField(decoration: InputDecoration(labelText: 'النوع'),),
              Row(
                children: [
                  _drawP_PriceField(),
                  SizedBox(
                    width: 15,
                  ),
                  _drawDropDownButton_P_Currency(),
                ],
              ),
              Row(
                children: [
                  _drawP_StatusField(),
                  _drawP_MilagelField(),
                ],
              ),
              _drawDropDownButton_P_Trans(),
              _drawDropDownButton_P_Fuell(),
            ],
          ),
        ),
      ),

      //third step //////////////////////////////////
      Step(
        title: const Text('ما هي المواصفات التي تتميز بها سيارتك '),
        isActive: false,
        state: StepState.disabled,
        content: Column(
          children: [
            Text('تفاصيل أكثر '),
            _drawCarDetails(),
          ],
        ),
      ),
      //step four ...........................
      Step(
        title: const Text('قم بإرفاق صور عن سيارتك'),
        isActive: false,
        state: StepState.disabled,
        content: _drawImages(),
      ),
    ];
    return steps;
  }

  // for first step /////////////////////////////////
  Widget _drawDropDownButtonBrands() {
    return DropdownButton<Brand>(
      isExpanded: true,
      hint: Text(hintValueBrand),
      icon: Icon(
        FontAwesomeIcons.carSide,
        color: Colors.grey.shade400,
      ),
      iconSize: 22,
      value: default_value_Brand,
      elevation: 16,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (newValue) {
        setState(() {
          default_value_Brand = newValue;
          for (var item in lstbrands) {
            if (newValue.b_id == item.b_id) {
              getlistmodulesDependonBrand(newValue.b_id);
            }
          }
        });
      },
      items: lstbrands.map<DropdownMenuItem<Brand>>((Brand value) {
        return DropdownMenuItem<Brand>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value.b_title,
              ),
            ),
          ),
        );
      }).toList(),
    );
  }

  Widget _drawDropDownButtonModules() {
    return DropdownButton<Module>(
      isExpanded: true,
      value: default_value_model,
      icon: Icon(
        FontAwesomeIcons.carSide,
        color: Colors.grey.shade400,
      ),
      iconSize: 22,
      elevation: 16,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (Module newValue) {
        setState(() {
          default_value_model = newValue;
        });
      },
      items: lstmodule_brand1.map<DropdownMenuItem<Module>>((Module value) {
        return DropdownMenuItem<Module>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value.mo_title,
              ),
            ),
          ),
        );
      }).toList(),
    );
  }

  TextFormField _drawYearModelField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      onSaved: (newValue) => p_year = newValue,
      /*
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNumberNullError);
        } else if (value.length > 4) {
          removeError(error: kInvalidPhoneNumberError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: p_YearNullError);
          return "";
        } else if (value.length < 5) {
          addError(error: Invalid_p_Year_Error);
          return "";
        }
        return null;
      },*/
      decoration: textInputDecoration.copyWith(
        hintText: "ادخل سنة الصنع لسيارتك",
      ),
    );
  }
///////////////////////////////////////////////////////

  // for second step ////////////////////////////////
  Widget _drawP_PriceField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      onSaved: (newValue) => p_price = newValue,
      /*
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNumberNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: p_PriceNullError);
          return "";
        }
        return null;
      },*/
      decoration: textInputDecoration.copyWith(
        hintText: "السعر",
        labelText: p_PriceNullError,
        //suffixIcon: Icon(FontAwesomeIcons.car,color: Colors.grey.shade400,),
      ),
    );
  }

  Widget _drawDropDownButton_P_Currency() {
    return DropdownButton<Currency>(
      isExpanded: true,
      icon: Icon(
        FontAwesomeIcons.moneyBill,
        color: Colors.grey.shade400,
      ),
      iconSize: 22,
      value: default_value_Currency,
      elevation: 16,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (Currency newValue) {
        setState(() {
          default_value_Currency = newValue;
        });
      },
      items: lstcurr.map<DropdownMenuItem<Currency>>((Currency value) {
        return DropdownMenuItem<Currency>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value.curr_title,
              ),
            ),
          ),
        );
      }).toList(),
    );
  }

  Widget _drawP_StatusField() {
    return DropdownButton<Status>(
      isExpanded: true,
      icon: Icon(
        FontAwesomeIcons.car,
        color: Colors.grey.shade400,
      ),
      iconSize: 22,
      value: default_value_status,
      elevation: 16,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (Status newValue) {
        setState(() {
          default_value_status = newValue;
        });
      },
      items: lststatus.map<DropdownMenuItem<Status>>((Status value) {
        return DropdownMenuItem<Status>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value.car_stat_title,
              ),
            ),
          ),
        );
      }).toList(),
    );
  }

  Widget _drawP_MilagelField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => p_mileage = newValue,
      decoration: textInputDecoration.copyWith(
        hintText: "عداد السيارة",
        labelText: 'ادخل عداد ممشى سياؤتك',
        suffixIcon: Icon(
          FontAwesomeIcons.car,
          color: Colors.grey.shade400,
        ),
      ),
    );
  }

  Widget _drawDropDownButton_P_Trans() {
    return DropdownButton<CarTrans>(
      isExpanded: true,
      icon: Icon(
        FontAwesomeIcons.car,
        color: Colors.grey.shade400,
      ),
      iconSize: 22,
      value: default_value_trans,
      elevation: 16,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (CarTrans newValue) {
        setState(() {
          default_value_trans = newValue;
        });
      },
      items: lsttrans.map<DropdownMenuItem<CarTrans>>((CarTrans value) {
        return DropdownMenuItem<CarTrans>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value.tr_trype,
              ),
            ),
          ),
        );
      }).toList(),
    );
  }

  Widget _drawDropDownButton_P_Fuell() {
    return DropdownButton(
      isExpanded: true,
      icon: Icon(
        FontAwesomeIcons.car,
        color: Colors.grey.shade400,
      ),
      iconSize: 22,
      value: default_value_fuell,
      elevation: 16,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (CarFuell newValue) {
        setState(() {
          default_value_fuell = newValue;
        });
      },
      items: lstfuell.map<DropdownMenuItem<CarFuell>>((CarFuell value) {
        return DropdownMenuItem<CarFuell>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value.f_type,
              ),
            ),
          ),
        );
      }).toList(),
    );
  }

//////////////////////////////////////////////////////////
  //for third step ////////////////////////////
  Widget _drawCarDetails() {
    return TextFormField(
      keyboardType: TextInputType.multiline,
      onSaved: (newValue) => p_details = newValue,
      minLines: 1, //Normal textInputField will be displayed
      maxLines: 7,
      decoration: textInputDecoration.copyWith(
        hintText: "ادخل تفاصيل اللون ومميزات سيارتك بوصف لايقل عن خمسة اسطر ",
        labelText: 'وصف السيارة ',
        suffixIcon: Icon(
          FontAwesomeIcons.car,
          color: Colors.grey.shade400,
        ),
      ),
    );
  }

/////////////////////////////////////////////
// for fourth step
  Widget _drawImages() {
    return Column(
      children: <Widget>[
        Center(child: Text('Error: $_error')),
        RaisedButton(
          child: Text("أضف صورا لسيارتك"),
          onPressed: loadAssets,
        ),
        buildGridView(),
      ],
    );
  }

  Widget buildGridView() {
    return GridView.count(
      crossAxisCount: 3,
      shrinkWrap: true,
      children: List.generate(images.length, (index) {
        Asset asset = images[index];
        return AssetThumb(
          asset: asset,
          width: 250,
          height: 250,
        );
      }),
    );
  }

  Future<void> loadAssets() async {
    List<Asset> resultList = List<Asset>();
    String error = 'لايوجد خطأ';

    try {
      resultList = await MultiImagePicker.pickImages(
        maxImages: 250,
        enableCamera: true,
        selectedAssets: images,
        // cupertinoOptions: CupertinoOptions(takePhotoIcon: "مثال"),
        materialOptions: MaterialOptions(
          //actionBarColor: "#abcdef",
          actionBarTitle: "ddddd",
          allViewTitle: "جميع الصور ",
          useDetailsView: false,
          selectCircleStrokeColor: "#000000",
        ),
      );
    } on Exception catch (e) {
      error = e.toString();
    }

    if (!mounted) return;
    setState(() {
      images = resultList;
      _error = error;
    });
  }
/////////////////////////////////////////////

}
