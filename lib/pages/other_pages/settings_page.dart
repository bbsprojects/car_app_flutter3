import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_flutter_app/model/currentmember.dart';
import 'package:my_flutter_app/utils/api_path.dart';
import 'package:my_flutter_app/utils/constants.dart';

class Settings extends StatefulWidget {
  static const String route_name = '/settingsPage';

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      appBar: AppBar(
        centerTitle: false,
        title: Text('الإعدادات'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Padding(
              padding: const EdgeInsets.all(15.0),
              child: _drawUpdateImage(),
            ),
            SizedBox(height: 15,),
            Card(
              color: Colors.white,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: _drawUpdateNumber(),
                  ),
                  _drawDivider(),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: _drawUpdatePass(),
                  ),
                  _drawDivider(),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: _drawDeleteAccount(),
                  ),

                ],
              ),
            ),
            SizedBox(height: 15,),
            Card(
              color: Colors.white,
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: _drawHelp(),
                  ),
                  _drawDivider(),
                  Padding(
                    padding: const EdgeInsets.all(20.0),
                    child:  _drawTellFriends(),
                  ),


                ],
              ),
            ),

          ],
        ),
      ),
    );
  }

  Widget _drawDivider() {
    return Container(
      height: 1,
      color: Colors.grey.shade400,
    );
  }

  Widget _drawUpdateImage() {
    return Column(
      children: [
        ClipRRect(
            child: FadeInImage(
              width: 100,
              height: 100,
              fit: BoxFit.cover,
              placeholder: AssetImage(
                  'assets/images/unknow.jpg'),
              image: AssetImage(
                  '${base_api + CurrentMember.m_img}'
              ),
            ),
            borderRadius:
            BorderRadius.all(
                Radius.circular(50))),

        InkWell(
          child: Text('تعديل',style: TextStyle(color:kaccentPrimaryLightColor),),
          onTap: (){},
        )
      ],
    );
  }

  Widget _drawUpdateNumber() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        InkWell(child: Row(
          children: [
            Icon(FontAwesomeIcons.key,color:kaccentPrimaryLightColor,),
            SizedBox(width: 3,),
            Text('تغيير الرقـم'),
          ],
        ),onTap: (){},),
        Icon(Icons.chevron_right)
      ],);
  }

  Widget _drawUpdatePass() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        InkWell(child: Row(
          children: [
            Icon(FontAwesomeIcons.key,color:kaccentPrimaryLightColor,),
            SizedBox(width: 3,),
            Text('تغيير كلمة السـر'),
          ],
        ),onTap: (){},),
        Icon(Icons.chevron_right)
      ],);
  }

  Widget _drawDeleteAccount() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        InkWell(child: Row(
          children: [
            Icon(Icons.warning,color: Colors.yellow.shade700,),
            SizedBox(width: 3,),
            Text('حذف حسـاب'),
          ],
        ),onTap: (){},),         Icon(Icons.chevron_right)
      ],);
  }

 Widget _drawHelp() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        InkWell(child: Row(
          children: [
            Icon(Icons.live_help,color: kaccentPrimaryLightColor,),
            SizedBox(width: 3,),
            Text('مساعـدة'),
          ],
        ),onTap: (){},),
        Icon(Icons.chevron_right)
      ],);
 }

  Widget _drawTellFriends() {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        InkWell(child: Row(
          children: [
            Icon(FontAwesomeIcons.solidHeart,color: Colors.redAccent,),
            SizedBox(width: 3,),
            Text('اخبر صديـق'),
          ],
        ),onTap: (){},),
        Icon(Icons.chevron_right)
      ],);
  }
}
