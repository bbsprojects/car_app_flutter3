import 'package:flutter/material.dart';
import 'package:my_flutter_app/api/brand_api.dart';
import 'package:my_flutter_app/api/model_api.dart';
import 'package:my_flutter_app/database/database_provider.dart';
import 'package:my_flutter_app/model/currentmember.dart';
import 'package:my_flutter_app/pages/home_page2.dart';
import 'package:splashscreen/splashscreen.dart';

class Splash extends StatelessWidget {
  BrandApi brandApi = BrandApi();
  ModuleApi moduleApi = ModuleApi();
  static const String route_name = '/splash';

  getData() async {
    await brandApi.fetchAllBrand();
    await moduleApi.fetchAllModel();
  }

  @override
  Widget build(BuildContext context) {
    getData();
    return SplashScreen(
      seconds: 6,
      navigateAfterSeconds: new HomePage(),
      title: new Text(
        'GeeksForGeeks',
        textScaleFactor: 2,
      ),
      image: new Image.network(
          'https://www.geeksforgeeks.org/wp-content/uploads/gfg_200X200.png'),
      loadingText: Text("Loading"),
      photoSize: 100.0,
      loaderColor: Colors.blue,
    );
  }
}
