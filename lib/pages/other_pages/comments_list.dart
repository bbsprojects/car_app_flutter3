import 'package:cache_image/cache_image.dart';
import 'package:flutter/material.dart';
import 'package:my_flutter_app/model/comments.dart';
import 'package:my_flutter_app/utils/constants.dart';

class CommentsListKeyPrefix {
  static final String singleComment = "Comment";
  static final String commentUser = "Comment User";
  static final String commentText = "Comment Text";
  static final String commentDivider = "Comment Divider";
}

class CommentsList extends StatefulWidget {
  const CommentsList({Key key}) : super(key: key);
  @override
  _CommentsListState createState() => _CommentsListState();
}

class _CommentsListState extends State<CommentsList> {
  final List<Comments> comments = List<Comments>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    comments.add(Comments(
        cmnt_content: "comment1",
        name: "name1",
        email: "email1",
        category: "cat1",
        cmnt_date: "6min",
        image: ""));
    comments.add(Comments(
        cmnt_content: "comment2",
        name: "name2",
        email: "email2",
        category: "cat2",
        cmnt_date: "5min",
        image: ""));
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(top: 8.0),
          child: ExpansionTile(
            leading: Icon(Icons.comment),
            trailing: Text(comments.length.toString()),
            title: Text("التعليقات"),
            children: List<Widget>.generate(
              comments.length,
              (int index) => _SingleComment(
                key: ValueKey("${CommentsListKeyPrefix.singleComment} $index"),
                commentData: comments[0],
                index: index,
              ),
            ),
          ),
        ),
        Container(
          //   margin: EdgeInsets.all(15.0),
          height: 61,
          child: Row(
            children: [
              Expanded(
                child: Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(35.0),
                    boxShadow: [
                      BoxShadow(
                          offset: Offset(0, 3),
                          blurRadius: 5,
                          color: Colors.grey[400])
                    ],
                  ),
                  child: Row(
                    children: [
                      SizedBox(
                        width: 8,
                      ), //   IconButton(icon: Icon(Icons.face), onPressed: () {}),
                      CircleAvatar(
                        backgroundColor: Colors.transparent,
                        backgroundImage: //FadeInImage(
                            //  placeholder:
                            ExactAssetImage(
                                'assets/images/bg_second_board.png'),
                        // image: CacheImage(''),
                        //   fit: BoxFit.contain,
                        // ),
                      ),
                      SizedBox(
                        width: 0,
                      ),
                      Expanded(
                        child: TextField(
                          decoration: InputDecoration(
                              hintText: "اكتب تعليقك هنا ...",
                              border: InputBorder.none,
                              enabledBorder: InputBorder.none,
                              focusedBorder: InputBorder.none),
                        ),
                      ),
                      /*  IconButton(
                        icon: Icon(Icons.photo_camera),
                        onPressed: () {},
                      ),
                      IconButton(
                        icon: Icon(Icons.attach_file),
                        onPressed: () {},
                      )*/
                    ],
                  ),
                ),
              ),
              SizedBox(width: 10),
              Container(
                padding: const EdgeInsets.all(15.0),
                decoration:
                    BoxDecoration(color: kPrimaryColor, shape: BoxShape.circle),
                child: InkWell(
                  child: Icon(
                    Icons.send,
                    color: Colors.white,
                  ),
                  onLongPress: () {
                    setState(() {
                      //    _showBottom = true;
                    });
                  },
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}

class _SingleComment extends StatelessWidget {
  final Comments commentData;
  final int index;

  const _SingleComment(
      {Key key, @required this.commentData, @required this.index})
      : super(key: key);
  bool isLandscape(BuildContext context) =>
      MediaQuery.of(context).orientation == Orientation.landscape;
  @override
  Widget build(BuildContext context) {
    final int flex = isLandscape(context) ? 10 : 5;

    return Container(
      width: double.infinity,
      margin: const EdgeInsets.symmetric(vertical: 8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Expanded(
                flex: 2,
                child: Container(
                  child: Row(children: <Widget>[
                    Expanded(
                      flex: 1,
                      child: Container(
                        width: 40,
                        height: 60,
                        child: ClipRRect(
                          borderRadius: BorderRadius.circular(40),
                          child: FadeInImage(
                            placeholder: ExactAssetImage(
                                'assets/images/bg_second_board.png'),
                            image: CacheImage(commentData.image ?? ''),
                            fit: BoxFit.cover,
                          ),
                        ),
                      ),
                    ),
                    Expanded(
                      flex: flex,
                      child: Padding(
                        padding: const EdgeInsets.all(4.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(commentData.name),
                            SizedBox(height: 2.0),
                            Text(commentData.cmnt_date),
                          ],
                        ),
                      ),
                    ),
                  ]),
                ),
              ),
            ],
          ),
          Text(
            commentData.cmnt_content,
            textAlign: TextAlign.left,
          ),
          Divider(
            color: Colors.black45,
          ),
        ],
      ),
    );
  }
}
