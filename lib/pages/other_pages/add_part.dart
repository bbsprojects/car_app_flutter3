import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:my_flutter_app/api/parts_api.dart';
import 'package:my_flutter_app/database/database_provider.dart';
import 'package:my_flutter_app/model/brand.dart';
import 'package:my_flutter_app/model/car_system.dart';
import 'package:my_flutter_app/model/carmodel.dart';
import 'package:my_flutter_app/model/currency.dart';
import 'package:my_flutter_app/model/parts.dart';
import 'package:my_flutter_app/model/status.dart';
import 'package:my_flutter_app/utils/constants.dart';
import 'package:my_flutter_app/widgets/cstmtoast/toast_utils.dart';
import 'package:my_flutter_app/widgets/default_button.dart';
import 'package:my_flutter_app/widgets/dialog.dart';

class AddNewPart extends StatefulWidget {
  static const String route_name = '/addpart';

  @override
  _AddNewPartState createState() => _AddNewPartState();
}

class _AddNewPartState extends State<AddNewPart> {
  int pr_brand,
      pr_model,
      is_new,
      pr_currency,
      is_haraj,
      pr_createdBy,
      pr_trans,
      pr_fuel, pr_price;
  String vin_no, pr_cdate, pr_name, pr_img, pr_details, pr_mileage, pr_year;
  List<Step> steps;
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final _formKey = GlobalKey<FormState>();

  // this for first step ///////////////
  Brand newValue;

  List<Brand> lstbrands;
  List<Module> lstmodule_brand1;
  Brand default_value_Brand = Brand();
  Module default_value_model = Module();

  ///////////////////////////////////////

  // this for second step ///////////////
// this for second step ///////////////
  Currency default_value_Type = currency[0];
  List<Currency> lstcurr = currency;
  Status default_value_status = status[0];
  List<Status> lststatus = status;
  CarTrans default_value_trans = carTrans[0];
  List<CarTrans> lsttrans = carTrans;
  CarFuell default_value_fuell = carFuell[0];
  List<CarFuell> lstfuell = carFuell;

/////////////////////////////////////
  /////////////////////////////////////
// four step /////////////////

  String selectes_image = 'لم يتم اختيار صورة ';
  File file;
  int selected = 0;

  //////////////////////////////////////////

  // for errors ////////////////////
  final List<String> errors = [];

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

////////////////////////////////////
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    get_brands();
  }

  void get_brands() async {
    try {
      if (await isthereInternet()) {
        lstbrands = await DatabaseProvider.db.getBrands();
        print('brand lenght ${lstbrands.length}');
        if (lstbrands.length > 0) default_value_Brand = lstbrands[0];

        lstmodule_brand1 = await DatabaseProvider.db.getModelsbyBrand(lstbrands[
        0]
            .b_id
            .toString()); //await moduleApi.fetchAllModel(lstbrands[0].b_id);
        print('module lenght ${lstmodule_brand1.length}');

        if (lstmodule_brand1.length > 0)
          default_value_model = lstmodule_brand1[0];

        //  print('default_value_model  ${default_value_model.mo_title}');

        setState(() {
          lstbrands = lstbrands;
          lstmodule_brand1 = lstmodule_brand1;
          default_value_model = default_value_model;
        });
      }
    } catch (Excaption) {}
  }

  getlistmodulesDependonBrand(int brand_id) async {
    //   try {
    // if (await isthereInternet()) {
    lstmodule_brand1 =
    await DatabaseProvider.db.getModelsbyBrand(brand_id.toString());
    print('lstmodule_depend on brand1 ${lstmodule_brand1.length}');
    setState(() {
      if (lstmodule_brand1.length > 0)
        default_value_model = lstmodule_brand1[0];
      else
        default_value_model = Module();
      print(
          'default_value_model depend brand ${default_value_model.mo_title}');
    });
    // }
    //} catch (Excaption) {}
  }

///////////////////////////////////////////

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(add_car_page_title),
      ),
      body: ListView(
        children: [
          AddNewPartForm(),
          SizedBox(height: 20),
          Text(
            'سيتم إضافة سيارتك في حراج وستتمكن من رؤيتها في تبويب حراج وعرضها للبيع',
            textAlign: TextAlign.center,
            style: Theme
                .of(context)
                .textTheme
                .caption,
          )
        ],
      ),
    );
  }

  Widget AddNewPartForm() {
    return Form(
      key: _formKey,
      child: Padding(
        padding: const EdgeInsets.all(15),
        child: Column(
          children: [
            Text('ادخل بيانات قطعة الغيار '),
            SizedBox(height: 10),
            _drawDropDownButtonBrands(),
            SizedBox(height: 5),
            _drawDropDownButtonModules(),
            _drawYearModelField(),
            SizedBox(height: 20),
            ///////////////////////////////////
            Text('أخبرنا المزيد عن سيارتك ', style: nameProductStyle),
            //SizedBox(height: 10),
            _drawP_PriceField(),
            Row(
              children: [
                Container(width: 100, child: _drawP_PriceField()),
                Expanded(child: Text('االسعر :')),
              ],
            ),
            Row(
              children: [
                Container(width: 100, child: _drawDropDownButtonPartType()),
                Expanded(child: Text('نوع القطعة')),
              ],
            ),

            Row(
              children: [
                Container(width: 100, child: _drawP_StatusField()),
                Expanded(child: Text('حدد حالة القطعة')),
              ],
            ),
            SizedBox(height: 20),
            ////////////////////////////
            Text('تفاصيل أكثر',
                style: nameProductStyle),
            SizedBox(height: 10),
            _drawPartDetails(),
            SizedBox(height: 20),
            ////////////////////////////
            Text('قم بإرفاق صور ', style: nameProductStyle),
            SizedBox(height: 10),
            _drawImages(),
            SizedBox(height: 30),
            ////////////////////////////
            DefaultButton(
              text: "إرسال",
              press: () async {
                //  if (_formKey.currentState.validate()) {
                PartsApi partsApi = PartsApi();
                _formKey.currentState.save();
                Dialogs.showLoadingDialog(context, _keyLoader, "جاري الإرسال");
                Parts p = Parts(
                  //p_id:1 ,
                  pr_name: "kkkkkkkkkkkkk",
                  pr_brand: default_value_Brand.b_id ?? 1,
                  pr_model: default_value_model.mo_id ?? 1,
                  pr_year: pr_year,
                  pr_mfr: '',
                  pr_price: pr_price,
                  pr_currency: 1,
                  pr_detailes: pr_details,
                  pr_cdate: DateTime.now().toString(),
                  pr_comment: '',
                  pr_tag: '',
                  pr_img: pr_img,
                  pr_createdBy: 12,
                  pr_type: 1,
                  is_orginal: 1,
                  is_new: default_value_status.car_stat_id,
                  is_sold: 0,
                  is_available: 1,
                  is_haraj: 1,
                  vin_no: '11111111',
                );
                try {
                  if (await isthereInternet()) {
                    var status = await partsApi.sendPartsInfo(p);


                    if (status == 1) {
                      print('success');
                      ToastUtils.showCustomToast(
                          context, "تم بنجاح .. شكرا لك  ");
                      Navigator.pop(context);
                    } else if (status == 0 || status == -9) {
                      addError(error: kTrySendCarAgain);
                    }
                    Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                        .pop();
                  }
                }
                on Exception catch (e) {
                  print('error${e.toString()}');
                }
                //  }
              },
            ),
            /////////////////////////////
          ],
        ),
      ),
    );
  }

  // for main info /////////////////////////////////
  Widget _drawDropDownButtonBrands() {
    return DropdownButton<Brand>(
      isExpanded: true,
      value: default_value_Brand,
      elevation: 16,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (Brand newValue) {
        setState(() {
          default_value_Brand = newValue;

          for (var item in lstbrands) {
            if (newValue.b_id == item.b_id) {
              getlistmodulesDependonBrand(newValue.b_id);
            }
          }
        });
      },
      items: lstbrands.map<DropdownMenuItem<Brand>>((Brand value) {
        return DropdownMenuItem<Brand>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value.b_title ?? '',
              ),
            ),
          ),
        );
      }).toList(),
    );
  }

  Widget _drawDropDownButtonModules() {
    return DropdownButton<Module>(
      isExpanded: true,
      value: default_value_model,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (Module newValue) {
        setState(() {
          default_value_model = newValue;
        });
      },
      items: lstmodule_brand1.map<DropdownMenuItem<Module>>((Module value) {
        return DropdownMenuItem<Module>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value.mo_title ?? '',
              ),
            ),
          ),
        );
      }).toList(),
    );
  }

  TextFormField _drawYearModelField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      onSaved: (newValue) => pr_year = newValue,
      /*
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: p_YearNullError);
        } else if (value.length > 4) {
          removeError(error: Invalid_p_Year_Error);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: p_YearNullError);
          return "";
        } else if (value.length < 5) {
          addError(error: Invalid_p_Year_Error);
          return "";
        }
        return null;
      },*/
      decoration: textInputDecoration.copyWith(
        hintText: "ادخل سنة الصنع ",
        labelText: 'سنة الصنع',
      ),
    );
  }

  // for more info ////////////////////////////////
  Widget _drawP_PriceField() {
    return TextFormField(
      keyboardType: TextInputType.number,
      onSaved: (newValue) => pr_price = int.tryParse(newValue),
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNumberNullError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: p_PriceNullError);
          return "";
        }
        return null;
      },
      decoration: textInputDecoration.copyWith(
        hintText: "السعر",
        labelText: p_PriceNullError,
        //suffixIcon: Icon(FontAwesomeIcons.car,color: Colors.grey.shade400,),
      ),
    );
  }

  Widget _drawDropDownButtonPartType() {
  }

  Widget _drawP_StatusField() {
    return DropdownButton<Status>(
      isExpanded: true,
      value: default_value_status,
      elevation: 16,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (Status newValue) {
        setState(() {
          default_value_status = newValue;
        });
      },
      items: lststatus.map<DropdownMenuItem<Status>>((Status value) {
        return DropdownMenuItem<Status>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value.car_stat_title,
              ),
            ),
          ),
        );
      }).toList(),
    );
  }

  //////////////////////////////////////
  // for more details /////////////
  Widget _drawPartDetails() {
    return TextFormField(
      keyboardType: TextInputType.multiline,
      onSaved: (newvalue) => pr_details = newValue as String,
      minLines: 1,
      //Normal textInputField will be displayed
      maxLines: 7,
      decoration: textInputDecoration.copyWith(
        hintText: "تفاصيل أكثر ",
        labelText: 'وصف القطعة ',

      ),
    );
  }
  ////////////////////////////////////////////
    Widget _drawImages()
    {
      return Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              RaisedButton(
                // onPressed: _choose,
                onPressed: () {
                  setState(() {
                    if (selected == 0)
                      selected = 1;
                    else
                      selected = 0;
                  });
                },
                child: Text('اضف صورة'),
              ),
              SizedBox(width: 10.0),
              RaisedButton(
                child: Text('إضافة مزيد من الصور'),
                onPressed: () {
                  //  _upload();
                },
              )
            ],
          ),
          selected == 0 ? Container() : _chooseTool(),
          file == null
              ? Text(selectes_image)
              : Container(
            child: Image.file(file),
            height: 100,
            width: 100,
          ),
        ],
      );
    }
  Widget _chooseTool() {
    return Card(
      child: Column(
        children: [
          InkWell(
            child: Row(
              children: [
                Icon(Icons.camera),
                Text('الكاميرا'),
              ],
            ),
            onTap: () {
              _choose(1);
            }, // _choose(1),
          ),
          InkWell(
            child: Row(
              children: [
                Icon(Icons.filter),
                Text('الصور'),
              ],
            ),
            onTap: () {
              _choose(2);
            }, //_choose(2),
          ),
        ],
      ),
    );
  }
  _choose(int i) async {
    i == 1
        ? file = await ImagePicker.pickImage(source: ImageSource.camera)
        : file = await ImagePicker.pickImage(source: ImageSource.gallery);

    if (file == null) return;
    String base64Image = base64Encode(file.readAsBytesSync());
    String fileName = file.path.split("/").last;
    pr_img = base64Image;

    print('base64${base64Image}');
    print('file${fileName}');
    setState(() {
      file = file;
    });
  }

}