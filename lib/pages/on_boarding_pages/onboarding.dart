import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_flutter_app/pages/home_page.dart';
import 'package:my_flutter_app/pages/home_page2.dart';
import 'package:my_flutter_app/pages/on_boarding_pages/on_boarding_items.dart';
import 'package:my_flutter_app/pages/other_pages/splash_page.dart';
import 'package:page_view_indicator/page_view_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:splashscreen/splashscreen.dart';

class OnBoarding extends StatefulWidget {
  static const String route_name = '/onBoarding';

  @override
  _OnBoardingState createState() => _OnBoardingState();
}

class _OnBoardingState extends State<OnBoarding> {
  List<OnBoardingItems> contents;
  ValueNotifier<int> _pageViewNotifier = ValueNotifier(0);
  List<String> images = [
    'assets/images/bg_first_board.png',
    'assets/images/bg_second_board.png',
    'assets/images/bg_third_board.png',
    'assets/images/bg_forth_board.png',
  ];
  List<IconData> icons = [
    Icons.ac_unit,
    Icons.directions_car,
    Icons.accessibility_new,
    Icons.account_balance_wallet,
  ];
  List<String> titles = [
    'اهلا بك في عالم السيارات الإلكتروني',
    'إضافة سيارتك للبيع بكل يسر وسهولة, البحث من خلال التطبيق عن السيارة المطلوبة بكل يسر وسهولة , كذلك يتضمن التطبيق عرضا تفصيليا من صور ومعلومات عن السيارة المعروضة وتوفر قطع غيارها بلمسة زر',
    'يوفر التطبيق أفضل المراكز لصيانة سيارتك بحيث يمكنك تصفح مراكز الصيانة والحلول من خلال تبويب فنيين ',
    'التطبيق الاول يضم اكبر سوق حراج للسيارات والاعلان عن سيارتك بكل سهولة ',
  ];
  List<String> desc = [
    'عالم السيارات بضغطة زر',
    '',
    '',
    'راحتك تهمنا .... انطلق',
  ];
  @override
  Widget build(BuildContext context) {
    _AddContent();

    return Stack(
      children: <Widget>[
        Scaffold(
          body: PageView.builder(
            itemBuilder: (context, index) {
              return Stack(
                alignment: Alignment.center,
                children: <Widget>[
                  Container(
                    //color: Theme.of(context).primaryColor,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: ExactAssetImage(
                          contents[index].images,
                        ),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Transform.translate(
                        child: Icon(contents[index].icons,
                            size: 90, color: Colors.white),
                        //to up the icon from texts use offset
                        offset: Offset(0, -50),
                      ),
                      Text(
                        (contents[index].titles),
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      Padding(
                        padding:
                            const EdgeInsets.only(right: 48, left: 48, top: 18),
                        child: Text(
                          contents[index].desc,
                          style: TextStyle(
                              color: Colors.white70,
                              fontSize: 18,
                              fontWeight: FontWeight.bold),
                          textAlign: TextAlign.center,
                        ),
                      ),
                    ],
                  )
                ],
              );
            },
            itemCount: contents.length,
            onPageChanged: (index) {
              _pageViewNotifier.value = index;
            },
          ),
        ),
        Transform.translate(
          offset: Offset(0, 175),
          child: Align(
            alignment: Alignment.center,
            child: _displayPageIndicators(contents.length),
          ),
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: Padding(
            padding: const EdgeInsets.only(bottom: 20, left: 15, right: 15),
            child: SizedBox(
              width: double.infinity,
              height: 50,
              child: RaisedButton(
                color: Theme.of(context).primaryColor,
                child: Text(
                  'إبدا',
                  style: TextStyle(color: Colors.white, fontSize: 24),
                ),
                onPressed: () {
                  _updateSeen();
                  // Navigator.pushReplacementNamed(context, HomePage.route_name);
                  Navigator.pushReplacementNamed(context, Splash.route_name);
                },
              ),
            ),
          ),
        )
      ],
    );
  }

  // to set info
  void _AddContent() {
    contents = List<OnBoardingItems>();
    //first onBoarding
    contents.add(
      OnBoardingItems(
        titles[0],
        desc[0],
        images[0],
        icons[0],
      ),
    );
    //second onBoarding
    contents.add(
      OnBoardingItems(
        titles[1],
        desc[1],
        images[1],
        icons[1],
      ),
    );
    //third onBoarding
    contents.add(
      OnBoardingItems(
        titles[2],
        desc[2],
        images[2],
        icons[2],
      ),
    );
    //forth onBoarding
    contents.add(
      OnBoardingItems(
        titles[3],
        desc[3],
        images[3],
        icons[3],
      ),
    );
  }

  Widget _displayPageIndicators(int length) {
    return PageViewIndicator(
      pageIndexNotifier: _pageViewNotifier,
      length: length,
      normalBuilder: (animationController, index) => Circle(
        size: 8.0,
        color: Colors.grey,
      ),
      highlightedBuilder: (animationController, index) => ScaleTransition(
        scale: CurvedAnimation(
          parent: animationController,
          curve: Curves.ease,
        ),
        child: Circle(
          size: 12.0,
          color: Theme.of(context).primaryColor,
        ),
      ),
    );
  }

  void _updateSeen() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setBool('seen', true);
  }
}
