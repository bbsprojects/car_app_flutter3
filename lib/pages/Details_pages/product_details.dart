import 'package:cache_image/cache_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:my_flutter_app/database/database_provider.dart';
import 'package:my_flutter_app/model/currentmember.dart';
import 'package:my_flutter_app/model/favorite.dart';
import 'package:my_flutter_app/model/products.dart';
import 'package:my_flutter_app/utils/api_path.dart';
import 'package:parallax_image/parallax_image.dart';
import '../../utils/constants.dart';

class ProductDetails extends StatefulWidget {
  static const String route_name = '/ProductDetailsPage';
  Products p;
  ProductDetails({this.p});

  @override
  _ProductDetailsState createState() => _ProductDetailsState(p: p);
}

class _ProductDetailsState extends State<ProductDetails> {
  Products p;
  _ProductDetailsState({this.p});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            actions: <Widget>[Icon(Icons.search)],
            backgroundColor: kPrimaryColor,
            expandedHeight: 200,
            floating: true,
            pinned: true,
            centerTitle: true,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              title: Text(
                'p.p_brand_name' + " " + p.name + " " + p.p_year,
                style: TextStyle(
                    fontSize: 17,
                    color: Colors.white,
                    fontWeight: FontWeight.w500),
              ),
              background: Container(
                child: FadeInImage(
                  placeholder:
                  ExactAssetImage('assets/images/bg_second_board.png'),
                  image: CacheImage('${p.p_img}'),
                  fit: BoxFit.cover,
                ),
              ),

            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (context, position) {
                // check for favorite ///
                if (p.fav == null)
                  p.fav = false;
                DatabaseProvider.db
                    .isExistinFavtProduct(
                    p.p_id.toString())
                    .then((value) => {
                  if (value)
                    setState(() => p.fav = true)
                });
                /////////////////////
                if (position == 0) {
                  return _drawProductDetails();
                } else if (position == 1) {
                  return _drawMainDetails();
                } else if (position == 2) {
                  return _drawMoreDetails();
                } else if (position == 3) {
                  return _drawMoreDetailsContent();
                } else if (position == 4) {
                  return _drawMoreImages();
                } else {
                  return _drawFooterProdectDetails();
                }
              },
              childCount: 6,
            ),
          ),
        ],
      ),
    );
  }

  Widget _drawProductDetails() {
    return Card(
      shadowColor: Colors.white70,
      color: Colors.grey.shade300,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(CarDetails),
            Text(p.p_price.toString() + '  ' + p.currency),
        IconButton(
            icon: Icon(
              (p.fav)
                  ? Icons.favorite
                  : Icons.favorite_border,
              color: (p.fav)
                  ? kPrimaryColor
                  : Colors.grey[600],
            ),
            onPressed: () {
              if (!p.fav) {
                DatabaseProvider.db.insertFavorite(Favorite(
                    f_date: DateTime.now().toString(),
                    f_prd:
                    p.p_id,
                    f_prt: 0)).then((value) =>
                {
                  DatabaseProvider.db.insertProduct(p),
                  setState(() {
                    p.fav = !p.fav;
                  })
                });
              }
              else {
                DatabaseProvider.db.deleteFavoritePoduct(
                    p.p_id.toString())
                    .then((value) => {
                  DatabaseProvider.db.deleteProduct(p).then((value) =>
                  {
                    setState(
                            () {
                          p.fav = !p.fav;
                        })
                  })
                });
              }
            }),
              //color:_is_selected ?true? Colors.grey.shade500,

          ],
        ),
      ),
    );
  }

  Widget _drawMainDetails() {
    return Card(
      child: Padding(
        padding: const EdgeInsets.only(
          top: 15.0,
          bottom: 25,
          left: 20,
          right: 20,
        ),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(first_dt),
                SizedBox(
                  width: 40,
                ),
                Text('p.p_brand_name'),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,

              children: <Widget>[
                Text(second_dt),
                SizedBox(
                  width: 40,
                ),
                Text(p.p_name),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,

              children: <Widget>[
                Text(third_dt),
                SizedBox(
                  width: 40,
                ),
                Text(p.p_year),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(forth_dt),
                SizedBox(
                  width: 40,
                ),
                p.is_new == 0 ? Text('مستخدمة') : Text('جديد'), // TO DO
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(fifth_dt),
                SizedBox(
                  width: 40,
                ),
                //Text('اللون  ${p.p_color ?? ''}'),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(sixth_dt),
                SizedBox(
                  width: 40,
                ),
                Text('ناقل الحركة' + p.p_fuel.toString()),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,

              children: <Widget>[
                Text(sev_dt),
                SizedBox(
                  width: 40,
                ),
                Text('الممشى  ' + p.p_mileage + '  كــم'),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _drawMoreDetails() {
    return Card(
      shadowColor: Colors.white70,
      color: Colors.grey.shade300,
      child: Padding(
        padding: const EdgeInsets.only(top:12.0,bottom: 12,left: 8,right: 8),
        child: Text(CarMoreDetails),
      ),
    );
  }

  Widget _drawMoreDetailsContent() {
    return Card(
      child: Padding(
        padding: const EdgeInsets.only(
          top: 15.0,
          bottom: 25,
          left: 20,
          right: 20,
        ),
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,

              children: <Widget>[
                Text(eigh_dt),
                SizedBox(
                  width: 40,
                ),
                Text('p.p_brand_name'),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(second_dt),
                SizedBox(
                  width: 40,
                ),
                Text(p.name),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(nin_dt),
                SizedBox(
                  width: 40,
                ),
                Text(p.p_year),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(tn_dt),
                SizedBox(
                  width: 40,
                ),
                if (p.is_new == 0) Text('جديدة'),
                if (p.is_new == 1) Text('مستخدمة'),
                if (p.is_new == null) Text('empty'),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _drawMoreImages() {
    return new Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Container(
          padding: const EdgeInsets.all(8.0),
          child: new Text(
            'مزيد من الصور',
            //style: textTheme.title,
          ),
        ),
        new Container(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          constraints: const BoxConstraints(maxHeight: 200.0),
          child: new ListView.builder(
            scrollDirection: Axis.horizontal,
            itemBuilder: _buildHorizontalChild,
          ),
        ),
      ],
    );
  }

  Widget _drawFooterProdectDetails() {
    return Padding(
      padding: const EdgeInsets.only(bottom: 15.0,left: 8,right: 8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Column(
            children: <Widget>[
              IconButton(
                  icon: Icon(Icons.shopping_basket,
                      color: Theme.of(context).primaryColor),
                  onPressed: () {}),
              Text('شراء'),
            ],
          ),
          Column(
            children: <Widget>[
              IconButton(
                  icon: Icon(Icons.favorite,
                      color: Theme.of(context).primaryColor),
                  onPressed: () {}),
              Text('إضافة للمفضلة '),
            ],
          ),
          Column(
            children: <Widget>[
              IconButton(
                  icon: Icon(Icons.star, color: Theme.of(context).primaryColor),
                  onPressed: () {}),
              Text('تقييم'),
            ],
          ),
        ],
      ),
    );
  }

  Widget _buildHorizontalChild(BuildContext context, int index) {
    index++;
    if (index > 7) return null;
    return new Padding(
      padding: const EdgeInsets.only(right: 10.0),
      child: new ParallaxImage(
        extent: 100.0,
        /* image: new ExactAssetImage(
          'images/img$index.jpg',
        ),*/
        image: ExactAssetImage('assets/images/bg_second_board.png'),
      ),
    );
  }
}
