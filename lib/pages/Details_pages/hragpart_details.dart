import 'package:cache_image/cache_image.dart';
import 'package:flutter/material.dart';
import 'package:my_flutter_app/database/database_provider.dart';
import 'package:my_flutter_app/model/favorite.dart';
import 'package:my_flutter_app/model/parts.dart';
import 'package:my_flutter_app/pages/other_pages/comments_list.dart';
import 'package:my_flutter_app/utils/api_path.dart';
import 'package:my_flutter_app/utils/constants.dart';
import 'package:timeago/timeago.dart' as timeago;

class HragPartDetails extends StatefulWidget {
  Parts part;
  HragPartDetails({this.part});
  @override
  _HragPartDetailsState createState() => _HragPartDetailsState(part: part);
}

class _HragPartDetailsState extends State<HragPartDetails> {
  Parts part;
  _HragPartDetailsState({this.part});

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('تفاصيل القطعة'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Column(
            children: [
              Card(
                  elevation: 0,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          Container(
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                            ),
                            height: 45,
                            width: 45,
                            margin: EdgeInsets.only(left: 8.0, right: 5),
                            child: ClipRRect(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(25)),
                              child: FadeInImage(
                                fit: BoxFit.cover,
                                placeholder:
                                    AssetImage('assets/images/unknow.jpg'),
                                image: AssetImage('${base_api + part.m_img}'),
                              ),
                            ),
                          ),
                          //colum name with date
                          Column(
                            children: [
                              Text('${part.name}'),
                              Text(
                                '${timeago.format(DateTime.parse(part.pr_cdate), locale: 'en_short')}',
                              )
                            ],
                          )
                        ],
                      ),
                      Column(
                        children: [
                          Icon(
                            Icons.place,
                            color: kTextColor,
                          ),
                          Text(
                            'الرياض',
                            style: TextStyle(color: kTextColor),
                          ),
                        ],
                      )
                    ],
                  )),
              Container(
                width: double.infinity,
                height: 200,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20),
                ),
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: FadeInImage(
                    placeholder:
                        ExactAssetImage('assets/images/bg_second_board.png'),
                    image: CacheImage(part.pr_img ?? ''),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Card(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 20, right: 20, top: 10),
                      child: Column(
                        children: [
                          Text(
                            '${part.pr_name}',
                            style: nameProductStyle,
                          ),
                          Text(
                            '${part.pr_price} ${part.currency}',
                            style: priceProductStyle,
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.grey,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        RaisedButton.icon(
                            color: kPrimaryColor,
                            label: Text(
                              'إضافة للسلة',
                              style: TextStyle(color: Colors.white),
                            ),
                            icon:
                                Icon(Icons.shopping_cart, color: Colors.white),
                            onPressed: () {}),
                        IconButton(
                            icon: Icon(
                                part.fav
                                    ? Icons.favorite
                                    : Icons.favorite_border,
                                color: part.fav ? kPrimaryColor : Colors.grey),
                            onPressed: () {
                              if (!part.fav) {
                                DatabaseProvider.db
                                    .insertFavorite(Favorite(
                                        f_date: DateTime.now().toString(),
                                        f_prt: part.pr_id,
                                        f_prd: 0))
                                    .then((value) => {
                                          DatabaseProvider.db.insertParts(part),
                                          setState(() {
                                            part.fav = !part.fav;
                                          })
                                        });
                              } else {
                                DatabaseProvider.db
                                    .deleteFavoritePart(part.pr_id.toString())
                                    .then((value) => {
                                          DatabaseProvider.db
                                              .deleteParts(part)
                                              .then((value) => {
                                                    setState(() {
                                                      part.fav = !part.fav;
                                                    })
                                                  })
                                        });
                              }
                            }),
                      ],
                    )
                  ],
                ),
              ),
              _drawMainDetails(),
              Container(
                child: Column(
                  children: [
                    CommentsList(),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _drawMainDetails() {
    return Card(
      elevation: 0,
      child: Padding(
        padding: const EdgeInsets.only(
          top: 15.0,
          bottom: 25,
        ),
        child: Column(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("البراند"),
                  SizedBox(
                    width: 40,
                  ),
                  Text('${part.brand}'),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("الموديل"),
                  SizedBox(
                    width: 40,
                  ),
                  Text('${part.model}'),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("سنة الصنع"),
                  SizedBox(
                    width: 40,
                  ),
                  Text('${part.pr_year}'),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("نوع الفطعة"),
                  SizedBox(
                    width: 40,
                  ),
                  part.is_orginal == 1 ? Text('اصلي') : Text('تقليد'),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("الوصف"),
                  SizedBox(
                    width: 40,
                  ),
                  Text('${part.pr_detailes}'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
