import 'package:cache_image/cache_image.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_flutter_app/model/parts.dart';
import 'package:my_flutter_app/pages/other_pages/add_car.dart';
import 'package:my_flutter_app/utils/constants.dart';

class PartDetails extends StatefulWidget {
  Parts part;
  PartDetails({this.part});
  @override
  _PartDetailsState createState() => _PartDetailsState(part: part);
}

class _PartDetailsState extends State<PartDetails> {
  Parts part;
  TextStyle _hashTagStyle = (TextStyle(color: Colors.deepOrange, fontSize: 20));
  _PartDetailsState({this.part});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('تفاصيل القطعة'),
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: Column(
            children: [
              Card(),
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  Container(
                    width: double.infinity,
                    height: 200,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                    ),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: FadeInImage(
                        placeholder: ExactAssetImage(
                            'assets/images/bg_second_board.png'),
                        image: CacheImage(part.pr_img ?? ''),
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      IconButton(
                          icon: Icon(Icons.favorite_border, color: Colors.grey),
                          onPressed: () {}),
                      IconButton(
                          icon: Icon(
                            FontAwesomeIcons.shareSquare,
                            color: Colors.grey,
                          ),
                          onPressed: () {}),
                    ],
                  )
                ],
              ),
              Card(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding:
                          const EdgeInsets.only(left: 20, right: 20, top: 10),
                      child: Column(
                        children: [
                          Text(
                            '${part.name}',
                            style: nameProductStyle,
                          ),
                          Text(
                            '${part.pr_price} ${part.currency}',
                            style: priceProductStyle,
                          ),
                        ],
                      ),
                    ),
                    Divider(
                      color: Colors.grey,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            //Text('الكميـة :'),
                            FlatButton(
                              onPressed: () {},
                              child: Text(
                                '+',
                                style: _hashTagStyle,
                              ),
                            ),
                            Text('1'),
                            FlatButton(
                              onPressed: () {},
                              child: Text(
                                '-',
                                style: _hashTagStyle,
                              ),
                            ),
                          ],
                        ),
                        InkWell(
                          child: Row(
                            children: [
                              IconButton(
                                onPressed: () {},
                                icon: Icon(
                                  Icons.shopping_cart,
                                  color: kPrimaryColor,
                                ),
                              ),
                              Text(
                                'اضف إلى العربة',
                                style: _hashTagStyle,
                              )
                            ],
                          ),
                          onTap: () {},
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              _drawMainDetails(),
            ],
          ),
        ),
      ),
    );
  }

  Widget _drawMainDetails() {
    return Card(
      elevation: 0,
      child: Padding(
        padding: const EdgeInsets.only(
          top: 15.0,
          bottom: 25,
        ),
        child: Column(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("البراند"),
                  SizedBox(
                    width: 40,
                  ),
                  Text('${part.brand}'),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("الموديل"),
                  SizedBox(
                    width: 40,
                  ),
                  Text('${part.model}'),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("سنة الصنع"),
                  SizedBox(
                    width: 40,
                  ),
                  Text('${part.pr_year}'),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("نوع الفطعة"),
                  SizedBox(
                    width: 40,
                  ),
                  part.is_orginal == 1 ? Text('اصلي') : Text('تقليد'),
                ],
              ),
            ),
            Container(
              padding: const EdgeInsets.all(8),
              decoration: BoxDecoration(
                  color: Colors.grey[300],
                  borderRadius: BorderRadius.circular(10)),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text("الوصف"),
                  SizedBox(
                    width: 40,
                  ),
                  Text('${part.pr_detailes}'),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
