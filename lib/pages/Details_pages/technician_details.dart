import 'package:flutter/material.dart';
import 'package:my_flutter_app/model/currentmember.dart';
import 'package:my_flutter_app/model/portofolio.dart';
import 'package:my_flutter_app/utils/api_path.dart';
import 'package:my_flutter_app/utils/constants.dart';

class TecnicianDetails extends StatefulWidget {
  static const String route_name = '/TecnicianDetailsPage';
  Portfolio member;
  TecnicianDetails({this.member});

  @override
  _TecnicianDetailsState createState() => _TecnicianDetailsState(member:member);
}

class _TecnicianDetailsState extends State<TecnicianDetails> {
  Portfolio member;
  _TecnicianDetailsState({this.member});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            //actions: <Widget>[Icon(Icons.search)],
            expandedHeight: 200,
            backgroundColor: kPrimaryColor,
            floating: true,
            pinned: true,
            centerTitle: true,
            flexibleSpace: FlexibleSpaceBar(
              centerTitle: true,
              title: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    //'p.p_brand_name' + " " + p.name + " " + p.p_year
                       'بيبي سوفت ',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                        fontWeight: FontWeight.w500),
                  ),
                  Text(
                    //'p.p_brand_name' + " " + p.name + " " + p.p_year
                    'كهربائي وميكانيك ',
                    style: TextStyle(
                        fontSize: 14,
                        color: Colors.white,
                        fontWeight: FontWeight.w500),
                  ),
                ],
              ),
              background:Stack(
                children: [
                  Container(
                    height: double.infinity,
                    width: double.infinity,
                    color: Colors.transparent,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top:30.0),
                    child: new Positioned(
                      child: new Align(
                        alignment: Alignment.topCenter,
                        child: ClipRRect(
                            child: FadeInImage(
                              width: 90,
                              height: 90,
                              fit: BoxFit.cover,
                              placeholder: AssetImage(
                                  'assets/images/unknow.jpg'),
                              image: AssetImage(
                                  '${base_api + CurrentMember.m_img}'
                              ),
                            ),
                            borderRadius:
                            BorderRadius.all(
                                Radius.circular(50))),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
                  (context, position) {
                    /*
                // check for favorite ///
                if (p.fav == null)
                  p.fav = false;
                DatabaseProvider.db
                    .isExistinFavtProduct(
                    p.p_id.toString())
                    .then((value) => {
                  if (value)
                    setState(() => p.fav = true)
                });*/
                /////////////////////
                if (position == 0) {
                  return _drawTecnicianavailable_dayAndCat();
                }
                else if (position == 1) {
                  return _drawTecnicianPersonalInfo();
                }
                else if (position == 2) {
                  return _drawTecnicianSkills();
                } else if (position == 3) {
                  return _drawTecniciancert();
                } else if (position == 4) {
                  return _drawTecnicianaccomplishment();
                }  else {
                  return Container();
                }
              },
              childCount: 5,
            ),
          ),
        ],
      ),

    );
  }

  Widget _drawTecnicianavailable_dayAndCat() {
    return Column(children: [
      Container(height: 200,
      width: 200,
      color: Colors.blue,),
      Container(height: 200,
        width: 200,
        color: Colors.amber,),
      Container(height: 200,
        width: 200,
        color: Colors.blue,),
      Container(height: 200,
        width: 200,
        color: Colors.cyanAccent,),
      Container(height: 200,
        width: 200,
        color: Colors.blue,),
      Container(height: 200,
        width: 200,
        color: Colors.indigoAccent,),
    ],);
  }

  Widget _drawTecnicianPersonalInfo() { return Text('');
  }

  Widget _drawTecnicianSkills() { return Text('');
  }

  Widget _drawTecniciancert() { return Text('');
  }

  Widget _drawTecnicianaccomplishment() { return Text('');
  }
}
