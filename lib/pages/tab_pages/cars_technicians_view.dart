import 'package:flutter/material.dart';
import 'package:my_flutter_app/api/technician_api.dart';
import 'package:my_flutter_app/model/members.dart';
import 'package:my_flutter_app/model/portofolio.dart';
import 'package:my_flutter_app/pages/Details_pages/technician_details.dart';
import 'package:my_flutter_app/utils/constants.dart';
class CarsTechniciansView extends StatefulWidget {

  static const String route_name = '/technicianspage';

  @override
  _CarsTechniciansViewState createState() => _CarsTechniciansViewState();
}

class _CarsTechniciansViewState extends State<CarsTechniciansView> {
  bool isLoading = true;
  TechniciansApi techniciansApi = TechniciansApi();
  List<Members> lsttechnicians = List<Members>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
      get_Data();
    print("isLoading $isLoading");
  }
  get_Data() async {
    try {
      if (await isthereInternet()) {
        lsttechnicians = await techniciansApi.fetchAllTechnicians(100);
      }
    }
    catch (Excaption) {}
    print('lenght of lsttechnicians:${lsttechnicians.length}');
    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //backgroundColor: kPrimaryLightColor,
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
        title: Text('فنيين'),
    ),
    body: ListView.builder(
      padding: EdgeInsets.all(8),
      itemBuilder: (context,position)
      {
        return InkWell(
          onTap: (){
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => TecnicianDetails(
                     // member: lsttechnicians[position],
                    )));
          },
          child: Padding(
            padding: const EdgeInsets.only(bottom:8.0),
            child: Card(
              color: Colors.grey.shade100,
              child:Column(
                children: <Widget>[
                  /*
                  // action of cart
                  ButtonBarTheme(
                    child: ButtonBar(
                      children: <Widget>[
                        FlatButton(
                          child: const Text(''),onPressed: (){},),
                        FlatButton(
                          child: const Text(''),
                          onPressed: (){},
                        // ignore: deprecated_member_use  ),],),),*/
                  _cardHeader(),
                  _cardBody(),
                  _drawLine(),
                  _cardFooter(),
                ],
              ) ,
            ),
          ),
        );
      },
      itemCount: 20,
    )
    );
  }

  Widget _cardHeader()
  {
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: CircleAvatar(backgroundImage:
          ExactAssetImage('assets/images/logo_car_techniqor.png'),
            radius: 27,
          ),
        ),
        Expanded(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text('بي بي سوفت ',
                    style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(width: 15,),
                  Text('سمكره وكهرباء',
                    style: TextStyle(color: Colors.grey),
                    maxLines: 2,
                  ),
                ],
              ),
              SizedBox(
                height: 8,
              ),
              Text('الايام المتاحة : السبت , الخميس ',
                style: TextStyle(color: Colors.grey,),
                maxLines:2,

              ),
            ],
          ),
        ),
      ],
    );

  }

  Widget _cardBody(){
    return Padding(
      padding: const EdgeInsets.only(top:8.0,left: 8,right: 8),
      child: Text('شرح عن امكانيات المهندس ما هو ادخلها ', style: TextStyle(
        fontSize: 17,
        fontWeight: FontWeight.w500,
      ),
      ),
    );
  }

  Widget _cardFooter(){
    return Padding(
      padding: const EdgeInsets.all(16),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: <Widget>[
          Row(
            children: <Widget>[
              IconButton(icon: Icon(Icons.more,size: 28,),
                color:Theme.of(context).primaryColor,
                onPressed: (){},
              ),
              Text("المزيد",style: TextStyle(color: Colors.grey),),
            ],
          ),
          Row(
            children: <Widget>[
              FlatButton(
                onPressed: (){},
                child: Text('تقييم',style: TextStyle(
                    color: Theme.of(context).primaryColor
                ),
                ),
              ),
              FlatButton(
                onPressed: (){},
                child: Text('طلب',style: TextStyle(
                    color: Theme.of(context).primaryColor
                ),),
              ),
            ],
          ),
        ],
      ),
    );

  }

  Widget _drawLine() {
    return Container(
      height: 1,
      color: Colors.grey.shade200,
      margin: EdgeInsets.only(top: 15),
    );
  }
}
