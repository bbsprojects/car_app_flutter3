import 'package:cache_image/cache_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_flutter_app/api/brand_api.dart';
import 'package:my_flutter_app/database/database_provider.dart';
import 'package:my_flutter_app/pages/menu_pages/products_view.dart';
import 'package:my_flutter_app/model/brand.dart';
import 'package:my_flutter_app/utils/constants.dart';

class AllBrands extends StatefulWidget {
  static const String rout_name = '/brands';

  @override
  _State createState() => _State();
}

class _State extends State<AllBrands> {
  List<Brand> popularlstBrands = List<Brand>();
  List<Brand> popularlstBrands2 = List<Brand>();
  List<Brand> alllstBrands = List<Brand>();
  BrandApi brandApi = BrandApi();
  bool isLoading = true;
  String title;
  // int lstlength;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("isLoading $isLoading");
    get_Data();
    title = 'أكثر الماركات شهرة';
    //lstlength = popularlstBrands.length;
  }

  get_Data() async {
    print("isssssLoading $isLoading");
    await DatabaseProvider.db.getBrands().then((value) {
      setState(() {
        alllstBrands = value;
      });
    });

    if (alllstBrands.length > 0) {
      popularlstBrands.add(alllstBrands[0]);
      if (alllstBrands.length > 1) {
        popularlstBrands.add(alllstBrands[1]);
        if (alllstBrands.length > 2) {
          popularlstBrands.add(alllstBrands[2]);
          if (alllstBrands.length > 3) {
            popularlstBrands.add(alllstBrands[3]);
            if (alllstBrands.length > 4) {
              popularlstBrands.add(alllstBrands[4]);
              if (alllstBrands.length > 5) {
                popularlstBrands.add(alllstBrands[5]);
              }
            }
          }
        }
      }
    }

    popularlstBrands2 = popularlstBrands;
    print('lenght:${popularlstBrands.length}');

    for (var item in popularlstBrands) {
      print(('Brand Name:${item.b_title}'));
    }
    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('الماركات'),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(top: 19, bottom: 10),
            child: Text(
              title,
              style: styleSectionTitle,
            ),
          ),
          _drawMostPopularBrand(),
          alllstBrands.length != popularlstBrands2.length
              ? Padding(
                  padding: const EdgeInsets.only(top: 19, bottom: 10),
                  child: InkWell(
                    child: Text(
                      'كل الماركات',
                      style: styleSectionTitle,
                    ),
                    onTap: () {
                      setState(() {
                        popularlstBrands2 = alllstBrands;
                        print(popularlstBrands.length);
                        title = "كل الماركات";
                      });
                    },
                  ),
                )
              : Container(),
        ],
      ),
    );
  }

  Widget _drawMostPopularBrand() {
    return Expanded(
      child: GridView.count(
        crossAxisCount: 3,
        //crossAxisSpacing: 5,
        //mainAxisSpacing: 5.0,
        shrinkWrap: true,
        children: List.generate(
          popularlstBrands2.length,
          (index) {
            return GestureDetector(
              child: Padding(
                padding: const EdgeInsets.all(0.0),
                child: Card(
                  child: Container(
                    margin: const EdgeInsets.all(10),
                    child: Column(
                      children: [
                        Container(
                          width: 60,
                          height: 60,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(20),
                            child: FadeInImage(
                              placeholder: ExactAssetImage(
                                  'assets/images/bg_second_board.png'),
                              image: CacheImage(popularlstBrands2[index].b_img),
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        Text(
                          popularlstBrands2[index].b_title,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) {
                    return ProductsView(
                      brand: popularlstBrands2[index],
                    );
                  }),
                );
              },
            );
          },
        ),
      ),
    );
  }
}
