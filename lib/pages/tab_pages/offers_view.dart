
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OffersView extends StatefulWidget {
  @override
  _OffersViewState createState() => _OffersViewState();
}

class _OffersViewState extends State<OffersView> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      padding: EdgeInsets.all(10),
        itemBuilder: (context,position){
          return Card(
            margin: EdgeInsets.only(bottom: 10),
            child: Column(children: <Widget>[
              _drawSingleRowNewHyundai(),
              _drawOffersCart(
                  Theme.of(context).accentColor,
                  'عرض ذهبي'),
              SizedBox(height: 10,),
            ],
              mainAxisAlignment: MainAxisAlignment.start,
            ) ,
          );
    },
    itemCount: 20,
    );
  }


  Widget _drawSingleRowNewHyundai()
  {
    return Padding(
      padding: const EdgeInsets.all(12.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          SizedBox(
            child: Image(
              image:
              ExactAssetImage('assets/images/bg_second_board.png'),
              fit: BoxFit.cover,
            ),
            width: 100,
            height: 100,
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(

            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('هونداي النترا  2017',maxLines: 3,
                  style: TextStyle(
                    fontSize: 17,
                    fontWeight: FontWeight.w500,
                  ),
                ),
                SizedBox(
                  height: 10,
                ),
                Text('معلومات عن السيارة لللللللللللللللللللللللللللللللللللللسبالفنخيوجئسةثليطبيلايجرن0 ',
                  style: TextStyle(
                    fontSize: 14,
                    fontWeight: FontWeight.w500,
                  ),),
              ],
            ),
          ),


        ],
      ),
    );


  }

  _drawOffersCart(Color color ,String category) {
    return Column(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right:16,left: 16, ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Text('RY',style:TextStyle(
                    color: Theme.of(context).primaryColor,
                  ),
                  ),
                  SizedBox(
                    width: 1,
                  ),
                  Text('160000',style: TextStyle(
                    color: Theme.of(context).primaryColor,
                  ),
                  ),
                  SizedBox(
                    width:7,
                  ),
                  Text('السعر: '),

                ],
              ),

              Row(
                children: <Widget>[
                  IconButton(icon: Icon(Icons.shopping_cart,
                      color: Theme.of(context).primaryColor
                  ), onPressed: () {}),
                  Text('شراء',style: TextStyle(
                    color: Theme.of(context).primaryColor,
                  ),),

                ],
              ),
            ],
          ),
        ),

        Padding(
          padding: const EdgeInsets.only(left: 16,right: 16),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Container(
                padding: EdgeInsets.only(
                    left: 30 , right: 30 ,top: 1 , bottom: 1),
                decoration: BoxDecoration(
                  color: color,
                  borderRadius: BorderRadius.circular(8),

                ),
                child:Text(category,
                  style: TextStyle(color: Colors.white70,
                      fontWeight: FontWeight.w600),
                ),

              ),
              SizedBox(
                width: 2,
              ),
              Row(
                children: <Widget>[
                  Icon(Icons.local_offer,
                      color:Colors.grey,
                  ),

                  Text('15%'),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

}
