import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_flutter_app/menu/my_drawer.dart';
import 'package:my_flutter_app/utils/constants.dart';

class AboutPage extends StatelessWidget {

  static const String route_name='/aboutPage';

  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('حول'),
      ),
      body: SizedBox(
        width: double.infinity,
        height: double.maxFinite,
        child: Center(
          child: Padding(
            padding: const EdgeInsets.only(left:10.0,right: 10,top: 40),
            child: Stack(
              alignment: FractionalOffset.topCenter,
              children: [
                Card(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text('تطبيق السيارات الذي يوفر سيارات جديدة من المعارض وتصفحها واحدث'
                        ' البراندات والسيارات وامكانية بيع وشراء اليارات وفنيين '
                        'وكل مستلزمات السيارات من خدمات كـ تنجيد وميكانيك وسرويس '
                        'طبيق السيارات الذي يوفر سيارات جديدة من المعارض وتصفحها واحدث'
                        ' البراندات والسيارات وامكانية بيع وشراء اليارات وفنيين'
                        'طبيق السيارات الذي يوفر سيارات جديدة من المعارض وتصفحها واحدث'
                        'ومعارض الزينة والاكسسوارات وغيرها ..........',
                    style: TextStyle(
                      fontSize: 17
                    ),),
                  ),
                ),
                SizedBox(height: 20),
                new Positioned(
                  child: new Align(
                    alignment: FractionalOffset.bottomCenter,
                    child: Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Expanded(
                            child: Container(
                              width: 150,
                              height: 120,
                              decoration: BoxDecoration(
                                image: DecorationImage(
                                    image: AssetImage('assets/images/about.png')),
                              ),
                            ),
                          ),

                          IconButton(
                            icon: Icon(FontAwesomeIcons.facebook,color: kPrimaryColor,size: 27,),
                            onPressed: (){},
                          ),
                          IconButton(
                            icon: Icon(FontAwesomeIcons.twitter,color: kPrimaryColor,size: 27,),
                            onPressed: (){},
                          ), IconButton(
                            icon: Icon(FontAwesomeIcons.instagram,color: kPrimaryColor,size: 27,),
                            onPressed: (){},
                          ), IconButton(
                            icon: Icon(FontAwesomeIcons.whatsapp,color: kPrimaryColor,size: 27,),
                            onPressed: (){},
                          ),
                          IconButton(
                            icon: Icon(FontAwesomeIcons.linkedin,color: kPrimaryColor,size: 27,),
                            onPressed: (){},
                          ),
                        ],
                      ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );

  }
}
