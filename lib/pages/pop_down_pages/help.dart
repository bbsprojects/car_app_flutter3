import 'package:flutter/material.dart';
import 'package:my_flutter_app/menu/my_drawer.dart';

class HelpPage extends StatelessWidget {

  static const String route_name='/helpPage';

  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: AppBar(
        title: Text('مساعدة'),
      ),
      drawer: MyDrawer(),
      body: Center(
        child: Text('هذه هي شاشة المساعدة'),
      ),
    );

  }
}
