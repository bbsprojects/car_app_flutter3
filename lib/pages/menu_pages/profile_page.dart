import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:my_flutter_app/database/database_provider.dart';
import 'package:my_flutter_app/model/currentmember.dart';
import 'package:my_flutter_app/model/members.dart';
import 'package:my_flutter_app/utils/api_path.dart';
import 'package:my_flutter_app/utils/constants.dart';
import 'package:my_flutter_app/utils/page_routes.dart';
import 'package:my_flutter_app/widgets/cstmtoast/toast_utils.dart';
import 'package:my_flutter_app/widgets/stepper_body.dart';
import 'package:my_flutter_app/widgets/default_button.dart';

class ProfilePage extends StatefulWidget {
  static const String route_name = '/profile';

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<ProfilePage> {
  final _formKey = GlobalKey<FormState>();
  String u_skills,u_cert, u_defination,u_accomplishment;
  int _currentStep = 0;
  bool complete = false;
  List<Step> steps;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          IconButton(icon: Icon(Icons.notifications,color: kPrimaryColor,), onPressed: () {}),
          IconButton(icon: Icon(Icons.settings,color: kPrimaryColor), onPressed: () { Navigator.pushNamed(context, PageRoutes.setting);}),
          IconButton(icon: Icon(FontAwesomeIcons.signOutAlt,color: kPrimaryColor), onPressed: ()
          {
            Members member =Members(m_id: CurrentMember.m_id,m_account:'',m_address:'', m_area:0,m_cdate:'',m_comment:'',
                m_country:0,m_email:'',m_fname:'',m_gender:'',m_img:'',m_lname:'',m_mobile:'');

            DatabaseProvider.db.deleteMember(member).then((value) => {
              setState(() {
if(value==1) {
  CurrentMember.m_id = 0;
  CurrentMember.m_account = '';
  CurrentMember.m_address = '';
  CurrentMember.m_area = '';
  CurrentMember.m_cdate = '';
  CurrentMember.m_comment = '';
  CurrentMember.m_country = '';
  CurrentMember.m_email = '';
  CurrentMember.m_fname = '';
  CurrentMember.m_gender = '';
  CurrentMember.m_img = '';
  CurrentMember.m_lname = '';
  CurrentMember.m_mobile = '';
}

              })
            });
            print('mem id ${member.m_id}');
            Navigator.pop(context);
            ToastUtils.showCustomToast(context, "تم سجيل الخروج");
          //  Navigator.pushReplacementNamed(context, PageRoutes.homepage);

          }),
        ],
        centerTitle: false,
        title: Text('البروفايل'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            SizedBox(height: 20,),
            _drawOfferHeadLine('احصل على عروض  وكن على استخدام دائم لتطبيقنا'),
            SizedBox(height: 10,),

            Card(
              child: _drawProfileinfo(),
              color: Colors.white,
            ),
            SizedBox(
              height: 20,
            ),
            _drawMoreservices(),

            _drawCustomTab(),
            SizedBox(
              height: 20,
            ),
            CurrentMember.m_utype ==2 ? _drawOfferHeadLine('أنت مسجل كـ فني ,يرجى تعبئة بياناتك ليطلع عليها الجميع '):Container(),
            CurrentMember.m_utype ==2 ? Padding(
              padding: const EdgeInsets.all(10.0),
              child: Container(child: DefaultButton(
                  text: "ادخل بياناتك",
                  press: () {
                    Navigator.pushNamed(context, PageRoutes.tech_info);
                  }
              ),width: 200,),
            ):Container(),

          ],
        ),
      ),
    );
  }
  Widget _drawOfferHeadLine(String title) {
    return Card(
      margin: EdgeInsets.only(top: 10),
      color: Colors.yellow.shade50,
      borderOnForeground: true,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Icon(
              Icons.chevron_left,
              color: Colors.deepOrange.shade200,
            ),
            Text(title,style: TextStyle(fontSize: 11),),
            Icon(
              Icons.local_offer,
              color: Colors.deepOrange.shade200,
            ),
          ],
        ),
      ),
    );
  }

  Widget _drawProfileinfo() {

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(16.0),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              ClipRRect(
                  child: FadeInImage(
                    width: 90,
                    height: 90,
                    fit: BoxFit.cover,
                    placeholder: AssetImage(
                        'assets/images/bg_second_board.png'),
                    image: AssetImage(
                        '${base_api + CurrentMember.m_img}'),
                  ),
                  borderRadius:
                  BorderRadius.all(
                      Radius.circular(50))),
              SizedBox(
                width: 10,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  CurrentMember.m_fname == '' ? Text(
                    'بي بي سوفت ',
                    style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.w500,
                    ),
                  ):Text(CurrentMember.m_fname+CurrentMember.m_lname),
                  SizedBox(
                    height: 5,
                  ),
                  CurrentMember.m_fname == '' ? Text(
                    '00967-011500123',
                    style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.w500,
                    ),
                  ):Text(CurrentMember.m_mobile),
                  Container(
                    padding: EdgeInsets.only(left: 7, right: 7),
                    decoration: BoxDecoration(
                      color: Colors.yellow.shade50,
                      borderRadius: BorderRadius.circular(8),
                    ),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.play_for_work,
                          color:Colors.deepOrange.shade200,
                        ),
                        Text('0 نقطة')
                      ],
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20, bottom: 10),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                children: <Widget>[
                  Text('15'),
                  Text('نقطة'),
                ],
              ),
              Column(
                children: <Widget>[
                  Icon(Icons.bubble_chart),
                  Text('محفظتي'),
                ],
              ),
              Column(
                children: <Widget>[
                  Text('عضو'),
                  Text(
                    'VIP',
                    style: TextStyle(color: kPrimaryColor),
                  ),
                ],
              ),
            ],
          ),
        )
      ],
    );
  }

  Widget _drawMoreservices() {
    return Card(
      color: Colors.white,
      child: Padding(
        padding: const EdgeInsets.only(left: 15, right: 15, bottom: 10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              'المزيد من الخدمات ',
              style: TextStyle(
                fontSize: 17,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Column(
                  children: <Widget>[
                    IconButton(
                        icon: Icon(Icons.monetization_on,
                            color: Theme.of(context).primaryColor),
                        onPressed: () {}),
                    Text('الرصيد'),
                  ],
                ),
                Column(
                  children: <Widget>[
                    IconButton(
                        icon: Icon(Icons.notification_important,
                            color: Theme.of(context).primaryColor),
                        onPressed: () {}),
                    Text('الإشعارات '),
                  ],
                ),
                Column(
                  children: <Widget>[
                    IconButton(
                        icon: Icon(Icons.settings,
                            color: Theme.of(context).primaryColor),
                        onPressed: () {}),
                    Text('اعدادات حسابي'),
                  ],
                ),
                Column(
                  children: <Widget>[
                    IconButton(
                        icon: Icon(Icons.wb_iridescent,
                            color: Theme.of(context).primaryColor),
                        onPressed: () {}),
                    Text('الدعم '),
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _drawCustomTab() {
    return Padding(
      padding: const EdgeInsets.only(top:15.0),
      child: Card(
        color: Colors.white,
        child: Padding(
          padding: const EdgeInsets.only(bottom:8.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              GestureDetector(
                  child: Row(
                    children: [
                      Icon(Icons.favorite,color: kPrimaryColor,),
                      Text(
                        'ذهاب إلى مفضلتي',
                        style: TextStyle(
                          fontSize: 15,
                        ),
                      ),
                    ],
                  ),
                  onTap: () {
                    Navigator.pushNamed(context, PageRoutes.favorite);
                  }),
              GestureDetector(
                child: Row(
                  children: [
                    Icon(Icons.shopping_cart,color: kPrimaryColor,),
                    Text(
                      'ذهاب إلى سلتي',
                      style: TextStyle(fontSize: 15),
                    ),
                  ],
                ),
                onTap: () {
                  {
                    Navigator.pushNamed(context, PageRoutes.shopcart);
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  List<Step>  _drawSteps() {
    List<Step> steps = [
    //first step /////////////////////////////////////
    Step(
      title: const Text('عرف بنفسك '),
      isActive: true,
      state: StepState.disabled,
      content: buildDefinationFormField(),
    ),

      // second step/////////////////////////////
      Step(
      title: const Text('مهاراتــك'),
      isActive: true,
      state: StepState.complete,
      //content: Form(key: _formKeys[0], child: …),
      content: buildSkillsFormField(),
      ),
      //third step //////////////////////////////////
      Step(
      title: const Text('شهاداتـــك التي حصلت عليها '),
      isActive: true,
      state: StepState.disabled,
      content: buildCertificationsFormField(),
      ),

      //step four ...........................
      Step(
      title: const Text('خبراتـــك'),
    isActive: true,
    state: StepState.disabled,
    content: buildExperienceFormField(),
    ),
    ];
    return steps;
 }


  Widget buildSkillsFormField() {
    return Container();
  }
  Widget buildCertificationsFormField() {
    return Container();
  }
  Widget buildDefinationFormField() {
    return Container();
  }
  Widget buildExperienceFormField() {
    return Container();
  }
  Widget buildAvaliableDaysFormField() {
    return Container();
  }

}


