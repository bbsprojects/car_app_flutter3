import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_flutter_app/database/database_provider.dart';
import 'package:my_flutter_app/model/currentmember.dart';
import 'package:my_flutter_app/pages/menu_pages/register_page.dart';
import 'package:my_flutter_app/utils/constants.dart';
import 'package:my_flutter_app/widgets/default_button.dart';
import 'package:my_flutter_app/widgets/form_error.dart';
import 'package:my_flutter_app/widgets/dialog.dart';
import 'package:my_flutter_app/widgets/cstmtoast/toast_utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:my_flutter_app/api/member_api.dart';

class LoginPage extends StatefulWidget {
  static const String route_name = '/loginPage';
  @override
  _SignFormState createState() => _SignFormState();
}

class _SignFormState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  String phone;
  String password;
  bool remember = false;
  final List<String> errors = [];

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      resizeToAvoidBottomPadding: true,
      appBar: AppBar(
        title: Text('تسجيل الدخول'),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 10),
                Text(
                  "اهلا بك",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 28,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  "يمكنك الدخول بتسجيل رقم موبايلك وكلمة السر الخاصة بك",
                  textAlign: TextAlign.center,
                ),
                SizedBox(height: 20),
                SignForm(),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "لا تملك حساب ؟ ",
                      style: TextStyle(fontSize: 16),
                    ),
                    GestureDetector(
                      onTap: () =>
                          Navigator.pushNamed(context, Register.route_name),
                      child: Text(
                        "تسجيل حساب",
                        style: TextStyle(fontSize: 16, color: kPrimaryColor),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget SignForm() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildPhoneFormField(),
          SizedBox(height: 20),
          buildPasswordFormField(),
          Row(
            children: [
              Checkbox(
                value: remember,
                activeColor: kPrimaryColor,
                onChanged: (value) {
                  setState(() {
                    remember = value;
                  });
                },
              ),
              Text("تذكرني"),
              Spacer(),
              GestureDetector(
                //  onTap: () => Navigator.pushNamed(context, ForgotPasswordScreen.routeName),
                child: Text(
                  "هل نسيت كلمة السر",
                  style: TextStyle(decoration: TextDecoration.underline),
                ),
              )
            ],
          ),
          FormError(errors: errors),
          SizedBox(height: 20),
          DefaultButton(
            text: "الدخول",
            press: () async {
              if (_formKey.currentState.validate()) {
                _formKey.currentState.save();
                if (_formKey.currentState.validate()) {
                  MemberApi memberApi = MemberApi();
                  _formKey.currentState.save();
                  Dialogs.showLoadingDialog(context, _keyLoader, "جاري الدخول");
                  var status = await memberApi.sendLoginInfo(phone, password);
                  SharedPreferences sharedPreferences =
                      await SharedPreferences.getInstance();
                  if (status == 1) {
                    isLogined();
                    ToastUtils.showCustomToast(context, "تم الدخول بنجاح");
                    sharedPreferences.setBool('isLogedIn', true);
                    Navigator.pop(context);
                    //    Navigator.popAndPushNamed(context, HomePage.route_name);
                  } else if (status == 0 || status == -9) {
                    print('response not 200');
                    sharedPreferences.setBool('isLogedIn', false);
                    addError(error: kAddressResponseError);
                    // _showToast(context);
                    // final snackBar = SnackBar(content: Text('هناك خطأ أثناء تسجيل الدخول يرجى إعادة المحاولة !!'));
                    // Find the Scaffold in the widget tree and use it to show a SnackBar.
                    // Scaffold.of(context).showSnackBar(snackBar);

                  }

                  Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                      .pop();
                }
              }
            },
          ),
        ],
      ),
    );
  }

  void isLogined() {
    DatabaseProvider.db.getMember().then((value) => {
          setState(() {
            if (value.m_id != null) {
              CurrentMember.m_id = value.m_id;
              CurrentMember.m_account = value.m_account;
              CurrentMember.m_address = value.m_address;
              CurrentMember.m_area = value.area_name;
              CurrentMember.m_cdate = value.m_cdate;
              CurrentMember.m_comment = value.m_comment;
              CurrentMember.m_country = value.c_name;
              CurrentMember.m_email = value.m_email;
              CurrentMember.m_fname = value.m_fname;
              CurrentMember.m_gender = value.m_gender;
              CurrentMember.m_img = value.m_img;
              CurrentMember.m_lname = value.m_lname;
              CurrentMember.m_mobile = value.m_mobile;
            }
          })
        });
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        } else if (value.length >= 8) {
          removeError(error: kShortPassError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if (value.length < 8) {
          addError(error: kShortPassError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "كلمة السر",
        hintText: "ادخل كلمة السر",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(Icons.lock_outline),
      ),
    );
  }

  TextFormField buildPhoneFormField() {
    return TextFormField(
      keyboardType: TextInputType.phone,
      onSaved: (newValue) => phone = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNumberNullError);
        } else if (value.length > 8) {
          removeError(error: kInvalidPhoneNumberError);
        }
        /*else if (emailValidatorRegExp.hasMatch(value)) {
          removeError(error: kInvalidEmailError);
        }*/
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPhoneNumberNullError);
          return "";
        } else if (value.length < 8) {
          addError(error: kInvalidPhoneNumberError);
          return "";
        }
        return null;
      },
      decoration: InputDecoration(
        labelText: "رقم الموبايل",
        hintText: "ادخل رقم موبايلك",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(Icons.phone_iphone),
      ),
    );
  }
}
