import 'package:cache_image/cache_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_flutter_app/api/products_api.dart';
import 'package:my_flutter_app/database/database_provider.dart';
import 'package:my_flutter_app/model/favorite.dart';
import 'package:my_flutter_app/pages/Details_pages/product_details.dart';
import 'package:my_flutter_app/model/brand.dart';
import 'package:my_flutter_app/model/products.dart';
import 'package:my_flutter_app/utils/constants.dart';
import 'package:my_flutter_app/widgets/no_internet.dart';
import 'package:my_flutter_app/widgets/shimmer.dart';

class ProductsView extends StatefulWidget {
  static const String route_name = '/carpage';

  Brand brand;
  ProductsView({this.brand});
  @override
  _ProductsViewState createState() => _ProductsViewState(brand: brand);
}

class _ProductsViewState extends State<ProductsView> {
  String title;
  Brand brand;
  _ProductsViewState({this.brand});

  ProductApi productApi = ProductApi();
  bool isLoading = true;
  List<Products> lstProduct = List<Products>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    print("isLoading $isLoading");
    if (brand == null) {
      print(brand.toString());
      print("all car ");
      title = "جميع السيارات ";
      get_Data();
    } else {
      print("toyota car ");
      setState(() {
        title = 'سيارات  ' + brand.b_title;
      });
      get_ModelData();
    }
    print("isLoading $isLoading");
  }

  get_Data() async {
    try {
      if (await isthereInternet()) {
        lstProduct = await productApi.fetchProducts(100);
      }
    } catch (Excaption) {}

    print('lenghttttt:${lstProduct.length}');

    setState(() => isLoading = false);
  }

  get_ModelData() async {
    lstProduct = await productApi.fetchbrandProducts(brand.b_id);
    print('lenghttttt:${lstProduct.length}');

    setState(() => isLoading = false);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('سيارات'),
      ),
      body: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Expanded(child: _drawListCars()),
          ],
        ),
      ),
    );
  }

  // this function to draw all news in the card
  Widget _drawListCars() {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 20, bottom: 20),
            child: _drawSectionTitle(title),
          ),
          Expanded(
            child: isLoading
                ? ShimmerList()
                : theris_internet
                    ? ListView.builder(
                        scrollDirection: Axis.vertical,
                        shrinkWrap: true,
                        itemBuilder: (context, index) {
                          return Column(
                            children: [
                              _drawSingleRowNewHyundai(index),
                              _drawDivider(),
                            ],
                          );
                        },
                        itemCount: lstProduct.length,
                      )
                    : Center(
                        child: NoInternetWiew(
                          press: () async {
                            get_Data();
                            setState(
                              () {
                                lstProduct = lstProduct;
                              },
                            );
                          },
                        ),
                      ),
          ),
          // the next section in product page for
        ],
      ),
    );
  }

  void _updateFavProd(int index) {
    if (lstProduct[index].fav == null) lstProduct[index].fav = false;
    DatabaseProvider.db
        .isExistinFavtProduct(lstProduct[index].p_id.toString())
        .then((value) => {
              if (value)
                setState(() => lstProduct[index].fav = true)
              else
                setState(() => lstProduct[index].fav = false)
            });
  }

  // function to draw the row in card for the news hyundai
  Widget _drawSingleRowNewHyundai(int index) {
    Products product = lstProduct[index];
    _updateFavProd(index);
    return InkWell(
      onTap: () {
        // Navigator.pushReplacementNamed(context, ProductDetails.route_name);
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return ProductDetails(
            p: product,
          );
        })).then((value) => _updateFavProd(index));
      },
      child: Padding(
        padding: const EdgeInsets.all(12.0),
        child: Row(
          children: <Widget>[
            SizedBox(
              child: ClipRRect(
                borderRadius: BorderRadius.circular(20),
                child: FadeInImage(
                  placeholder:
                      ExactAssetImage('assets/images/bg_second_board.png'),
                  image: CacheImage('${product.p_name}'),
                  fit: BoxFit.cover,
                ),
              ),
              width: 100,
              height: 100,
            ),
            SizedBox(
              width: 10,
            ),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    '${product.p_name}',
                    maxLines: 3,
                    style: TextStyle(
                      fontSize: 17,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('${product.brand}'),
                      Row(
                        children: <Widget>[
                          IconButton(
                              icon: Icon(
                                Icons.shopping_cart,
                                color: Theme.of(context).primaryColor,
                              ),
                              onPressed: () {}),
                          Text(
                            'شراء',
                            style: TextStyle(
                              color: Theme.of(context).primaryColor,
                            ),
                          )
                        ],
                      ),
                      IconButton(
                          icon: Icon(
                            (product.fav)
                                ? Icons.favorite
                                : Icons.favorite_border,
                            color: (product.fav)
                                ? kPrimaryColor
                                : Colors.grey[600],
                          ),
                          onPressed: () {
                            if (!product.fav) {
                              DatabaseProvider.db
                                  .insertFavorite(Favorite(
                                      f_date: DateTime.now().toString(),
                                      f_prd: product.p_id,
                                      f_prt: 0))
                                  .then((value) => {
                                        DatabaseProvider.db
                                            .insertProduct(product),
                                        setState(() {
                                          product.fav = !product.fav;
                                        })
                                      });
                            } else {
                              DatabaseProvider.db
                                  .deleteFavoritePoduct(product.p_id.toString())
                                  .then((value) => {
                                        DatabaseProvider.db
                                            .deleteProduct(product)
                                            .then((value) => {
                                                  setState(() {
                                                    product.fav = !product.fav;
                                                  })
                                                })
                                      });
                            }
                          }),
                    ],
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  // to divider between rows
  Widget _drawDivider() {
    return Container(
      height: 0.7,
      width: double.infinity,
      color: kSecondaryColor,
    );
  }

  // to set the name of sections in the products page
  // like 'جديد هونداي', 'الفنيين ' etc...
  _drawSectionTitle(String title) {
    return Text(
      title,
      style: styleSectionTitle,
    );
  }
}
