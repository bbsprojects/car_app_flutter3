import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_flutter_app/api/member_api.dart';
import 'package:my_flutter_app/model/area.dart';
import 'package:my_flutter_app/model/members.dart';
import 'package:my_flutter_app/utils/constants.dart';
import 'package:my_flutter_app/widgets/default_button.dart';
import 'package:my_flutter_app/widgets/form_error.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:my_flutter_app/widgets/dialog.dart';
import 'package:my_flutter_app/widgets/cstmtoast/toast_utils.dart';

class Register extends StatefulWidget {
  static const String route_name = '/register';
  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  String radioItem;

  City dropdownValueCity = cities[0];
  Area dropdownValueArea = areas_Alriad[0];
  List<City> lstcty = cities;
  List<Area> lstarea = areas_Alriad;

  final GlobalKey<State> _keyLoader = new GlobalKey<State>();
  final _formKey = GlobalKey<FormState>();

  String name;
  String phone;
  String email;
  String password;
  String conform_password;
  int m_type = 1;
  bool remember = false;
  final List<String> errors = [];

  void addError({String error}) {
    if (!errors.contains(error))
      setState(() {
        errors.add(error);
      });
  }

  void removeError({String error}) {
    if (errors.contains(error))
      setState(() {
        errors.remove(error);
      });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('تسجيل حساب'),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 20),
          child: SingleChildScrollView(
            child: Column(
              children: [
                SizedBox(height: 20), // 4%
                Text("تسجيل حساب", style: headingStyle),
                _SignupRadiobutton(),
                Text(
                  "الرجاء تسجيل بياناتك",
                  textAlign: TextAlign.center,
                ),

                SizedBox(height: 20),
                SignUpForm(),
                SizedBox(height: 20),
                Text(
                  'من خلال تسجيل الدخول انت موافق على شروط فريقنا',
                  textAlign: TextAlign.center,
                  style: Theme.of(context).textTheme.caption,
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _SignupRadiobutton() {
    return Column(
      children: <Widget>[
        RadioListTile(
          title: Text('كمستخدم '),
          groupValue: m_type,
          value: 1,
          activeColor: kPrimaryColor,
          onChanged: (val) {
            setState(() {
              m_type = val;
              print(m_type.toString());
            });
          },
        ),

        RadioListTile(
          activeColor: kPrimaryColor,
          groupValue: m_type,
          title: Text('كفني '),
          value: 2,
          onChanged: (val) {
            setState(() {
              m_type = val;
              print(m_type.toString());
            });
          },
        ),

        //   Text('$radioItem', style: TextStyle(fontSize: 23),)
      ],
    );
  }

  Widget SignUpForm() {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          buildNameFormField(),
          SizedBox(height: 20),
          buildPhoneFormField(),
          SizedBox(height: 20),
          buildCityDropdown(),
          SizedBox(height: 20),
          buildAreaDropdown(),
          SizedBox(height: 20),
          buildEmailFormField(),
          SizedBox(height: 20),
          buildPasswordFormField(),
          SizedBox(height: 20),
          buildConformPassFormField(),
          FormError(errors: errors),
          SizedBox(height: 30),
          DefaultButton(
            text: "تسجيل حساب",
            press: () async {
              if (_formKey.currentState.validate()) {
                MemberApi memberApi = MemberApi();
                _formKey.currentState.save();
                Dialogs.showLoadingDialog(context, _keyLoader, "جاري التسجيل");
                Members mem = Members(
                    //  m_id: 90,
                    m_lname: "",
                    m_fname: "",
                    m_gender: "",
                    m_status: 2,
                    m_account: name,
                    m_pwd: password,
                    m_mobile: phone,
                    m_email: email,
                    m_country: dropdownValueCity.c_id,
                    m_area: dropdownValueArea.area_id,
                    m_address: "",
                    m_utype: m_type,
                    m_cdate: DateTime.now().toString(),
                    m_comment: "");
                var status = await memberApi.sendRegestrationInfo(mem);
                if (status == 1) {
                  SharedPreferences sharedPreferences =
                      await SharedPreferences.getInstance();
                  sharedPreferences.setBool('isLogedIn', true);
                  ToastUtils.showCustomToast(context, "تم التسجيل بنجاح");
                  Navigator.pop(context);
                } else if (status == 0 || status == -9) {
                  addError(error: kTryRigesterAgain);
                }
                Navigator.of(_keyLoader.currentContext, rootNavigator: true)
                    .pop();
              }
            },
          ),
        ],
      ),
    );
  }

  TextFormField buildConformPassFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => conform_password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        } else if (value.isNotEmpty && password == conform_password) {
          removeError(error: kMatchPassError);
        }
        conform_password = value;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if ((password != value)) {
          addError(error: kMatchPassError);
          return "";
        }
        return null;
      },
      decoration: textInputDecoration.copyWith(
        //  labelText: "تأكيد كلمة السر",
        hintText: "ادخل كلمة السر مرة اخرى",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        // floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(Icons.lock),
      ),
    );
  }

  TextFormField buildPasswordFormField() {
    return TextFormField(
      obscureText: true,
      onSaved: (newValue) => password = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPassNullError);
        } else if (value.length >= 8) {
          removeError(error: kShortPassError);
        }
        password = value;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPassNullError);
          return "";
        } else if (value.length < 8) {
          addError(error: kShortPassError);
          return "";
        }
        return null;
      },
      decoration: textInputDecoration.copyWith(
        // labelText: "كلمة السر",
        hintText: "ادخل كلمة السر",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        // floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(Icons.lock_outline),
      ),
    );
  }

  TextFormField buildEmailFormField() {
    return TextFormField(
      keyboardType: TextInputType.emailAddress,
      onSaved: (newValue) => email = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kEmailNullError);
        } else if (emailValidatorRegExp.hasMatch(value)) {
          removeError(error: kInvalidEmailError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kEmailNullError);
          return "";
        } else if (!emailValidatorRegExp.hasMatch(value)) {
          addError(error: kInvalidEmailError);
          return "";
        }
        return null;
      },
      decoration: textInputDecoration.copyWith(
        // labelText: "البريد الإلكتروني",
        hintText: "ادخل البريد الإلكتروني",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        //floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(Icons.email),
      ),
    );
  }

  TextFormField buildPhoneFormField() {
    return TextFormField(
      keyboardType: TextInputType.phone,
      onSaved: (newValue) => phone = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNumberNullError);
        } else if (value.length > 8) {
          removeError(error: kInvalidPhoneNumberError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kPhoneNumberNullError);
          return "";
        } else if (value.length < 9) {
          addError(error: kInvalidPhoneNumberError);
          return "";
        }
        return null;
      },
      decoration: textInputDecoration.copyWith(
        // labelText: "رقم الموبايل",
        hintText: "ادخل رقم الموبايل",
        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        //  floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(Icons.phone),
      ),
    );
  }

  TextFormField buildNameFormField() {
    return TextFormField(
      keyboardType: TextInputType.text,
      onSaved: (newValue) => name = newValue,
      onChanged: (value) {
        if (value.isNotEmpty) {
          removeError(error: kPhoneNumberNullError);
        } else if (value.length > 8) {
          removeError(error: kInvalidPhoneNumberError);
        }
        return null;
      },
      validator: (value) {
        if (value.isEmpty) {
          addError(error: kNamelNullError);
          return "";
        } else if (value.length < 9) {
          addError(error: kInvalidNameError);
          return "";
        }
        return null;
      },
      decoration: textInputDecoration.copyWith(
        //labelText: "اسمك",
        hintText: "ادخل اسمك الثلاثي",

        // If  you are using latest version of flutter then lable text and hint text shown like this
        // if you r using flutter less then 1.20.* then maybe this is not working properly
        //  floatingLabelBehavior: FloatingLabelBehavior.always,
        suffixIcon: Icon(Icons.person),
      ),
    );
  }

  DropdownButton buildCityDropdown() {
    return DropdownButton<City>(
      isExpanded: true,
      value: dropdownValueCity,
      icon: Icon(Icons.home),
      iconSize: 24,
      elevation: 16,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (City newValue) {
        setState(() {
          dropdownValueCity = newValue;

          if (newValue.c_name == cities[0].c_name) {
            lstarea = areas_Alriad;
            dropdownValueArea = areas_Alriad[0];
          }

          if (newValue.c_name == cities[1].c_name) {
            lstarea = areas_Jedah;
            dropdownValueArea = areas_Jedah[0];
          }

          if (newValue.c_name == cities[2].c_name) {
            lstarea = areas_Abha;
            dropdownValueArea = areas_Abha[0];
          }

          if (newValue.c_name == cities[3].c_name) {
            lstarea = areas_Makah;
            dropdownValueArea = areas_Makah[0];
          }
          ////////////////////
        });
      },
      items: lstcty.map<DropdownMenuItem<City>>((City value) {
        return DropdownMenuItem<City>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value.c_name,
              ),
            ),
          ),
        );
      }).toList(),
    );
  }

  DropdownButton buildAreaDropdown() {
    return DropdownButton<Area>(
      isExpanded: true,
      value: dropdownValueArea,
      icon: Icon(Icons.place),
      iconSize: 24,
      elevation: 16,
      style: TextStyle(
        color: Colors.black,
      ),
      underline: Container(
        height: 1,
        color: Colors.grey,
      ),
      onChanged: (Area newValue) {
        setState(() {
          dropdownValueArea = newValue;
        });
      },
      items: lstarea.map<DropdownMenuItem<Area>>((Area value) {
        return DropdownMenuItem<Area>(
          value: value,
          child: Padding(
            padding: const EdgeInsets.only(right: 10),
            child: SizedBox(
              width: double.infinity, // for example
              child: Text(
                value.area_name,
              ),
            ),
          ),
        );
      }).toList(),
    );
  }
}
