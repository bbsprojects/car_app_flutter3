import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:my_flutter_app/api/parts_api.dart';
import 'package:my_flutter_app/database/database_provider.dart';
import 'package:my_flutter_app/model/parts.dart';
import 'package:my_flutter_app/pages/Details_pages/part_details.dart';
import 'package:my_flutter_app/utils/constants.dart';
import 'package:my_flutter_app/widgets/no_internet.dart';
import 'package:my_flutter_app/widgets/shimmer.dart';

class PartsView extends StatefulWidget {
  static const String route_name = '/parts';
  int part_type;
  PartsView({this.part_type});
  @override
  _PartsViewState createState() => _PartsViewState(type: part_type);
}

class _PartsViewState extends State<PartsView> {
  int type;
  _PartsViewState({this.type});

  String title;
  TextStyle _hashTagStyle = (TextStyle(color: Colors.deepOrange, fontSize: 20));
  PartsApi partsApi = PartsApi();
  bool isLoading = true;
  List<Parts> lstParts = List<Parts>();
  List<Parts> lstcontrol = List<Parts>();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    if (type == 1) {
      get_Data(1);
      title = 'مكائن وقطع متعلقة به';
    } else if (type == 2) {
      get_Data(2);
      title = 'البودي وقطع متعلقة به';
    } else if (type == 3) {
      get_Data(3);
      title = 'الإطارات وقطع متعلقة به';
    } else if (type == 4) {
      get_Data(4);
      title = 'الزيوت وقطع متعلقة به';
    } else if (type == 5) {
      get_Data(5);
      title = 'الإكسسوارات وقطع متعلقة بها';
    }
  }

  get_Data(int i) async {
    print('type no:${i}');
    try {
      if (await isthereInternet()) {
        lstParts = await partsApi.fetchAllParts(100);
        // set items to controll list
        if (lstParts.length >= 1) {
          lstcontrol = lstParts.where((list) => list.pr_type == i).toList();
        }
      }
    } catch (Excaption) {}

    print('lenght list of parts:${lstParts.length}');
    setState(() => isLoading = false);
  }

  void _updateFavPart(int index) {
    if (lstcontrol[index].fav == null) lstcontrol[index].fav = false;
    DatabaseProvider.db
        .isExistinFavtProduct(lstcontrol[index].pr_id.toString())
        .then((value) => {
              if (value)
                setState(() => lstcontrol[index].fav = true)
              else
                setState(() => lstcontrol[index].fav = false)
            });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //backgroundColor: kPrimaryLightColor,
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text(title),
      ),
      body: isLoading
          ? ShimmerList()
          : theris_internet
              ? ListView.builder(
                  itemCount: lstcontrol.length,
                  scrollDirection: Axis.vertical,
                  shrinkWrap: true,
                  padding:
                      EdgeInsets.only(right: 8, left: 8, top: 30, bottom: 10),
                  itemBuilder: (context, position) {
                    _updateFavPart(position);
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: Card(
                        child: InkWell(
                          onTap: () {
                            Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => PartDetails(
                                              part: lstcontrol[position],
                                            )))
                                .then((value) => _updateFavPart(position));
                          },
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              _cardHeader(position),
                              _cardFooter(position),
                            ],
                          ),
                        ),
                      ),
                    );
                  },
                )
              : Center(
                  child: NoInternetWiew(
                    press: () async {
                      get_Data(1);
                      setState(() {
                        lstcontrol = lstParts;
                      });
                    },
                  ),
                ),
    );
  }

  Widget _cardHeader(position) {
    bool _is_selected = false;
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Row(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: SizedBox(
                width: MediaQuery.of(context).size.height * 0.15,
                height: MediaQuery.of(context).size.height * 0.15,
                child: Image.network(
                  lstcontrol[position].pr_img,
                  fit: BoxFit.cover,
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Text(
                      lstcontrol[position].pr_name,
                      style: TextStyle(
                        color: Colors.grey.shade500,
                        fontSize: 17,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    lstcontrol[position].is_orginal == 0
                        ? Text(
                            'وكالة ',
                            style: TextStyle(color: Colors.blueAccent),
                          )
                        : Text(
                            'تقليد ',
                            style: TextStyle(color: Colors.blueAccent),
                          ),
                  ],
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  children: <Widget>[
                    Text(
                      lstcontrol[position].pr_mfr +
                          ' ' +
                          lstcontrol[position].pr_brand.toString() +
                          ' ' +
                          lstcontrol[position].pr_model.toString(),
                      style: TextStyle(color: Colors.grey),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Text(
                      'السعر ' + lstcontrol[position].pr_price.toString(),
                      style: TextStyle(color: Colors.blueAccent),
                    ),
                    lstcontrol[position].pr_currency == 0
                        ? Text('دولار')
                        : Text('ر.س'),
                  ],
                ),
              ],
            ),
          ],
        ),
        Row(
          children: <Widget>[
            Transform.translate(
              child: IconButton(
                icon: Icon(
                  (lstcontrol[position].fav)
                      ? Icons.favorite
                      : Icons.favorite_border,
                  color: (lstcontrol[position].fav)
                      ? kPrimaryColor
                      : Colors.grey[600],
                ),
                onPressed: () {
                  /*
                  if (!lstcontrol[position].fav) {
                    DatabaseProvider.db.insertFavorite(Favorite(
                        f_date: DateTime.now().toString(),
                        f_prd:
                        lstcontrol[position].pr_id,
                        f_prt: 0)).then((value) =>
                    {
                     // DatabaseProvider.db.insertParts(lstcontrol[position]),
                      setState(() {
                        lstcontrol[position].fav = !lstcontrol[position].fav;
                      })
                    });
                  }
                  else {
                    DatabaseProvider.db.deleteFavoritePoduct(
                        lstcontrol[position].pr_id.toString())
                        .then((value) => {
                      DatabaseProvider.db.deleteParts(lstcontrol[position]).then((value) =>
                      {
                        setState(
                                () {
                              lstcontrol[position].fav = !lstcontrol[position].fav;
                            })
                      })
                    });
                  }*/
                },
                //color:_is_selected ?true? Colors.grey.shade500,
                color: Colors.grey.shade500,
              ),
              offset: Offset(-5, 0),
            ),
          ],
        ),
      ],
    );
  }

  Widget _cardFooter(position) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: <Widget>[
        Row(
          children: <Widget>[
            //Text('الكميـة :'),
            FlatButton(
              onPressed: () {},
              child: Text(
                '+',
                style: _hashTagStyle,
              ),
            ),
            Text('1'),
            FlatButton(
              onPressed: () {},
              child: Text(
                '-',
                style: _hashTagStyle,
              ),
            ),
          ],
        ),
        InkWell(
          child: Row(
            children: [
              IconButton(
                onPressed: () {},
                icon: Icon(
                  Icons.shopping_cart,
                  color: kPrimaryColor,
                ),
              ),
              Text(
                'اضف إلى العربة',
                style: _hashTagStyle,
              )
            ],
          ),
          onTap: () {},
        ),
      ],
    );
  }
}
