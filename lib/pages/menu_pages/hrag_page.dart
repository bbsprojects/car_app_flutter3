import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:my_flutter_app/api/parts_api.dart';
import 'package:my_flutter_app/api/products_api.dart';
import 'package:my_flutter_app/database/database_provider.dart';
import 'package:my_flutter_app/model/favorite.dart';
import 'package:my_flutter_app/pages/Details_pages/hragpart_details.dart';
import 'package:my_flutter_app/model/parts.dart';
import 'package:my_flutter_app/model/products.dart';
import 'package:my_flutter_app/utils/api_path.dart';
import 'package:my_flutter_app/utils/constants.dart';
import 'package:my_flutter_app/widgets/no_internet.dart';
import 'package:my_flutter_app/widgets/shimmer.dart';
import 'package:intl/intl.dart' as intl;
import 'package:timeago/timeago.dart' as timeago;

class HragPage extends StatefulWidget {
  static const String route_name = '/hragPage';
  int _selectedtype;
  HragPage(this._selectedtype);
  @override
  _HragPageState createState() => _HragPageState(_selectedtype);
}

class _HragPageState extends State<HragPage> {
  int _selectedtype;
  _HragPageState(this._selectedtype);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        title: Text('حراج'),
      ),
      body: Container(
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    setState(() {
                      _selectedtype = 0;
                    });
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    child: Text(
                      'سيارات',
                      style: TextStyle(
                          color:
                              _selectedtype == 0 ? Colors.white : kTextColor),
                    ),
                    decoration: BoxDecoration(
                        color:
                            _selectedtype == 0 ? kPrimaryColor : Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    setState(() {
                      _selectedtype = 1;
                    });
                  },
                  child: Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20),
                    child: Text(
                      'قطع غيار',
                      style: TextStyle(
                          color:
                              _selectedtype == 1 ? Colors.white : kTextColor),
                    ),
                    decoration: BoxDecoration(
                        color:
                            _selectedtype == 1 ? kPrimaryColor : Colors.white,
                        borderRadius: BorderRadius.circular(10)),
                  ),
                ),
              ],
            ),
            (_selectedtype == 0) ? HargCarsDetails() : HargPratsDetails(),
            // (_selectedtype == 0) ? HargPratsDetails() : HargPratsDetails(),
          ],
        ),
      ),
    );
  }
}

class HargCarsDetails extends StatefulWidget {
  @override
  _HargCarsDetailsState createState() => _HargCarsDetailsState();
}

class _HargCarsDetailsState extends State<HargCarsDetails> {
  TextStyle _Author_name =
      TextStyle(fontWeight: FontWeight.bold, color: kTextColor);
  TextStyle _Author_desc =
      TextStyle(color: Colors.blue, fontWeight: FontWeight.w600, fontSize: 14);
  List<Products> lstProducts = List<Products>();
  ProductApi productApi = ProductApi();
  bool isLoading = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    getData();
  }

  Future<void> getData() async {
    try {
      if (await isthereInternet()) {
        lstProducts = await productApi.fetchHragProducts(100);
      }
    } catch (Excaption) {}

    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Expanded(
        child: isLoading
            ? ShimmerList()
            : theris_internet
                ? RefreshIndicator(
                    onRefresh: getData,
                    child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        itemCount: lstProducts.length,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, int index) {
                          if (lstProducts[index].fav == null)
                            lstProducts[index].fav = false;
                          DatabaseProvider.db
                              .isExistinFavtProduct(
                                  lstProducts[index].p_id.toString())
                              .then((value) => {
                                    if (value)
                                      setState(
                                          () => lstProducts[index].fav = true)
                                  });
                          return Padding(
                            padding: const EdgeInsets.only(
                                left: 20, right: 20, top: 10),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Container(
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                          ),
                                          height: 45,
                                          width: 45,
                                          margin: EdgeInsets.only(
                                              left: 8.0, right: 5),
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(25)),
                                            child: FadeInImage(
                                              fit: BoxFit.cover,
                                              placeholder: AssetImage(
                                                  'assets/images/unknow.jpg'),
                                              image: AssetImage(
                                                  '${base_api + lstProducts[index].m_img}'),
                                            ),
                                          ),
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              '${lstProducts[index].name}',
                                              style: _Author_name,
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Row(
                                              children: <Widget>[
                                                Text(
                                                  '${timeago.format(DateTime.parse(lstProducts[index].p_cdate), locale: 'en_short')}',
                                                  style: TextStyle(
                                                      color:
                                                          Colors.grey.shade500,
                                                      fontSize: 12),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                GestureDetector(
                                                  child: Text(
                                                    '${lstProducts[index].brand}',
                                                    style: _Author_desc,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        GestureDetector(
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Icon(
                                                Icons.place,
                                                color: Colors.black54,
                                              ),
                                              Padding(
                                                padding:
                                                    const EdgeInsets.all(0),
                                                child: Text(
                                                  'الرياض',
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      color: Colors.grey[600]),
                                                ),
                                              )
                                            ],
                                          ),
                                          onTap: () {},
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                InkWell(
                                  onTap: () async {
                                    ProductApi aa = ProductApi();
                                    await aa
                                        .sendProductInfo(lstProducts[index]);
                                  },
                                  child: Container(
                                    margin: const EdgeInsets.only(top: 10),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Container(
                                                  width: 100,
                                                  height: 100,
                                                  child: ClipRRect(
                                                      child: FadeInImage(
                                                        width: 50,
                                                        height: 50,
                                                        fit: BoxFit.cover,
                                                        placeholder: AssetImage(
                                                            'assets/images/bg_second_board.png'),
                                                        image: AssetImage(
                                                            'assets/images/bg_second_board.png'),
                                                      ),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  10))),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 10),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: <Widget>[
                                                      Text(
                                                        '${lstProducts[index].p_name}',
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      Text(
                                                          '${lstProducts[index].p_detailes}',
                                                          textAlign:
                                                              TextAlign.start),
                                                      Row(
                                                        children: [
                                                          Text(
                                                              '${lstProducts[index].model}'),
                                                          SizedBox(
                                                            width: 5,
                                                          ),
                                                          Text('.'),
                                                          SizedBox(
                                                            width: 5,
                                                          ),
                                                          Text(
                                                              '${lstProducts[index].fuel}'),
                                                          Text('.'),
                                                          SizedBox(
                                                            width: 5,
                                                          ),
                                                          Text(
                                                              '${lstProducts[index].p_mileage}'),
                                                        ],
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            IconButton(
                                                icon: Icon(
                                                  (lstProducts[index].fav)
                                                      ? Icons.favorite
                                                      : Icons.favorite_border,
                                                  color:
                                                      (lstProducts[index].fav)
                                                          ? kPrimaryColor
                                                          : Colors.grey[600],
                                                ),
                                                onPressed: () {
                                                  /*    if (!lstProducts[index].fav) {
                                                    DatabaseProvider.db
                                                        .insertFavorite(Favorite(
                                                            f_date:
                                                                DateTime.now()
                                                                    .toString(),
                                                            f_prd: lstProducts[
                                                                    index]
                                                                .p_id,
                                                            f_prt: 0))
                                                        .then((value) => {
                                                              DatabaseProvider
                                                                  .db
                                                                  .insertProduct(
                                                                      lstProducts[
                                                                          index]),
                                                              setState(() {
                                                                lstProducts[index]
                                                                        .fav =
                                                                    !lstProducts[
                                                                            index]
                                                                        .fav;
                                                              })
                                                            });
                                                  } else {
                                                    DatabaseProvider.db
                                                        .deleteFavoritePoduct(
                                                            lstProducts[index]
                                                                .p_id
                                                                .toString())
                                                        .then((value) => {
                                                              DatabaseProvider
                                                                  .db
                                                                  .deleteProduct(
                                                                      lstProducts[
                                                                          index])
                                                                  .then(
                                                                      (value) =>
                                                                          {
                                                                            setState(() {
                                                                              lstProducts[index].fav = !lstProducts[index].fav;
                                                                            })
                                                                          })
                                                            });
                                                  }*/
                                                })
                                          ],
                                        ),
                                        Container(
                                          width: double.infinity,
                                          height: 50,
                                          decoration: BoxDecoration(
                                              color: Colors.grey[100],
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Padding(
                                                padding: const EdgeInsets.only(
                                                    right: 10),
                                                child: Text(
                                                  '${lstProducts[index].p_price}  ${lstProducts[index].currency}',
                                                  style: TextStyle(
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: kTextColor),
                                                ),
                                              ),
                                              IconButton(
                                                icon: Icon(
                                                  Icons.comment,
                                                  color: Colors.grey[600],
                                                ),
                                                onPressed: () {},
                                              )
                                            ],
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        }),
                  )
                : Center(
                    child: NoInternetWiew(
                      press: () async {
                        getData();
                        setState(() {
                          lstProducts = lstProducts;
                        });
                      },
                    ),
                  ),
      ),
    );
  }
}

class HargPratsDetails extends StatefulWidget {
  @override
  _HargPratsDetailsState createState() => _HargPratsDetailsState();
}

class _HargPratsDetailsState extends State<HargPratsDetails> {
  TextStyle _Author_name =
      TextStyle(fontWeight: FontWeight.bold, color: kTextColor);
  TextStyle _Author_desc =
      TextStyle(color: Colors.blue, fontWeight: FontWeight.w600, fontSize: 14);
  List<Parts> lstParts = List<Parts>();
  PartsApi partsApi = PartsApi();
  bool isLoading = true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getData();
  }

  Future<void> getData() async {
    try {
      if (await isthereInternet()) {
        lstParts = await partsApi.fetchHaragParts(100);
      }
    } catch (Excaption) {}
    setState(() {
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Expanded(
        child: isLoading
            ? ShimmerList()
            : theris_internet
                ? RefreshIndicator(
                    onRefresh: getData,
                    child: ListView.builder(
                        scrollDirection: Axis.vertical,
                        itemCount: lstParts.length,
                        shrinkWrap: true,
                        itemBuilder: (BuildContext context, int index) {
                          if (lstParts[index].fav == null)
                            lstParts[index].fav = false;
                          DatabaseProvider.db
                              .isExistinFavtPart(
                                  lstParts[index].pr_id.toString())
                              .then((value) => {
                                    if (value)
                                      setState(() => lstParts[index].fav = true)
                                  });

                          return Padding(
                            padding: const EdgeInsets.only(
                                left: 20, right: 20, top: 10),
                            child: Column(
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Row(
                                      children: <Widget>[
                                        Container(
                                          decoration: BoxDecoration(
                                            shape: BoxShape.circle,
                                          ),
                                          height: 45,
                                          width: 45,
                                          margin: EdgeInsets.only(
                                              left: 8.0, right: 5),
                                          child: ClipRRect(
                                            borderRadius: BorderRadius.all(
                                                Radius.circular(25)),
                                            child: FadeInImage(
                                              fit: BoxFit.cover,
                                              placeholder: AssetImage(
                                                  'assets/images/unknow.jpg'),
                                              image: AssetImage(
                                                  '${base_api + lstParts[index].m_img}'),
                                            ),
                                          ),
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Text(
                                              '${lstParts[index].name}',
                                              style: _Author_name,
                                            ),
                                            SizedBox(
                                              height: 5,
                                            ),
                                            Row(
                                              children: <Widget>[
                                                Text(
                                                  '${timeago.format(DateTime.parse(lstParts[index].pr_cdate), locale: 'en_short')}',
                                                  style: TextStyle(
                                                      color:
                                                          Colors.grey.shade500,
                                                      fontSize: 12),
                                                ),
                                                SizedBox(
                                                  width: 5,
                                                ),
                                                GestureDetector(
                                                  child: lstParts[index]
                                                              .is_orginal ==
                                                          0
                                                      ? Text(
                                                          'تقليد',
                                                          style: _Author_desc,
                                                        )
                                                      : Text(
                                                          'اصلي',
                                                          style: _Author_desc,
                                                        ),
                                                ),
                                              ],
                                            ),
                                          ],
                                        ),
                                      ],
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.start,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        GestureDetector(
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.start,
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              Icon(
                                                Icons.place,
                                                color: Colors.black54,
                                              ),
                                              Padding(
                                                padding:
                                                    const EdgeInsets.all(0),
                                                child: Text(
                                                  'الرياض',
                                                  style: TextStyle(
                                                      fontSize: 12,
                                                      color: Colors.grey[600]),
                                                ),
                                              )
                                            ],
                                          ),
                                          onTap: () {},
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                                InkWell(
                                  onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) =>
                                                HragPartDetails(
                                                  part: lstParts[index],
                                                )));
                                  },
                                  child: Container(
                                    margin: const EdgeInsets.only(top: 10),
                                    child: Column(
                                      children: <Widget>[
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Row(
                                              children: <Widget>[
                                                Container(
                                                  width: 100,
                                                  height: 100,
                                                  child: ClipRRect(
                                                      child: FadeInImage(
                                                        width: 50,
                                                        height: 50,
                                                        fit: BoxFit.cover,
                                                        placeholder: AssetImage(
                                                            'assets/images/bg_second_board.png'),
                                                        image: AssetImage(
                                                            'assets/images/bg_second_board.png'),
                                                      ),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  10))),
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.only(
                                                          right: 10),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: <Widget>[
                                                      Text(
                                                        lstParts[index].pr_name,
                                                        textAlign:
                                                            TextAlign.start,
                                                        style: TextStyle(
                                                            fontWeight:
                                                                FontWeight
                                                                    .bold),
                                                      ),
                                                      Text(
                                                          lstParts[index].brand,
                                                          textAlign:
                                                              TextAlign.start),
                                                      /*  Container(
                                                      height: 50,
                                                      width: 150,
                                                      decoration: BoxDecoration(
                                                          color: Colors.grey[100],
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(10)),
                                                      child: Row(
                                                        mainAxisAlignment:
                                                            MainAxisAlignment
                                                                .spaceAround,
                                                        children: [
                                                          IconButton(
                                                            icon: Icon(
                                                              Icons.add_circle,
                                                              color: Colors
                                                                  .grey[600],
                                                            ),
                                                            onPressed: () {
                                                              setState(() {
                                                                if (lstParts[
                                                                            index]
                                                                        .qty ==
                                                                    null) {
                                                                  lstParts[index]
                                                                      .qty = 0;
                                                                }
                                                                lstParts[index]
                                                                    .qty++;
                                                              });
                                                            },
                                                          ),
                                                          Text(
                                                              '${lstParts[index].qty ?? 0}'),
                                                          IconButton(
                                                            icon: Icon(
                                                              Icons
                                                                  .do_not_disturb_on,
                                                              color: Colors
                                                                  .grey[600],
                                                            ),
                                                            onPressed: () {
                                                              if (lstParts[index]
                                                                      .qty >
                                                                  0) {
                                                                setState(() {
                                                                  lstParts[index]
                                                                      .qty--;
                                                                });
                                                              }
                                                            },
                                                          ),
                                                        ],
                                                      ),
                                                    ),*/
                                                    ],
                                                  ),
                                                ),
                                              ],
                                            ),
                                            Column(
                                              children: [
                                                IconButton(
                                                    icon: Icon(
                                                      lstParts[index].fav
                                                          ? Icons.favorite
                                                          : Icons
                                                              .favorite_border,
                                                      color: lstParts[index].fav
                                                          ? kPrimaryColor
                                                          : Colors.grey[600],
                                                    ),
                                                    onPressed: () {
                                                      if (!lstParts[index]
                                                          .fav) {
                                                        DatabaseProvider.db
                                                            .insertFavorite(Favorite(
                                                                f_date: DateTime
                                                                        .now()
                                                                    .toString(),
                                                                f_prt: lstParts[
                                                                        index]
                                                                    .pr_id,
                                                                f_prd: 0))
                                                            .then((value) => {
                                                                  DatabaseProvider
                                                                      .db
                                                                      .insertParts(
                                                                          lstParts[
                                                                              index]),
                                                                  setState(() {
                                                                    lstParts[
                                                                            index]
                                                                        .fav = !lstParts[
                                                                            index]
                                                                        .fav;
                                                                  })
                                                                });
                                                      } else {
                                                        DatabaseProvider.db
                                                            .deleteFavoritePart(
                                                                lstParts[index]
                                                                    .pr_id
                                                                    .toString())
                                                            .then((value) => {
                                                                  DatabaseProvider
                                                                      .db
                                                                      .deleteParts(
                                                                          lstParts[
                                                                              index])
                                                                      .then(
                                                                          (value) =>
                                                                              {
                                                                                setState(() {
                                                                                  lstParts[index].fav = !lstParts[index].fav;
                                                                                })
                                                                              })
                                                                });
                                                      }
                                                    }),
                                                SizedBox(
                                                  height: 15,
                                                ),
                                                Text(
                                                    '${lstParts[index].pr_price}  ${lstParts[index].currency}',
                                                    style: TextStyle(
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        color: kTextColor)),
                                              ],
                                            ),
                                          ],
                                        ),
                                        SizedBox(
                                          height: 10,
                                        ),
                                        Container(
                                          width: double.infinity,
                                          height: 1,
                                          decoration: BoxDecoration(
                                              color: Colors.grey[100],
                                              borderRadius:
                                                  BorderRadius.circular(10)),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        }),
                  )
                : Center(
                    child: NoInternetWiew(
                      press: () {
                        getData();
                        setState(() {
                          lstParts = lstParts;
                        });
                      },
                    ),
                  ),
      ),
    );
  }
}
