class Notification {
  int n_id;
  int n_cat;
  String nc_type;
  String n_desc;
  String n_content;
  int n_createdBy;
  String n_cdate;
  int is_seen;

  Notification(
      {this.n_id,
      this.n_cat,
      this.nc_type,
      this.n_cdate,
      this.n_desc,
      this.n_content,
      this.n_createdBy,
      this.is_seen});

  factory Notification.fromMap(Map<String, dynamic> json) => new Notification(
        n_id: int.tryParse(json['n_id'].toString().trim()),
        n_cat: int.tryParse(json['n_cat'].toString().trim()),
        nc_type: json['nc_type'] ?? '',
        n_createdBy: int.tryParse(json['n_createdBy'].toString().trim()),
        is_seen: int.tryParse(json['is_seen'].toString().trim()),
        n_desc: json['n_desc'] ?? '',
        n_content: json['n_content'] ?? '',
        n_cdate: json['n_cdate'] ?? '',
      );

  Map<String, dynamic> toMap() => {
        'n_id': n_id,
        'n_cat': n_cat,
        'nc_type': nc_type,
        'n_desc': n_desc,
        'n_content': n_content,
        'n_createdBy': n_createdBy,
        'n_cdate': n_cdate,
        'is_seen': is_seen,
      };
}
