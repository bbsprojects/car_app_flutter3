class Brand {
  int b_id, b_sort;
  String b_title, b_def, b_img, b_type;

  Brand(
      {this.b_id,
      this.b_def,
      this.b_img,
      this.b_sort,
      this.b_title,
      this.b_type});

  factory Brand.fromMap(Map<String, dynamic> json) => new Brand(
        b_id: int.tryParse(json['b_id'].toString().trim()),
        b_sort: int.tryParse(json['b_sort'].toString().trim()),
        b_title: json['b_title'] ?? '',
        b_def: json['b_def'] ?? '',
        b_img: json['b_img'] ?? '',
        b_type: json['b_type'] ?? '',
      );

  Map<String, dynamic> toMap() => {
        'b_id': b_id,
        'b_sort': b_sort,
        'b_title': b_title,
        'b_def': b_def,
        'b_img': b_img,
        'b_type': b_type,
      };
}
