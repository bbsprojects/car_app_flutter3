class City {
  int c_id;
  String c_name;
  String c_latLang;
  City({this.c_id, this.c_name, this.c_latLang});

  factory City.fromMap(Map<String, dynamic> json) => new City(
        c_id: int.tryParse(json['moc_id_id'].toString().trim()),
        c_name: json['c_name'] ?? '',
        c_latLang: json['c_latLang'] ?? '',
      );

  Map<String, dynamic> toMap() => {
        'c_id': c_id,
        'c_name': c_name,
        'c_latLang': c_latLang,
      };
}

class Area {
  int area_id;
  String area_name;
  String area_latLang;
  int country;

  Area({this.area_id, this.area_name, this.area_latLang, this.country});

  factory Area.fromMap(Map<String, dynamic> json) => new Area(
        area_id: int.tryParse(json['area_id'].toString().trim()),
        area_name: json['area_name'] ?? '',
        area_latLang: json['area_latLang'] ?? '',
        country: int.tryParse(json['country'].toString().trim()),
      );

  Map<String, dynamic> toMap() => {
        'area_id': area_id,
        'area_name': area_name,
        'c_latLang': area_latLang,
        'country': country,
      };
}
