class ProductType {
  int pt_id;
  String pt_type;
  String pt_desc;
  String pt_comment;

  ProductType({this.pt_id, this.pt_type, this.pt_desc, this.pt_comment});
  factory ProductType.fromMap(Map<String, dynamic> json) => new ProductType(
        pt_id: int.tryParse(json['pt_id'].toString().trim()),
        pt_type: json['pt_type'] ?? '',
        pt_desc: json['pt_desc'] ?? '',
        pt_comment: json['pt_comment'] ?? '',
      );

  Map<String, dynamic> toMap() => {
        'pt_id': pt_id,
        'pt_type': pt_type,
        'pt_desc': pt_desc,
        'pt_comment': pt_comment,
      };
}
