class Module {
  int mo_id, mo_brand;
  String mo_title;

  Module({this.mo_id, this.mo_title, this.mo_brand});

  factory Module.fromMap(Map<String, dynamic> json) => new Module(
        mo_id: int.tryParse(json['mo_id'].toString().trim()),
        mo_title: json['mo_title'] ?? '',
        mo_brand: int.tryParse(json['mo_brand'].toString().trim()),
      );

  Map<String, dynamic> toMap() => {
        'mo_id': mo_id,
        'mo_title': mo_title,
        'mo_brand': mo_brand,
      };
}
