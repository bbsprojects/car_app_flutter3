class ShoppingCart {
  int s_id;
  int prd_id;
  int part_id;
  int s_qty;
  String s_date;

  ShoppingCart({this.s_id, this.part_id, this.prd_id, this.s_qty, this.s_date});
  factory ShoppingCart.fromMap(Map<String, dynamic> json) => new ShoppingCart(
        s_id: int.tryParse(json['s_id'].toString().trim()),
        prd_id: int.tryParse(json['prd_id'].toString().trim()),
        part_id: int.tryParse(json['part_id'].toString().trim()),
        s_qty: int.tryParse(json['s_qty'].toString().trim()),
        s_date: json['s_date'] ?? '',
      );

  Map<String, dynamic> toMap() => {
        's_id': s_id,
        'prd_id': prd_id,
        'part_id': part_id,
        's_qty': s_qty,
        's_date': s_date,
      };
}
