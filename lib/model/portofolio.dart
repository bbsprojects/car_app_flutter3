class Portfolio {

  String u_skills, u_cert, u_defination, u_accomplishment,u_available_day,u_category;

  Portfolio(
      {this.u_skills,
        this.u_cert,
        this.u_defination,
        this.u_accomplishment,
        this.u_available_day,
        this.u_category,
        });

  factory Portfolio.fromMap(Map<String, dynamic> json) => new Portfolio(

    u_skills: json['u_skills'] ?? '',
    u_cert: json['u_cert'] ?? '',
    u_defination: json['u_defination'] ?? '',
    u_accomplishment: json['u_accomplishment'] ?? '',
    u_available_day: json['u_available_day'] ?? '',
    u_category: json['u_category'] ?? '',

  );

  Map<String, dynamic> toMap() => {
    'u_skills': u_skills,
    'u_cert': u_cert,
    'u_defination': u_defination,
    'u_accomplishment': u_accomplishment,
    'u_available_day': u_available_day,
    'u_category': u_category,

  };
}
