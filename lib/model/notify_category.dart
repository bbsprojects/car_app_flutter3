class NotifyCategory {
  int nc_id;
  String nc_type;
  String nc_desc;

  NotifyCategory({this.nc_id, this.nc_type, this.nc_desc});

  factory NotifyCategory.fromMap(Map<String, dynamic> json) =>
      new NotifyCategory(
        nc_id: int.tryParse(json['n_id'].toString().trim()),
        nc_type: json['nc_type'] ?? '',
        nc_desc: json['nc_desc'] ?? '',
      );

  Map<String, dynamic> toMap() => {
        'nc_id': nc_id,
        'nc_type': nc_type,
        'nc_desc': nc_desc,
      };
}
