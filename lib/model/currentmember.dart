class CurrentMember {
  static int m_id = 0;
  static int m_status = 0;
  static String m_country = "";
  static String m_area = "";
  static String m_utype = "";

  static String m_fname = "";
  static String m_lname = "";
  static String m_gender = "";
  static String m_account = "";
  static String m_pwd = "";
  static String m_mobile = "";
  static String m_email = "";
  static String m_address = "";
  static String m_cdate = "";
  static String m_comment = "";
  static String m_img = "";
  static String token = "";
}
