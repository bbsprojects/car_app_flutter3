class Parts {
  int pr_id,
      is_sold,
      pr_model,
      is_new,
      pr_brand,
      pr_currency,
      pr_price,
      is_available,
      is_haraj,
      pr_createdBy,
      pr_type,
      is_orginal,
      qty;
  bool fav;

  String pr_name,
      vin_no,
      pr_mfr,
      pr_detailes,
      pr_comment,
      pr_cdate,
      pr_img,
      pr_tag,
      pr_year,
      m_img,
      name,
      model,
      currency,
      brand;

  Parts(
      {this.pr_id,
      this.is_sold,
      this.pr_model,
      this.is_new,
      this.pr_brand,
      this.pr_currency,
      this.pr_price,
      this.is_available,
      this.is_haraj,
      this.pr_createdBy,
      this.pr_type,
      this.is_orginal,
      this.pr_name,
      this.vin_no,
      this.pr_mfr,
      this.pr_detailes,
      this.pr_comment,
      this.pr_cdate,
      this.pr_img,
      this.pr_tag,
      this.pr_year,
      this.m_img,
      this.name,
      this.model,
      this.currency,
      this.brand,
      this.qty,
      this.fav});

  factory Parts.fromMap(Map<String, dynamic> json) => new Parts(
        pr_id: int.tryParse(json['pr_id'].toString().trim()),
        is_sold: int.tryParse(json['is_sold'].toString().trim()),
        pr_model: int.tryParse(json['pr_model'].toString().trim()),
        is_new: int.tryParse(json['is_new'].toString().trim()),
        pr_brand: int.tryParse(json['pr_brand'].toString().trim()),
        pr_currency: int.tryParse(json['pr_currency'].toString().trim()),
        pr_price: int.tryParse(json['pr_price'].toString().trim()),
        is_available: int.tryParse(json['is_available'].toString().trim()),
        is_haraj: int.tryParse(json['is_haraj'].toString().trim()),
        pr_createdBy: int.tryParse(json['pr_createdBy'].toString().trim()),
        pr_type: int.tryParse(json['pr_type'].toString().trim()),
        is_orginal: int.tryParse(json['is_orginal'].toString().trim()),
        pr_name: json['pr_name'] ?? '',
        vin_no: json['vin_no'] ?? '',
        pr_mfr: json['pr_mfr'] ?? '',
        pr_detailes: json['pr_detailes'] ?? '',
        pr_comment: json['pr_comment'] ?? '',
        pr_cdate: json['pr_cdate'] ?? '',
        pr_img: json['pr_img'] ?? '',
        pr_tag: json['pr_tag'] ?? '',
        pr_year: json['pr_year'] ?? '',
        m_img: json['m_img'] ?? '',
        name: json['name'] ?? '',
        model: json['model'] ?? '',
        currency: json['currency'] ?? '',
        brand: json['brand'] ?? '',
      );

  Map<String, dynamic> toMap() => {
        'pr_id': pr_id,
        'is_sold': is_sold,
        'pr_model': pr_model,
        'is_new': is_new,
        'pr_brand': pr_brand,
        'pr_currency': pr_currency,
        'pr_price': pr_price,
        'is_available': is_available,
        'is_haraj': is_haraj,
        'pr_createdBy': pr_createdBy,
        'pr_type': pr_type,
        'is_orginal': is_orginal,
        'pr_name': pr_name,
        'vin_no': vin_no,
        'pr_mfr': pr_mfr,
        'pr_detailes': pr_detailes,
        'pr_comment': pr_comment,
        'pr_cdate': pr_cdate,
        'pr_img': pr_img,
        'pr_tag': pr_tag,
        'pr_year': pr_year,
        'm_img': m_img,
        'name': name,
        'model': model,
        'currency': currency,
        'brand': brand,
      };
}
