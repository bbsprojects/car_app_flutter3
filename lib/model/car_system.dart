class CarTrans {
//trns
  int tr_id;
  String tr_trype;
  CarTrans({this.tr_id, this.tr_trype});
}

class CarFuell {
//fuel_type
  int f_id;
  String f_type;
  CarFuell({this.f_id, this.f_type});

  factory CarFuell.fromMap(Map<String, dynamic> json) => new CarFuell(
        f_id: int.tryParse(json['mo_id'].toString().trim()),
        f_type: json['f_type'] ?? '',
      );

  Map<String, dynamic> toMap() => {
        'f_id': f_id,
        'f_type': f_type,
      };
}
