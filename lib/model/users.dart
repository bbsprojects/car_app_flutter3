class Users {
  int u_id, u_status, u_country, u_area, u_address, u_utype;

  String u_fname,
      u_lname,
      u_gender,
      u_account,
      u_pwd,
      u_mobile,
      u_email,
      u_cdate,
      u_comment;
  // data for view
  String u_area_lebel, u_address_lebel, u_country_lebel, u_utype_lebel;

  Users(
      {this.u_area_lebel,
      this.u_address_lebel,
      this.u_country_lebel,
      this.u_utype_lebel,
      this.u_cdate,
      this.u_id,
      this.u_status,
      this.u_country,
      this.u_area,
      this.u_address,
      this.u_utype,
      this.u_fname,
      this.u_lname,
      this.u_gender,
      this.u_account,
      this.u_pwd,
      this.u_mobile,
      this.u_email,
      this.u_comment});

  factory Users.fromMap(Map<String, dynamic> json) => new Users(
        u_id: int.tryParse(json['u_id'].toString().trim()),
        u_status: int.tryParse(json['u_status'].toString().trim()),
        u_country: int.tryParse(json['u_country'].toString().trim()),
        u_area: int.tryParse(json['u_area'].toString().trim()),
        u_address: int.tryParse(json['u_address'].toString().trim()),
        u_utype: int.tryParse(json['u_utype'].toString().trim()),

        u_fname: json['u_fname'] ?? '',
        u_lname: json['u_lname'] ?? '',
        u_gender: json['u_gender'] ?? '',
        u_account: json['u_account'] ?? '',
        u_pwd: json['u_pwd'] ?? '',
        u_mobile: json['u_mobile'] ?? '',
        u_email: json['u_email'] ?? '',
        u_cdate: json['u_cdate'] ?? '',
        u_comment: json['u_comment'] ?? '',

        // aditional data

        u_area_lebel: json['u_area_lebel'] ?? '',
        u_address_lebel: json['u_address_lebel'] ?? '',
        u_country_lebel: json['u_country_lebel'] ?? '',
        u_utype_lebel: json['u_utype_lebel'] ?? '',
      );
  Map<String, dynamic> toMap() => {
        'u_id': u_id,
        'u_status': u_status,
        'u_country': u_country,
        'u_area': u_area,
        'u_address': u_address,
        'u_utype': u_utype,
        'u_fname': u_fname,
        'u_lname': u_lname,
        'u_gender': u_gender,
        'u_account': u_account,
        'u_pwd': u_pwd,
        'u_mobile': u_mobile,
        'u_email': u_email,
        'u_cdate': u_cdate,
        'u_comment': u_comment,
      };
}
