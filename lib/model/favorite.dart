class Favorite {
  int f_id;
  int f_prd;
  int f_prt;
  String f_date;

  Favorite({this.f_id, this.f_prd, this.f_prt, this.f_date});

  factory Favorite.fromMap(Map<String, dynamic> json) => new Favorite(
        f_id: int.tryParse(json['f_id'].toString().trim()),
        f_prd: int.tryParse(json['f_prd'].toString().trim()),
        f_prt: int.tryParse(json['f_prt'].toString().trim()),
        f_date: json['f_date'] ?? '',
      );

  Map<String, dynamic> toMap() => {
        'f_prd': f_prd,
        'f_prt': f_prt,
        'f_date': f_date,
      };
}
